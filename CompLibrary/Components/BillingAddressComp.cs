﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DBLibrary;
namespace CompLibrary
{
   public  class BillingAddressComp
    {
        //Customer Billing Address
        public static DataTable GetCustomerBillingAddressByID(int custometid, int RowsPerPage, int PageNumber)
        {
            return BillingAddressDB.GetCustomerBillingAddresbyCustomerID(custometid, RowsPerPage, PageNumber);
        }
        public static long AddCustomerAddress(string FirstName, string LastName,   int CountryId, int StateProvinceId,
            string City, string Address1, string Address2, string ZipPostalCode, string PhoneNumber ,int customerID)
        {
            return BillingAddressDB.AddCustomerBillingAddress(FirstName, LastName,   CountryId, StateProvinceId,
                City, Address1, Address2, ZipPostalCode, PhoneNumber,  customerID);
        }
        public static long UpdateCustomerBillAddress(string FirstName, string LastName,   int CountryId, int StateProvinceId,
            string City, string Address1, string Address2, string ZipPostalCode, string PhoneNumber, int customerID, int AddressID)
        {
            return BillingAddressDB.UpdateCustomerBillingAddress(FirstName, LastName,  CountryId, StateProvinceId,
                City, Address1, Address2, ZipPostalCode, PhoneNumber,  customerID, AddressID);
        }
        public static long DeleteCustomerBillingAddress(int CustomerID, int Address_ID)
        {
            return BillingAddressDB.DeleteCustomerBillingAddress(CustomerID, Address_ID);
        }
    }
}
