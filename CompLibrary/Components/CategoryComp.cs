﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CompLibrary
{
    public class CategoryComp
    {
        public static DataTable getAllCategories(int categoryID =0)
        {
            return CategoryDB.getAllCategories(categoryID);
        }
    }
}
