﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CompLibrary
{
    public class CyberplatConfComp
    {
        public static DataTable getCyberPlatConf(string configID)
        {
            return CyberplatConfDB.getCyberPlatConf(configID);
        }
    }
}
