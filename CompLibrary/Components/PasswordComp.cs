﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DBLibrary;
namespace CompLibrary
{
   public class PasswordComp
    {
        public static DataTable GetCustomerInformation(int customerID)
        {
            return PasswordDB.GetCustomerIformation(customerID);
        }

        public static long UpdateNewPassword(int CustomerId, string password)
        {
            return PasswordDB.UpdateNewPassword(CustomerId, password);
        }

        public static DataTable GetCustomerInformationByEamilid (string  email)
        {
            return PasswordDB.GetCustomerInformationByEamilid(email);
        }


        public static long InsertPasswordResetToken(string token, int customerid)
        {
            return PasswordDB.InsertPasswordResetToken(token, customerid);
        }
        public static DataTable GetEmaillerData()
        {
            return PasswordDB.GetEmaillerData();
        }

        public static DataTable StoreInformation()
        {
            return PasswordDB.StoreInformation();
        }
        public static DataTable GetLatestPassword(int customerid)
        {
            return PasswordDB.GetLatestPassword(customerid);
        }
    }
}
