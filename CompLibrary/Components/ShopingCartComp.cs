﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DBLibrary;
namespace CompLibrary
{
    public class ShopingCartComp
    {
        //Shopping Cart
        public static DataTable GetMaxProductQty(int ProductID)
        {
            return ShopingCartDB.GetMaxProductQtyAllow(ProductID);
        }
        public static long InsertGuestId(bool IsSystemAccount)
        {
            return ShopingCartDB.InsertGuestId(IsSystemAccount);
        }

        public static long AddShopingCartItem(int customerId, int? ParentItemId, int? BundleItemId, int ProductId, string AttributesXml, int Quantity)
        {
            return ShopingCartDB.AddItemstoCart(customerId, ParentItemId, BundleItemId, ProductId, AttributesXml,Quantity);
        }
        public static DataTable AddShopingCartItem(int customerid,int productid)
        {
            return ShopingCartDB.AddShopingCartItem(customerid, productid);
        }

        public static long UpdateCartItmes( int customerId, int ProductId, int Quantity)
        {
            return ShopingCartDB.UpdateCartItems(customerId, ProductId, Quantity);
        }
        public static long DeleteCartItems(int customerId, int ProductId)
        {
            return ShopingCartDB.DeleteCartItems(customerId, ProductId);
        }
        public static DataTable GetCartItemsDetails(int customerID, int ItemsPerPage, int PageNumber)
        {
            return ShopingCartDB.GetCartItemsDetailsByCustomerID(customerID, ItemsPerPage, PageNumber);
        }
//08.02.2018
        public static DataTable CheckProductIncluesBundle(int ProductID)
        {
            return ShopingCartDB.CheckProductIncludesBundle(ProductID);
        }
        public static DataTable GetParentItemID(int ProductID, int CustomerId)
        {
            return ShopingCartDB.GetProductParentItemId(ProductID, CustomerId);
        }

        public static DataTable CheckCustomerIsvalid( int CustomerId)
        {
            return ShopingCartDB.CheckCustomerIsvalid(CustomerId);
        }

        public static DataTable GetProdutAttributeXml(int ProdutId, int VariantId)
        {
            return ShopingCartDB.GetProductAttribuuteXml(ProdutId, VariantId);
        }
        public static DataTable GetCartItemsCount(int customerID)
        {
            return ShopingCartDB.GetCartItemsCount(customerID);
        }
        public static DataTable getproductdetails(int id_customer,int id_product)
        {
            return ShopingCartDB.getproductdetails(id_customer, id_product);
        }
        public static DataTable cartTotal(int id_customer)
        {
            return ShopingCartDB.cartTotal(id_customer);
        }
        public static DataTable GetCartdetails(int id_customer)
        {
            return ShopingCartDB.GetCartdetails(id_customer);
        }
    }
}
