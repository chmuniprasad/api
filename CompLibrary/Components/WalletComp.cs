﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CompLibrary
{
    public class WalletComp
    {

        public static DataTable getCustomerCurrentWalletBalance(int customerID)
        {
            return WalletDB.getCustomerCurrentWalletBalance(customerID);
        }

        public static long saveWalletBalance(string userID, string orderID, string amount, string balanceAmount, int type, int serviceID, string IPAddress, string desc)
        {
            return WalletDB.saveWalletBalance(userID, orderID, amount, balanceAmount, type, serviceID, IPAddress, desc);
        }

        public static long createWalletOrder(int customerID, int toCustomerID, int amount, int serviceID, int type, string text)
        {
            return WalletDB.createWalletOrder(customerID, toCustomerID, amount, serviceID, type, text);
        }
        public static DataTable getWalletOrderDetailsByOrderId(int OrderID)
        {
            return WalletDB.getWalletOrderDetailsByOrderId(OrderID);
        }

        public static DataTable getWalletHistory(int customerID, int RowsPerPage, int PageNumber)
        {
            return WalletDB.getWalletHistory(customerID, RowsPerPage, PageNumber);
        }

        public static DataTable getRechargeHistory(int customerID,int RowsPerPage, int PageNumber)
        {
            return WalletDB.getRechargeHistory(customerID, RowsPerPage, PageNumber);
        }
        public static DataTable getSendtoMobile(int customerID,int walletOrderid)
        {
            return WalletDB.getSendtoMobile(customerID, walletOrderid);
        }
        public static DataTable getreceiveFromMobile(int customerID, int walletOrderid)
        {
            return WalletDB.getreceiveFromMobile(customerID, walletOrderid);
        }

       
    }
}
