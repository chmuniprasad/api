﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DBLibrary;

namespace CompLibrary
{
   public  class HomePageComp
    {
        public static DataTable GetHomepageBanners()
        {
            return HomePageDB.GetHomepageBanners();
        }
        public static DataTable GetHomepageCategories()
        {
            return HomePageDB.GetHomepageCatetories();
        }
        public static DataTable shopbyCategories()
        {
            return HomePageDB.shopbyCategories();
        }
        public static DataTable newproductlist1()
        {
            return HomePageDB.newproductlist1();
        }
        public static DataTable newproductlist2()
        {
            return HomePageDB.newproductlist2();
        }
        public static DataTable CheckProductDelivery(string zipcode, int id_product)
        {
            return HomePageDB.CheckProductDelivery(zipcode, id_product);
        }

        public static DataTable activeServices()
        {
            return HomePageDB.activeServices();
        }

        public static DataTable GetStates(int RowsPerPage, int PageNumber)
        {
            return HomePageDB.GetStates(RowsPerPage, PageNumber);
        }
        public static DataTable ShippingCarrier(int id_customer)
        {
            return HomePageDB.ShippingCarrier(id_customer);
        }
       
    }
}
