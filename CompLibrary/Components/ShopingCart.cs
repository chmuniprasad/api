﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DBLibrary;
namespace CompLibrary
{
    public class ShopingCart
    {
       //Shopping Cart
        public static long AddShopingCartItem(int StoreId, int ParentItemId, int BundleItemId, int ShoppingCartTypeId, int customerId, int ProductId, string AttributesXml, double CustomerEnteredPrice, int Quantity, DateTime CreatedOnUtc, DateTime UpdatedOnUtc)
        {
            return ShopingCartDB.AddItemstoCart(StoreId, ParentItemId,BundleItemId,ShoppingCartTypeId,customerId,ProductId,AttributesXml,CustomerEnteredPrice,Quantity,CreatedOnUtc,UpdatedOnUtc);
        }

        public static long UpdateCartItmes(int StoreId, int ParentItemId, int BundleItemId, int ShoppingCartTypeId, int customerId, int ProductId, string AttributesXml, double CustomerEnteredPrice, int Quantity,  DateTime UpdatedOnUtc)
        {
            return ShopingCartDB.UpdateCartItems(StoreId, ParentItemId, BundleItemId, ShoppingCartTypeId, customerId, ProductId, AttributesXml, CustomerEnteredPrice, Quantity,  UpdatedOnUtc);
        }
        public static long DeleteCartItems(int customerId, int ProductId,int Quantity)
        {
            return ShopingCartDB.DeleteCartItems(customerId, ProductId, Quantity);
        }
        public static DataTable GetCartItemsDetails(int customerID)
        {
            return ShopingCartDB.GetCartItemsDetailsByCustomerID(customerID);
        }

      


    }
}
