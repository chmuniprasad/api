﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CompLibrary
{
    public class OrdersComp
    {
        public static DataTable getShopingCartOrders(int customerID, int RowsPerPage, int PageNumber)
        {
            return OrdersDB.getShopingCartOrders(customerID,RowsPerPage,PageNumber);
        }
        public static DataTable getcustomeraddress(int customerID, int addressid)
        {
            return OrdersDB.getcustomeraddress(customerID, addressid);
        }
        public static DataTable getOrderDetails(int customerID, int OrderID)
        {
            return OrdersDB.getOrderDetails(customerID, OrderID);
        }

        public static long ConfirmOrder(int StoreId, int CustomerId, int BillingAddressId, int ShippingAddressId, int OrderStatusId, int ShippingStatusId, int PaymentStatusId,
                                         string PaymentMethodSystemName, string CustomerCurrencyCode, decimal CurrencyRate, int CustomerTaxDisplayTypeId, string VatNumber,
                                         decimal OrderSubtotalInclTax, decimal OrderSubtotalExclTax, decimal OrderSubTotalDiscountInclTax, decimal OrderSubTotalDiscountExclTax,
                                         decimal OrderShippingInclTax, decimal OrderShippingExclTax, decimal PaymentMethodAdditionalFeeInclTax, decimal PaymentMethodAdditionalFeeExclTax,
                                         string TaxRates, decimal OrderTax, decimal OrderDiscount, decimal OrderTotal, decimal RefundedAmount, bool RewardPointsWereAdded,
                                         string CheckoutAttributeDescription, string CheckoutAttributesXml, int CustomerLanguageId, int AffiliateId, string CustomerIp,
                                         bool AllowStoringCreditCardNumber, string CardType, string CardName, string CardNumber, string MaskedCreditCardNumber, string CardCvv2,
                                         string CardExpirationMonth, string CardExpirationYear, bool AllowStoringDirectDebit, string DirectDebitAccountHolder, string DirectDebitAccountNumber,
                                         string DirectDebitBankCode, string DirectDebitBankName, string DirectDebitBIC, string DirectDebitCountry, string DirectDebitIban, string AuthorizationTransactionId,
                                         string AuthorizationTransactionCode, string AuthorizationTransactionResult, string CaptureTransactionId, string CaptureTransactionResult, string SubscriptionTransactionId,
                                         string PurchaseOrderNumber, DateTime PaidDateUtc, string ShippingMethod, string ShippingRateComputationMethodSystemName, bool Deleted, DateTime CreatedOnUtc,
                                         DateTime UpdatedOnUtc, int RewardPointsRemaining, string CustomerOrderComment, decimal OrderShippingTaxRate, decimal PaymentMethodAdditionalFeeTaxRate,
                                         bool HasNewPaymentNotification, bool AcceptThirdPartyEmailHandOver, decimal OrderTotalRounding,string order_from)
        {
            return OrdersDB.ConfirmOrder(StoreId,CustomerId,BillingAddressId,ShippingAddressId,OrderStatusId,ShippingStatusId,PaymentStatusId,PaymentMethodSystemName,CustomerCurrencyCode,CurrencyRate,
                CustomerTaxDisplayTypeId,VatNumber, OrderSubtotalInclTax, OrderSubtotalExclTax, OrderSubTotalDiscountInclTax,OrderSubTotalDiscountExclTax,OrderShippingInclTax,OrderShippingExclTax,
                PaymentMethodAdditionalFeeInclTax,PaymentMethodAdditionalFeeExclTax,TaxRates,OrderTax,OrderDiscount,OrderTotal,RefundedAmount,RewardPointsWereAdded,CheckoutAttributeDescription,CheckoutAttributesXml,
                CustomerLanguageId, AffiliateId,CustomerIp,AllowStoringCreditCardNumber,CardType,CardName,CardNumber, MaskedCreditCardNumber, CardCvv2, CardExpirationMonth, CardExpirationYear, AllowStoringDirectDebit, 
                DirectDebitAccountHolder, DirectDebitAccountNumber, DirectDebitBankCode, DirectDebitBankName, DirectDebitBIC, DirectDebitCountry, DirectDebitIban, AuthorizationTransactionId, AuthorizationTransactionCode, 
                AuthorizationTransactionResult, CaptureTransactionId, CaptureTransactionResult, SubscriptionTransactionId, PurchaseOrderNumber, PaidDateUtc, ShippingMethod, ShippingRateComputationMethodSystemName, Deleted, 
                CreatedOnUtc, UpdatedOnUtc, RewardPointsRemaining, CustomerOrderComment, OrderShippingTaxRate, PaymentMethodAdditionalFeeTaxRate, HasNewPaymentNotification, AcceptThirdPartyEmailHandOver, OrderTotalRounding,order_from
                );
        }

        public static long InsertOrderItemsData(int CustomerId, int ProductId, int Quantity, decimal UnitPriceInclTax, decimal UnitPriceExclTax, decimal PriceInclTax,
                                                 decimal PriceExclTax, decimal DiscountAmountInclTax, decimal DiscountAmountExclTax, string AttributeDescription,
                                                 string AttributesXml, int DownloadCount, bool IsDownloadActivated, int LicenseDownloadId, decimal ItemWeight, string BundleData,
                                                 decimal ProductCost, decimal TaxRate)
        {
            return OrdersDB.InsertOrderItemsData(CustomerId, ProductId, Quantity, UnitPriceInclTax, UnitPriceExclTax,PriceInclTax,PriceExclTax,DiscountAmountInclTax,DiscountAmountExclTax,AttributeDescription,AttributesXml,DownloadCount,
                IsDownloadActivated,LicenseDownloadId,ItemWeight,BundleData,ProductCost,TaxRate);
        }


       
        public static DataTable CheckBundleIteminCart(int customerID)
        {
            return OrdersDB.CheckBuldleItemsavailableinCart(customerID);
        }
        public static DataTable GetCartItemsBeforeOrder(int customerID, bool bundleitem)
        {
            return OrdersDB.GetCartItemsBeforePlaceOrder(customerID, bundleitem);
        }
        public static DataTable GetPriceAdjnustment(int id,int val)
        {
            return OrdersDB.GetPriceAdjustment(id,val);
        }

        public static DataTable GetProductTaxCategory(int ProductID)
        {
            return OrdersDB.GetProductTaxCategory(ProductID);
        }

        public static long RemoveFromCart(int CustomerId)
        {
            return OrdersDB.RemoveFromCartAfterPlaceOrder(CustomerId);
        }
        public static long UpdateProductStock(int Productid, int Qty)
        {
            return OrdersDB.UpdateProductStock(Productid, Qty);
        }


        public static DataTable getOrderDetailsbyOrderId(int OrderID)
        {
            return OrdersDB.getOrderDetailsByOrderId( OrderID);
        }
        public static DataTable GetDeliveryAddressbyOrderID(int addressid, int OrderID)
        {
            return OrdersDB.GetDeliveryAddressbyOrderID(addressid, OrderID);
        }
        public static DataTable GetOrderDetailsWithOrderId(int customerid, int OrderID)
        {
            return OrdersDB.GetOrderDetailsWithOrderId(customerid, OrderID);
        }
        public static DataTable GetTierPrice(int OrderID)
        {
            return OrdersDB.GetTierPrice(OrderID);
        }

        public static DataTable GetproductsInContactByOrderId(int id_customer, int id_order)
        {
            return OrdersDB.GetproductsInContactByOrderId(id_customer, id_order);
        }
        public static DataTable orderReferencesByCustomerid(int id_customer)
        {
            return OrdersDB.orderReferencesByCustomerid(id_customer);
        }

        public static DataTable Paymentmethods(int customerID)
        {
            return OrdersDB.Paymentmethods(customerID);
        }

        public static DataTable Pricedetails(int customerid)
        {
            return OrdersDB.Pricingdetails(customerid);
        }
        public static DataTable GetOrderProducts(int customerid,int orderid)
        {
            return OrdersDB.GetOrderProducts(customerid, orderid);
        }
    }
}
