﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBLibrary;
using System.Data;
namespace CompLibrary
{
   public class ReviewsComp
    {
        public static long WriteProductReview(int ProductId, string Title, string ReviewText, int Rating, int HelpfulYesTotal, int HelpfulNoTotal, int CustomerId, string IpAddress, bool IsApproved, DateTime CreatedOnUtc, DateTime UpdatedOnUtc)
        {
            return ReviewsDB.WriteProductReviews(ProductId,Title,ReviewText,Rating,HelpfulYesTotal,HelpfulNoTotal,CustomerId,IpAddress,IsApproved,CreatedOnUtc,UpdatedOnUtc);
        }

        public static DataTable GetProductReviews(int ProductID,int ItemsPerPage, int PageNumber)
        {
            return ReviewsDB.GetProductReviews(ProductID, ItemsPerPage, PageNumber);
        }
        public static DataTable GetCustomerName(int id_customer)
        {
            return ReviewsDB.GetCustomerName(id_customer);
        }
        public static DataTable ReviewCheckStatus(int id_customer, int reviewid)
        {
            return ReviewsDB.ReviewCheckStatus(id_customer, reviewid);
        }
        public static DataTable CustomerRole(int Customerid)
        {
            return ReviewsDB.GetCustomerRole(Customerid);
        }

        public static DataTable GetReviewsbyCustomerID(int CustomerID,int ItemsPerPage,int PageNumber)
        {
            return ReviewsDB.GetProductReviewsByCustomer(CustomerID, ItemsPerPage, PageNumber);
        }
        public static long ProductReviewHelpfulYes(int CustomerId, string IpAddress, bool IsApproved, DateTime CreatedOnUtc, DateTime UpdatedOnUtc, int ProductReviewId,bool is_useful)
        {
            return ReviewsDB.ProductReviewHelpfulYES(CustomerId, IpAddress, IsApproved, CreatedOnUtc, UpdatedOnUtc, ProductReviewId, is_useful);
        }

        public static long ProductReviewHelpfulNo(int CustomerId, string IpAddress, bool IsApproved, DateTime CreatedOnUtc, DateTime UpdatedOnUtc, int ProductReviewId)
        {
            return ReviewsDB.ProductReviewHelpfulNo(CustomerId, IpAddress, IsApproved, CreatedOnUtc, UpdatedOnUtc, ProductReviewId);
        }
        public static DataTable ProductReviewHelpfulYesNO(int  ProductReviewId)
        {
            return ReviewsDB.ProductReviewHelpfulYesNO(ProductReviewId);
        }
    }
}
