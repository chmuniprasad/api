﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CompLibrary
{

    public class RechargeBillPaymentsComp
    {
        
        public static long saveRechargeAndBillPaymentOrder(string customerID, string orderTypeID, string serviceID, string amount, string IpAddress)
        {
             return RechargeBillPaymentsDB.saveRechargeAndBillPaymentOrder(customerID, orderTypeID, serviceID, amount, IpAddress);
        }
        public static long saveMobileRechargeData(string OrderID, string networkType, string network, string mobileNumber)
        {
            return RechargeBillPaymentsDB.saveMobileRechargeData(OrderID, networkType, network, mobileNumber);
        }

        public static long updateRechargeOrdersData(string OrderID, string error, string result, string rechargeStatus, string cert, string Paydate, string TxID)
        {
            return RechargeBillPaymentsDB.updateRechargeOrdersData(OrderID, error, result, rechargeStatus, cert, Paydate, TxID);
        }

        public static DataTable getCurrentCustomerRBOrderID(string customerID)
        {
            return RechargeBillPaymentsDB.getCurrentCustomerRBOrderID(customerID);
        }

        public static long saveDthRechargeData(string OrderID, string operatorId, string cardNumber)
        {
            return RechargeBillPaymentsDB.saveDthRechargeData(OrderID, operatorId, cardNumber);
        }

        public static long saveElectricityBillPaymentData(string OrderID, string boardNumber, string customerNumber)
        {
            return RechargeBillPaymentsDB.saveElectricityBillPaymentData(OrderID, boardNumber, customerNumber);
        }

        public static long saveGasBillPaymentData(string OrderID, string operatorCode, string customerNumber, string billgroupNumber)
        {
            return RechargeBillPaymentsDB.saveGasBillPaymentData(OrderID, operatorCode, customerNumber, billgroupNumber);
        }

        public static DataTable checkUserDailyMonthlyTrasctions(int CustomerId, int isDaily, int txType)
        {
            return RechargeBillPaymentsDB.checkUserDailyMonthlyTrasctions(CustomerId, isDaily, txType);
        }

    }
}
