﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CompLibrary
{
    public class ProductsComp
    {

        public static DataTable getProducts(string categoryID, string searchText, string orderby, string orderway,int ItemsPerPage,int PageNumber)
        {
            return ProductsDB.getProducts(categoryID, searchText, orderby, orderway, ItemsPerPage, PageNumber);
        }

        public static DataTable getProductDetails(int productID)
        {
            return ProductsDB.getProductDetails(productID);
        }
        public static DataTable getProductDetailsbyCustomer(int productID, int customerid)
        {
            return ProductsDB.getProductDetailsbyCustomer(productID, customerid);
        }
        public static DataTable GetproductsSameCategory(int CategoryId)
        {
            return ProductsDB.getProductsOfsameCategory(CategoryId);
        }

        public static DataTable GetProductStockDetails(string customerid)
        {
            return ProductsDB.GetProductStockinDetail(customerid);
        }

        public static DataTable GetAllProducts(string display, int ItemsPerPage, int PageNumber)
        {
            return ProductsDB.getAllProductDetails(display, ItemsPerPage, PageNumber);
        }
        public static DataTable getProductsbyCategory(int id_category, int ItemsPerPage, int PageNumber)
        {
            return ProductsDB.getAllProductsByCatagory(id_category, ItemsPerPage, PageNumber);
        }
        public static DataTable getCategories(int ItemsPerPage, int PageNumber)
        {
            return ProductsDB.getCategories(ItemsPerPage, PageNumber);
        }
        public static DataTable getProductfromcart(int productID, int customerid)
        {
            return ProductsDB.getProductfromcart(productID, customerid);
        }
        public static DataTable getProductimages(int productID)
        {
            return ProductsDB.getProductimages(productID);
        }
        public static DataTable ProductRatings(int productID)
        {
            return ProductsDB.ProductRating(productID);
        }

        public static DataTable GetProductsBysubCategory(string id,string  orderby,string  orderway, int ItemsPerPage, int PageNumber)
        {
            return ProductsDB.GetProductsBysubCategory(id, orderby, orderway, ItemsPerPage, PageNumber);
        }

        public static DataTable GetImageFullNames(int imgId)
        {
            return ProductsDB.GetImgaeFullNames(imgId);
        }

        public static DataTable GetSameImageNames(string  ProductName, string SeofileName)
        {
            return ProductsDB.Getsameimagenames(ProductName, SeofileName);
        }
    }
}
