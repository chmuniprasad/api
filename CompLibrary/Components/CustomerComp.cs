﻿using DBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CompLibrary
{
    public class CustomerComp
    {
        public static long addCustomer(string email, string password, string phoneNumber)
        {
            return CustomerDB.addCustomer(email, password, phoneNumber);
        }

        public static DataTable getcustomerbyEmail(string email)
        {
            return CustomerDB.getcustomerbyEmail(email);
        }
        public static DataTable getcustomerbyphone(string mobile)
        {
            return CustomerDB.getcustomerbyphone(mobile);
        }
        //This method used for authenticate a customer
        public static DataTable login(string userName, string password)
        {
            return CustomerDB.login(userName, password);
        }
        public static DataTable changePassword(string userName, string password)
        {
            return CustomerDB.ChangePassword(userName, password);
        }
        public static DataTable RegisterDetails(string email, string mobile)
        {
            return CustomerDB.RegisterDetails(email, mobile);
        }

        public static DataTable getCustomerDetailsByUserName(string phoneNumber)
        {
            return CustomerDB.getCustomerDetailsByUserName( phoneNumber);
        }

        public static DataTable getCustomerDetailsByUser(string userName, bool MobileOrEmail)
        {
            return CustomerDB.getCustomerDetailsByUser(userName, MobileOrEmail);
        }

        public static DataTable getCustomerDetails(int customerID)
        {
            return CustomerDB.getCustomerDetailsByCustomerID(customerID);
        }

        public static long UpdateCustomerProfile(int CustomerID, string Gender, string FirstName, string LastName, string DateOfBirth, string Company, string City, string Phone, string Signature, string VatNumber)
        {
            return CustomerDB.UpdateCustomerProfile(CustomerID, Gender, FirstName, LastName, DateOfBirth, Company, City, Phone, Signature, VatNumber);
        }

        public static long updatecustomerImage(int CustomerID, string imagename)
        {
            return CustomerDB.updatecustomerImage(CustomerID, imagename);
        }

        public static DataTable RegisteredCustomers(int customer)
        {
            return CustomerDB.GetRegisteredCustomers(customer);
        }

        public static DataTable CustomerProfileDetails(int customerid)
        {
            return CustomerDB.CustomerProfileDetails(customerid);
        }

        public static DataTable getCustomerOtp(int customerid)
        {
            return CustomerDB.getcustomerotp(customerid);
        }
        public static long UpdateCustomerData(int CustomerID, int Gender, string FirstName, string LastName, string DateOfBirth)
        {
            return CustomerDB.UpdateCustomerData(CustomerID, Gender, FirstName, LastName, DateOfBirth);
        }
    }
}
