﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DBLibrary;

namespace CompLibrary
{
   public class NetworkComp
    {
        public static DataTable GetRechargeNetworks(int serviceid,int type)
        {
            return NetworkDB.GetRechargeNetworks(serviceid, type);
        }

        public static DataTable getAllNetworks(int serviceid)
        {
            return NetworkDB.getAllNetworks(serviceid);
        }
    }
}
