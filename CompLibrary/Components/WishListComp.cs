﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DBLibrary;

namespace CompLibrary
{
    public class WishListComp
    {
        //Wish list 
        public static DataTable GetWhishlisttemsDetails(int custometid, int shopingcarttype,int  ItemsPerPage,int PageNumber)
        {
            return WishListDB.GetWhishlisttemsDetailsByCustomerID(custometid, shopingcarttype, ItemsPerPage, PageNumber);
        }
        public static long UpdateWishlistItmes( int customerId, int ProductId, int Quantity)
        {
            return WishListDB.UpdateWishlistItems( customerId, ProductId, Quantity);
        }
        //public static long AddItemtoWishlist(int StoreId, int ParentItemId, int BundleItemId, int ShoppingCartTypeId, int customerId, int ProductId, string AttributesXml, double CustomerEnteredPrice, int Quantity, DateTime CreatedOnUtc, DateTime UpdatedOnUtc)
        //{
        //    return WishListDB.AddItemstoIshlist(StoreId, ParentItemId, BundleItemId, ShoppingCartTypeId, customerId, ProductId, AttributesXml, CustomerEnteredPrice, Quantity, CreatedOnUtc, UpdatedOnUtc);
        //}
        public static long DeleteWishlistItems(int customerId, int ProductId)
        {
            return WishListDB.DeleteWishlistItems( customerId, ProductId);
        }

        public static long AddItemtoWishlist(int customerId, int? ParentItemId, int? BundleItemId, int ProductId, string AttributesXml, int Quantity)
        {
            return WishListDB.AddItemstoIshlist(customerId, ParentItemId, BundleItemId, ProductId, AttributesXml, Quantity);
        }
    }
}
