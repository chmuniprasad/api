﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBLibrary;
using System.Data;
namespace CompLibrary
{
   public  class GiftCardComp
    {
        public static DataTable GetgiftCardOrderDetails(int custometid, int ItemsPerPage, int PageNumber)
        {
            return GiftCardDB.GetGiftCardOrders(custometid, ItemsPerPage, PageNumber);
        }

        public static long AddGiftCardtoCart(int customerId, int ProductId, string AttributesXml, int Quantity)
        {
            return GiftCardDB.AddGiftCardtoCart( customerId, ProductId, AttributesXml, Quantity);
        }

        public static long DeletGiftCard(int CustomerID,int ProductId,int Quantity)
        {
            return GiftCardDB.DeleteGiftCardfromCart(CustomerID, ProductId, Quantity);
        }
    }
}
