﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class OrdersDB
    {
        public static DataTable dt = new DataTable();
        public static long result;
        public static DataTable dt2 = new DataTable();

        public static DataTable getShopingCartOrders(int customerID, int RowsPerPage, int PageNumber)
        {
            try
            {
                
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.VarChar) { Value = customerID },
                    new SqlParameter("@RowsPerPage",SqlDbType.Int){Value=RowsPerPage},
                    new SqlParameter("@PageNumber",SqlDbType.Int ){Value =PageNumber}
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getShopingCartOrders", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getcustomeraddress(int customerid,  int addressid)
        {
            try
            {

                SqlParameter[] parameters = { new SqlParameter("@customerid", SqlDbType.VarChar) { Value = customerid },
                    new SqlParameter("@addressid",SqlDbType.Int){Value=addressid}
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getcustomeraddress", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getOrderDetails(int customerID, int OrderID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID",SqlDbType.Int){Value =customerID},
                    new SqlParameter("@OrderID", SqlDbType.Int)    { Value = OrderID }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getOrderDetails", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable getOrderDetailsByOrderId(int Orderid)
        {
            try
            {
                SqlParameter[] parameters = { 
                                              new SqlParameter("@Orderid", SqlDbType.VarChar)    { Value = Orderid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "SP_sp_getOrderDetailsByOrderId", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable GetDeliveryAddressbyOrderID(int addressid, int orderid)
        {
            try
            {
                SqlParameter[] parameters = {
                                              new SqlParameter("@addressid", SqlDbType.Int)    { Value = addressid },
                                              new SqlParameter("@orderid", SqlDbType.Int)    { Value = orderid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getDeliveryAddress", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable GetOrderDetailsWithOrderId(int customerID, int orderid)
        {
            try
            {
                SqlParameter[] parameters = {
                                              new SqlParameter("@customerID", SqlDbType.Int)    { Value = customerID },
                                              new SqlParameter("@orderid", SqlDbType.Int)    { Value = orderid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "SP_getOrderDetailswithOrderid", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable CheckBuldleItemsavailableinCart(int CustomerID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@CustomerID", SqlDbType.VarChar) { Value = CustomerID }

                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CheckBundleItemsAvailability", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }


        public static DataTable GetCartItemsBeforePlaceOrder(int customerID, bool bundleitem)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.VarChar) { Value = customerID },
                                               new SqlParameter("@bundleitem", SqlDbType.Bit) { Value = bundleitem }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCartDetailsBeforPlaceOrder", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable GetPriceAdjustment(int id, int val)
        {
           
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@id", SqlDbType.Int) { Value = val },
                                              new SqlParameter("@val", SqlDbType.Int ) { Value = id }

                                            };
                 dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetPriceAdjustment", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static long ConfirmOrder(int StoreId, int CustomerId, int BillingAddressId, int ShippingAddressId, int OrderStatusId, int ShippingStatusId, int PaymentStatusId,
                                         string PaymentMethodSystemName, string CustomerCurrencyCode, decimal CurrencyRate, int CustomerTaxDisplayTypeId, string VatNumber,
                                         decimal OrderSubtotalInclTax, decimal OrderSubtotalExclTax, decimal OrderSubTotalDiscountInclTax, decimal OrderSubTotalDiscountExclTax,
                                         decimal OrderShippingInclTax, decimal OrderShippingExclTax, decimal PaymentMethodAdditionalFeeInclTax, decimal PaymentMethodAdditionalFeeExclTax,
                                         string TaxRates, decimal OrderTax, decimal OrderDiscount, decimal OrderTotal, decimal RefundedAmount, bool RewardPointsWereAdded,
                                         string CheckoutAttributeDescription, string CheckoutAttributesXml, int CustomerLanguageId, int AffiliateId, string CustomerIp,
                                         bool AllowStoringCreditCardNumber, string CardType, string CardName, string CardNumber, string MaskedCreditCardNumber, string CardCvv2,
                                         string CardExpirationMonth, string CardExpirationYear, bool AllowStoringDirectDebit, string DirectDebitAccountHolder, string DirectDebitAccountNumber,
                                         string DirectDebitBankCode, string DirectDebitBankName, string DirectDebitBIC, string DirectDebitCountry, string DirectDebitIban, string AuthorizationTransactionId,
                                         string AuthorizationTransactionCode, string AuthorizationTransactionResult,string CaptureTransactionId,string CaptureTransactionResult,string SubscriptionTransactionId,
                                         string PurchaseOrderNumber,DateTime PaidDateUtc,string ShippingMethod,string ShippingRateComputationMethodSystemName,bool Deleted,DateTime CreatedOnUtc,
                                         DateTime UpdatedOnUtc,int RewardPointsRemaining,string CustomerOrderComment,decimal OrderShippingTaxRate,decimal PaymentMethodAdditionalFeeTaxRate,
                                         bool HasNewPaymentNotification,bool AcceptThirdPartyEmailHandOver, decimal OrderTotalRounding,string order_from
                                       )
           
        {
            string OrderNumber = "";
            var OrderGuid = Guid.NewGuid().ToString();
           // int id=0;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@OrderNumber",SqlDbType.VarChar){Value=OrderNumber},
                    new SqlParameter("@OrderGuid",SqlDbType.VarChar){Value=OrderGuid},
                    new SqlParameter("@StoreId",SqlDbType.Int){Value=StoreId},
                    new SqlParameter("@CustomerId", SqlDbType.Int) { Value = CustomerId },
                    new SqlParameter("@BillingAddressId", SqlDbType.Int) { Value = BillingAddressId },
                    new SqlParameter("@ShippingAddressId",SqlDbType.Int ){Value=ShippingAddressId},
                    new SqlParameter("@OrderStatusId",SqlDbType.Int ){Value=OrderStatusId},
                    new SqlParameter("@ShippingStatusId",SqlDbType.Int ){Value=ShippingStatusId},
                    new SqlParameter("@PaymentStatusId",SqlDbType.Int ){Value=PaymentStatusId},
                    new SqlParameter("@PaymentMethodSystemName",SqlDbType.VarChar ){Value=PaymentMethodSystemName},
                    new SqlParameter("@CustomerCurrencyCode",SqlDbType.VarChar ){Value=CustomerCurrencyCode},
                    new SqlParameter("@CurrencyRate",SqlDbType.Decimal ){Value=CurrencyRate},
                    new SqlParameter("@CustomerTaxDisplayTypeId",SqlDbType.Int ){Value=CustomerTaxDisplayTypeId},
                    new SqlParameter("@VatNumber",SqlDbType.VarChar ){Value=VatNumber},
                    new SqlParameter("@OrderSubtotalInclTax",SqlDbType.Decimal ){Value=OrderSubtotalInclTax},
                    new SqlParameter("@OrderSubtotalExclTax",SqlDbType.Decimal ){Value=OrderSubtotalExclTax},
                    new SqlParameter("@OrderSubTotalDiscountInclTax",SqlDbType.Decimal ){Value=OrderSubTotalDiscountInclTax},
                    new SqlParameter("@OrderSubTotalDiscountExclTax",SqlDbType.Decimal ){Value=OrderSubTotalDiscountExclTax},
                    new SqlParameter("@OrderShippingInclTax",SqlDbType.Decimal ){Value=OrderShippingInclTax},
                    new SqlParameter("@OrderShippingExclTax",SqlDbType.Decimal ){Value=OrderShippingExclTax},
                    new SqlParameter("@PaymentMethodAdditionalFeeInclTax",SqlDbType.Decimal ){Value=PaymentMethodAdditionalFeeInclTax},
                    new SqlParameter("@PaymentMethodAdditionalFeeExclTax",SqlDbType.Decimal ){Value=PaymentMethodAdditionalFeeExclTax},
                    new SqlParameter("@TaxRates",SqlDbType.VarChar ){Value=TaxRates},
                    new SqlParameter("@OrderTax",SqlDbType.Decimal ){Value=OrderTax},
                    new SqlParameter("@OrderDiscount",SqlDbType.Decimal ){Value=OrderDiscount},
                    new SqlParameter("@OrderTotal",SqlDbType.Decimal ){Value=OrderTotal},
                    new SqlParameter("@RefundedAmount",SqlDbType.Decimal ){Value=RefundedAmount},
                    new SqlParameter("@RewardPointsWereAdded",SqlDbType.Bit ){Value=RewardPointsWereAdded},
                    new SqlParameter("@CheckoutAttributeDescription",SqlDbType.VarChar ){Value=CheckoutAttributeDescription},
                    new SqlParameter("@CheckoutAttributesXml",SqlDbType.VarChar ){Value=CheckoutAttributesXml},
                    new SqlParameter("@CustomerLanguageId",SqlDbType.Int ){Value=CustomerLanguageId},
                    new SqlParameter("@AffiliateId",SqlDbType.Int ){Value=AffiliateId},
                    new SqlParameter("@CustomerIp",SqlDbType.VarChar ){Value=CustomerIp},
                    new SqlParameter("@AllowStoringCreditCardNumber",SqlDbType.Bit ){Value=AllowStoringCreditCardNumber},
                    new SqlParameter("@CardType",SqlDbType.VarChar ){Value=CardType},
                    new SqlParameter("@CardName",SqlDbType.VarChar ){Value=CardName},
                    new SqlParameter("@CardNumber",SqlDbType.VarChar ){Value=CardNumber},
                    new SqlParameter("@MaskedCreditCardNumber",SqlDbType.VarChar ){Value=MaskedCreditCardNumber},
                    new SqlParameter("@CardCvv2",SqlDbType.VarChar ){Value=CardCvv2},
                    new SqlParameter("@CardExpirationMonth",SqlDbType.VarChar ){Value=CardExpirationMonth},
                    new SqlParameter("@CardExpirationYear",SqlDbType.VarChar ){Value=CardExpirationYear},
                    new SqlParameter("@AllowStoringDirectDebit",SqlDbType.Bit ){Value=AllowStoringDirectDebit},
                    new SqlParameter("@DirectDebitAccountHolder",SqlDbType.VarChar){ Value=DirectDebitAccountHolder} ,
                    new SqlParameter("@DirectDebitAccountNumber",SqlDbType.VarChar){ Value =DirectDebitAccountNumber} ,
                    new SqlParameter("@DirectDebitBankCode",SqlDbType.VarChar){ Value =DirectDebitBankCode},
                    new SqlParameter("@DirectDebitBankName",SqlDbType.VarChar){Value =DirectDebitBankName },
                    new SqlParameter("@DirectDebitBIC",SqlDbType.VarChar){ Value=DirectDebitBIC } ,
                    new SqlParameter("@DirectDebitCountry",SqlDbType.VarChar){ Value =DirectDebitCountry },
                    new SqlParameter("@DirectDebitIban",SqlDbType.VarChar ){ Value =DirectDebitIban } ,
                    new SqlParameter("@AuthorizationTransactionId",SqlDbType.VarChar ) { Value =AuthorizationTransactionId}  ,
                    new SqlParameter("@AuthorizationTransactionCode",SqlDbType.VarChar){Value =AuthorizationTransactionCode } ,
                    new SqlParameter("@AuthorizationTransactionResult",SqlDbType.VarChar){Value =AuthorizationTransactionResult }  ,
                    new SqlParameter("@CaptureTransactionId",SqlDbType.VarChar){Value =CaptureTransactionId }  ,
                    new SqlParameter("@CaptureTransactionResult",SqlDbType.VarChar){Value =CaptureTransactionResult } ,
                    new SqlParameter("@SubscriptionTransactionId",SqlDbType.VarChar){ Value=SubscriptionTransactionId }  ,
                    new SqlParameter("@PurchaseOrderNumber",SqlDbType.VarChar){ Value =PurchaseOrderNumber}  ,
                    new SqlParameter("@PaidDateUtc",SqlDbType.DateTime){Value =PaidDateUtc },
                    new SqlParameter("@ShippingMethod",SqlDbType.VarChar){ Value=ShippingMethod} ,
                    new SqlParameter("@ShippingRateComputationMethodSystemName",SqlDbType.VarChar){Value=ShippingRateComputationMethodSystemName }  ,
                    new SqlParameter("@Deleted",SqlDbType.Bit){ Value=Deleted} ,
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc },
                    new SqlParameter("@UpdatedOnUtc",SqlDbType.DateTime){Value=UpdatedOnUtc },
                    new SqlParameter("@RewardPointsRemaining",SqlDbType.Int){Value=RewardPointsRemaining } ,
                    new SqlParameter("@CustomerOrderComment",SqlDbType.VarChar ){Value=CustomerOrderComment },
                    new SqlParameter("@OrderShippingTaxRate",SqlDbType.Decimal){ Value=OrderShippingTaxRate},
                    new SqlParameter("@PaymentMethodAdditionalFeeTaxRate",SqlDbType.Decimal){ Value=PaymentMethodAdditionalFeeTaxRate}  ,
                    new SqlParameter("@HasNewPaymentNotification",SqlDbType.Bit){Value=HasNewPaymentNotification },
                    new SqlParameter("@AcceptThirdPartyEmailHandOver",SqlDbType.Bit){Value=AcceptThirdPartyEmailHandOver },
                    new SqlParameter("@OrderTotalRounding",SqlDbType.Decimal ){Value =OrderTotalRounding },
                    new SqlParameter("@order_from",SqlDbType.VarChar){Value=order_from},
                    new SqlParameter("@id",SqlDbType.Int){ Direction = ParameterDirection.InputOutput }


            };
                                           
               // result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_ConfirmOrder", parameters);
                result = ClsDAL.ExecuteNonQueryWithOutPut(null, CommandType.StoredProcedure, "sp_ConfirmOrder", parameters);
                
            }
            catch (Exception e)
            {

            }
            return result;
        }



        public static long InsertOrderItemsData(int CustomerId, int ProductId, int Quantity, decimal UnitPriceInclTax ,decimal UnitPriceExclTax, decimal PriceInclTax ,
                                                 decimal PriceExclTax,decimal DiscountAmountInclTax,decimal DiscountAmountExclTax,string AttributeDescription,
                                                 string AttributesXml,int DownloadCount,bool IsDownloadActivated,int LicenseDownloadId,decimal ItemWeight,string BundleData,
                                                 decimal ProductCost,decimal TaxRate )
        {
            
            var OrderItemGuid = Guid.NewGuid().ToString();
            int @OrderId = 0;
            try
            {
                SqlParameter[] parameters = {   new SqlParameter("@CustomerId",SqlDbType.Int){Value=CustomerId},
                                                new SqlParameter("@OrderItemGuid",SqlDbType.VarChar){Value=OrderItemGuid},
                                                new SqlParameter("@OrderId",SqlDbType.Int){Value =OrderId},
                                                new SqlParameter("@ProductId",SqlDbType.Int){Value=ProductId},
                                                new SqlParameter("@Quantity",SqlDbType.Int){Value=Quantity},
                                                new SqlParameter("@UnitPriceInclTax",SqlDbType.Decimal){Value=UnitPriceInclTax},
                                                new SqlParameter("@UnitPriceExclTax",SqlDbType.Decimal){Value =UnitPriceExclTax},
                                                new SqlParameter("@PriceInclTax",SqlDbType.Decimal ){Value =PriceInclTax},
                                                new SqlParameter("@PriceExclTax",SqlDbType.Decimal ){Value =PriceExclTax},
                                                new SqlParameter("@DiscountAmountInclTax",SqlDbType.Decimal){Value =DiscountAmountInclTax},
                                                new SqlParameter("@DiscountAmountExclTax",SqlDbType.Decimal){Value =DiscountAmountExclTax},
                                                new SqlParameter("@AttributeDescription",SqlDbType.VarChar){Value =AttributeDescription},
                                                new SqlParameter("@AttributesXml",SqlDbType.VarChar){Value=AttributesXml},
                                                new SqlParameter("@DownloadCount",SqlDbType.Int ){Value=DownloadCount},
                                                new SqlParameter ("@IsDownloadActivated",SqlDbType.Bit ){Value=IsDownloadActivated},
                                                new SqlParameter("@LicenseDownloadId",SqlDbType.Int ){Value=LicenseDownloadId},
                                                new SqlParameter ("@ItemWeight",SqlDbType.Decimal){Value =ItemWeight},
                                                new SqlParameter("@BundleData",SqlDbType.VarChar){Value=BundleData},
                                                new SqlParameter("@ProductCost",SqlDbType.Decimal){Value=ProductCost},
                                                new SqlParameter("@TaxRate",SqlDbType.Decimal){Value =TaxRate}
                                               
            };

                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_InsertOrderItemData", parameters);
               
            }
            catch (Exception e)
            {

            }
            return result;
        }


        public static DataTable GetProductTaxCategory(int ProductID)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@ProductID", SqlDbType.Int) { Value = ProductID }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetProductTaxCategory", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }


        public static long RemoveFromCartAfterPlaceOrder(int CustomerId)
        {
            try
            {

                int ShoppingCartTypeId = 1; //Cart id =1
                SqlParameter[] parameters =
                {

                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = CustomerId},
                    new SqlParameter("@ShoppingCartTypeId", SqlDbType.Int) {Value = ShoppingCartTypeId}


                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_RemoveFromCartAfterPlaced", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static long UpdateProductStock(int Productid,int Qty)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@Productid",SqlDbType.Int) {Value = Productid},
                    new SqlParameter("@Qty",SqlDbType.Int ){ Value=Qty}
                   
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "Sp_UpdateProductStock", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static DataTable GetTierPrice(int ProductId)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@ProductId", SqlDbType.Int) { Value = ProductId }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetTierPrice", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable GetproductsInContactByOrderId(int id_customer,int id_order)
        {

            try
            {
                SqlParameter[] parameters = {   new SqlParameter("@id_customer",SqlDbType.Int){Value =id_customer},
                                                new SqlParameter("@id_order", SqlDbType.Int)    { Value = id_order }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_productsInContactByOrderId", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable orderReferencesByCustomerid(int id_customer)
        {

            try
            {
                SqlParameter[] parameters = {   new SqlParameter("@id_customer",SqlDbType.Int){Value =id_customer}
                                               
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_orderReferencesByCustomerid", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable Paymentmethods(int id_customer)
        {

            try
            {
                SqlParameter[] parameters = {   new SqlParameter("@id_customer",SqlDbType.Int){Value =id_customer} };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getSummary", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable Pricingdetails(int id_customer)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@id_customer", SqlDbType.Int) { Value = id_customer } };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getPriceSummary", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable GetOrderProducts(int customerid,int orderid)
        {

            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid },
                    new SqlParameter("@orderid", SqlDbType.Int) { Value = orderid }
                                            };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "SP_GetOrderProducts", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
    }
}
