﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DBLibrary
{
    public class GiftCardDB
    {
        public static long result;
        public static DataTable dt;

        //GiftCards 
        public static DataTable GetGiftCardOrders(int CustomerId,int ItemsPerPage,int PageNumber)
        {
            try
            {
              
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@CustomerId", SqlDbType.Int) { Value = CustomerId},
                    new SqlParameter("@ItemsPerPage", SqlDbType.Int) { Value = ItemsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }

                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "SP_GetGiftCardOrders", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }
        public static long AddGiftCardtoCart(  int customerId, int ProductId, string AttributesXml, int Quantity)
        {
            try
            {
                int ShoppingCartTypeId = 1;
                double CustomerEnteredPrice = 0.0;
                int StoreId = 1; int? ParentItemId = null; int? BundleItemId = null;
                DateTime CreatedOnUtc = DateTime.Now; 
                DateTime UpdatedOnUtc = DateTime.Now;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@StoreId",SqlDbType.Int) {Value = StoreId},
                    new SqlParameter("@ParentItemId",SqlDbType.Int ){ Value=ParentItemId},
                    new SqlParameter("@BundleItemId",SqlDbType.Int ){Value=BundleItemId},
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId},
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
                    new SqlParameter("@AttributesXml",SqlDbType.VarChar){Value=AttributesXml},
                    new SqlParameter("@CustomerEnteredPrice",SqlDbType.Money){Value =CustomerEnteredPrice},
                    new SqlParameter("@Quantity", SqlDbType.VarChar) {Value = Quantity},
                    new SqlParameter("@CreatedOnUtc", SqlDbType.DateTime) {Value = CreatedOnUtc},
                    new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = UpdatedOnUtc}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_AddGiftCardtoCart", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        //Delete giftcard from cart
        public static long DeleteGiftCardfromCart(int customerId,int ProductId,int Quantity)
        {
            try
            {
                String  GiftCard = "GiftCardInfo";
                SqlParameter[] parameters =
                {

                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId",SqlDbType.Int){Value=ProductId},
                    new SqlParameter("@Quantity",SqlDbType.Int ){Value=Quantity},
                    new SqlParameter("@GiftCard", SqlDbType.VarChar) {Value = GiftCard}
                    


                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_DeleteGiftCardfromCart", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }


    }
}
