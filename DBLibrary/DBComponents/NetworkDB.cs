﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
  
    
    public  class NetworkDB
    {
        public static long result;
        public static DataTable dt;
        public static DataTable GetRechargeNetworks(int serviceid,int type)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@serviceid", SqlDbType.Int) { Value = serviceid },
                new SqlParameter("@type", SqlDbType.Int) { Value = type }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getmobilenetworks", parameters).Tables[0];

            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static DataTable getAllNetworks(int serviceid)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@serviceid", SqlDbType.Int) { Value = serviceid }
                
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetAllNetworksNames", parameters).Tables[0];

            }
            catch (Exception Err)
            {

            }
            return dt;
        }
    }
}
