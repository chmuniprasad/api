﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DBLibrary
{
   public class PasswordDB
    {
        public static long result;
        public static DataTable dt = new DataTable();
        public static DataTable GetCustomerIformation(int CustomerId)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@CustomerId", SqlDbType.Int) { Value = CustomerId } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCustomerCurrentPassword", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }

        public static long UpdateNewPassword(int CustomerId, string password)
        {
            try
            {

                string PasswordSalt = Encryption.CreateSaltKey(5);
                password = Encryption.CreatePasswordHash(password, PasswordSalt, "SHA1");
                SqlParameter[] parameters =
                {
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = CustomerId},
                    new SqlParameter("@Password", SqlDbType.VarChar) {Value = password},
                    new SqlParameter("@PasswordSalt", SqlDbType.VarChar) {Value = PasswordSalt},

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_UpdateNewPassword", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static DataTable GetCustomerInformationByEamilid(string  email)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@email", SqlDbType.VarChar) { Value = email } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCustomerInfowithEmail", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }
        public static DataTable GetEmaillerData(string  mailer= "mailer")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@mailer", SqlDbType.VarChar) { Value = mailer } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetMailerInformation", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }
        public static long InsertPasswordResetToken(string token, int  customerid)
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@token",SqlDbType.VarChar ) {Value = token},
                    new SqlParameter("@customerid",SqlDbType.Int ){ Value=customerid}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "SP_insertPasswordResetToken", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static DataTable StoreInformation(string store = "Pay2cart")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@store", SqlDbType.VarChar) { Value = store } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetstoreInformation", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }

        public static DataTable GetLatestPassword(int customerid)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerid", SqlDbType.VarChar) { Value = customerid } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetLatestCustomerPassword", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }


    }
}
