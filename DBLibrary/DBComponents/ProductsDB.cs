﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class ProductsDB
    {
        public static DataTable dt = new DataTable();
        public static DataRow dr;
        public static DataTable getProducts(string categoryID, string searchText, string orderby, string orderway, int RowsPerPage, int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@categoryID", SqlDbType.VarChar) { Value = categoryID },
                                              new SqlParameter("@searchText", SqlDbType.VarChar) { Value = searchText },
                                              new SqlParameter("@orderby", SqlDbType.VarChar) { Value = orderby },
                                              new SqlParameter("@orderway", SqlDbType.VarChar) { Value = orderway },
                                               new SqlParameter("@RowsPerPage", SqlDbType.VarChar) { Value = RowsPerPage.ToString() },
                                              new SqlParameter("@PageNumber", SqlDbType.VarChar ) { Value = PageNumber.ToString() }
                                             
                     };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getProducts", parameters).Tables[0];

            }
            catch (Exception e)
            {
                

            }
            return dt;
        }

        public static DataTable getProductDetails(int productID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productID", SqlDbType.Int ) { Value = productID }
                   
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getProductDetails", parameters).Tables[0].AsEnumerable().Take(1).CopyToDataTable();
            }
            catch(Exception e)
            {

            }
            return dt;
        }

        public static DataTable getProductDetailsbyCustomer(int productID, int customerid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productID", SqlDbType.Int ) { Value = productID }
                  
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getProductDetailsbyCustomer", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getProductfromcart(int productID, int customerid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productID", SqlDbType.Int ) { Value = productID },
                new SqlParameter("@customerid", SqlDbType.Int ) { Value = customerid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getProductfromCart", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getProductimages(int productid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productid", SqlDbType.Int ) { Value = productid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getProductImages", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable ProductRating(int productid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productid", SqlDbType.Int ) { Value = productid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getproductRatings", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getProductsOfsameCategory(int CategoryId)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@CategoryId", SqlDbType.Int ) { Value = CategoryId }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_sp_getProductsOfsameCategory", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable GetProductStockinDetail(string customerid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerid", SqlDbType.VarChar) { Value = customerid }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetProductStock", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable getAllProductDetails(string productType, int ItemsPerPage,int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@productType", SqlDbType.VarChar) { Value = productType },
                    new SqlParameter("@ItemsPerPage", SqlDbType.Int) { Value = ItemsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getAllproductsDetails", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getAllProductsByCatagory(int id_category, int ItemsPerPage, int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@id_category", SqlDbType.Int) { Value = id_category },
                    new SqlParameter("@ItemsPerPage", SqlDbType.Int) { Value = ItemsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getproductsbyCategoryId", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getCategories( int ItemsPerPage, int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@ItemsPerPage", SqlDbType.Int) { Value = ItemsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCategories", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable GetProductsBysubCategory(string id,string  orderby,string  orderway, int ItemsPerPage, int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@SubCategoryId", SqlDbType.VarChar  ) { Value = id },
                    new SqlParameter("@orderby", SqlDbType.VarChar ) { Value = orderby },
                    new SqlParameter("@orderway", SqlDbType.VarChar ) { Value = orderway },
                    new SqlParameter("@ItemsPerPage", SqlDbType.VarChar ) { Value = ItemsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.VarChar ) { Value = PageNumber }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getProductbySubcategoryID", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable GetImgaeFullNames(int imgId)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@imgId", SqlDbType.Int ) { Value = imgId }
                   
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetImageFullNames", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable Getsameimagenames(string ProdcutName, string SeoFilename)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@ProdName", SqlDbType.VarChar ) { Value = ProdcutName },
                    new SqlParameter("@SeoFileName", SqlDbType.VarChar ) { Value = SeoFilename }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "Sp_getSameImagenames", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
    }
}
