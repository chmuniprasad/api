﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class WalletDB
    {
        public static float amount;
        public static long result;
        public static DataTable dt = new DataTable();

        public static DataTable getCustomerCurrentWalletBalance(int customerID)
        {

            try
            {
                 SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.Int) { Value = customerID } };
                 dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCustomerCurrentWalletBalance", parameters).Tables[0];
               
            }
            catch(Exception Err)
            {

            }
            return dt;
        }

        public static long saveWalletBalance(string userID, string orderID, string amount, string balanceAmount, int type, int serviceID, string IPAddress, string desc)
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@customerID", SqlDbType.VarChar) { Value = userID },
                    new SqlParameter("@orderID", SqlDbType.VarChar) { Value = orderID },
                    new SqlParameter("@amount", SqlDbType.VarChar) { Value = amount },
                    new SqlParameter("@balanceAmount", SqlDbType.VarChar) { Value = balanceAmount },
                    new SqlParameter("@type", SqlDbType.Int) { Value = type },
                    new SqlParameter("@serviceID", SqlDbType.Int) { Value = serviceID },
                    new SqlParameter("@IPAddress", SqlDbType.VarChar) { Value = IPAddress },
                    new SqlParameter("@desc", SqlDbType.VarChar) { Value = desc },
                     new SqlParameter("@txDate", SqlDbType.DateTime) {Value = DateTime.Now.ToShortTimeString()}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_saveWalletBalance", parameters);

            }
            catch (Exception Err)
            {

            }
            return result;
        }

        public static long createWalletOrder(int customerID, int toCustomerID, int amount, int serviceID, int type, string text)
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@customerID", SqlDbType.Int) { Value = customerID },
                    new SqlParameter("@toCustomerID", SqlDbType.Int) { Value = toCustomerID },
                    new SqlParameter("@amount", SqlDbType.Int) { Value = amount },
                    new SqlParameter("@serviceID", SqlDbType.Int) { Value = serviceID },
                    new SqlParameter("@type", SqlDbType.Int) { Value = type },
                    new SqlParameter("@text", SqlDbType.VarChar) { Value = text },
                    new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.InputOutput }
                };
                result = ClsDAL.ExecuteNonQueryWithOutPut(null, CommandType.StoredProcedure, "sp_createWalletOrder", parameters);

            }
            catch (Exception Err)
            {

            }
            return result;
        }

        public static DataTable getWalletOrderDetailsByOrderId(int OrderID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderID", SqlDbType.Int) { Value = OrderID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getWalletOrderDetailsByOrderId", parameters).Tables[0];
            }
            catch(Exception Err)
            {

            }
            return dt;
        }

        public static DataTable getWalletHistory(int customerID, int RowsPerPage, int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.Int) { Value = customerID },
                    new SqlParameter("@RowsPerPage",SqlDbType.Int){Value=RowsPerPage},
                    new SqlParameter("@PageNumber",SqlDbType.Int ){Value =PageNumber}};
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getWalletHostory", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static DataTable getRechargeHistory(int customerID, int RowsPerPage, int PageNumber)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.Int) { Value = customerID },
                 new SqlParameter("@RowsPerPage",SqlDbType.Int){Value=RowsPerPage},
                    new SqlParameter("@PageNumber",SqlDbType.Int ){Value =PageNumber}};
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getRechargeHostory", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }
        public static DataTable getSendtoMobile(int customerid,int walletOrderid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid },
                    new SqlParameter("@walletOrderid", SqlDbType.Int) { Value = walletOrderid }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getSendeMoneytoMobile", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }
        public static DataTable getreceiveFromMobile(int customerid, int walletOrderid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid },
                    new SqlParameter("@walletOrderid", SqlDbType.Int) { Value = walletOrderid }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getReceiveFromtoMobile", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static DataTable checkWalletOrderExists(int orderId, int serviceID)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@txOrderID", SqlDbType.Int) { Value = orderId },
                                              new SqlParameter("@txServiceID ", SqlDbType.Int) { Value = serviceID }

                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "Sp_check_WalletOrderID", parameters).Tables[0];

            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        

    }
}
