﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DBLibrary
{
   
    public  class WishListDB
        
    {
        public static long result;
        public static DataTable dt;

        //Wish list
        public static DataTable GetWhishlisttemsDetailsByCustomerID(int custometid, int shopingcarttype,int ItemsPerPage,int PageNumber)
        {
            try
            {
                shopingcarttype = 2;
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@customerID", SqlDbType.Int) { Value = custometid },
                    new SqlParameter("@ShoppingCartTypeId", SqlDbType.Int) { Value = shopingcarttype },
                     new SqlParameter("@ItemsPerPage", SqlDbType.Int) { Value = ItemsPerPage },
                      new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }
                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_WishlistItems_Get", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }
        public static long UpdateWishlistItems( int customerId, int ProductId, int Quantity)
        {
            try
            {
                //ShoppingCartTypeId 
                int ShoppingCartTypeId = 2;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId},
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
                    new SqlParameter("@Quantity", SqlDbType.VarChar) {Value = Quantity}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_UpdateWishlistItemsItems", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        //public static long AddItemstoIshlist(int StoreId, int ParentItemId, int BundleItemId, int ShoppingCartTypeId, int customerId, int ProductId, string AttributesXml, double CustomerEnteredPrice, int Quantity, DateTime CreatedOnUtc, DateTime UpdatedOnUtc)
        //{
        //    try
        //    {
        //        ShoppingCartTypeId = 2;
        //        SqlParameter[] parameters =
        //        {
        //            new SqlParameter("@StoreId",SqlDbType.Int) {Value = StoreId},
        //            new SqlParameter("@ParentItemId",SqlDbType.Int ){ Value=ParentItemId},
        //            new SqlParameter("@BundleItemId",SqlDbType.Int ){Value=BundleItemId},
        //            new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId},
        //            new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
        //            new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
        //            new SqlParameter("@AttributesXml",SqlDbType.VarChar){Value=AttributesXml},
        //            new SqlParameter("@CustomerEnteredPrice",SqlDbType.Money){Value =CustomerEnteredPrice},
        //            new SqlParameter("@Quantity", SqlDbType.VarChar) {Value = Quantity},
        //            new SqlParameter("@CreatedOnUtc", SqlDbType.DateTime) {Value = DateTime.Now.ToShortTimeString()},
        //            new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = DateTime.Now.ToShortTimeString()}

        //        };
        //        result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_AddItemstoWishlist", parameters);

        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return result;
        //}

        public static long AddItemstoIshlist(int customerId, int? ParentItemId, int? BundleItemId, int ProductId, string AttributesXml, int Quantity)
        {
            try
            {

                int StoreId = 1;

                double CustomerEnteredPrice = 0.00;
                DateTime CreatedOnUtc = DateTime.Now;
                DateTime UpdatedOnUtc = DateTime.Now;
                int ShoppingCartTypeId = 2;// idicates that Wishlist
                SqlParameter[] parameters =
                {
                    new SqlParameter("@StoreId",SqlDbType.Int) {Value = StoreId},
                    new SqlParameter("@ParentItemId",SqlDbType.Int ){ Value=ParentItemId},
                    new SqlParameter("@BundleItemId",SqlDbType.Int ){Value=BundleItemId},
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId},
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
                    new SqlParameter("@AttributesXml",SqlDbType.VarChar){Value=AttributesXml},
                    new SqlParameter("@CustomerEnteredPrice",SqlDbType.Money){Value =CustomerEnteredPrice},
                    new SqlParameter("@Quantity", SqlDbType.VarChar) {Value = Quantity},
                    new SqlParameter("@CreatedOnUtc", SqlDbType.DateTime) {Value = CreatedOnUtc},
                    new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = UpdatedOnUtc}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_AddItemstoWishlist", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static long DeleteWishlistItems( int customerId, int ProductId)
        {
            try
            {
               int ShoppingCartTypeId = 2;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@ShoppingCartTypeId", SqlDbType.Int) {Value = ShoppingCartTypeId},
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId}
                   


                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_DeleteWishlistItems", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }


    }
}
