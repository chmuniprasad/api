﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class CategoryDB
    {
        public static DataTable dt = new DataTable();

        public static DataTable getAllCategories(int categoryID = 0)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@categoryID", SqlDbType.BigInt) { Value = categoryID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getAllCategories", parameters).Tables[0];

            }
            catch (Exception e)
            {

            }
            return dt;
        }

    }
}
