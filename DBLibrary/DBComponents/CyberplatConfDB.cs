﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class CyberplatConfDB
    {
        public static DataTable dt = new DataTable();

        public static DataTable getCyberPlatConf(string configID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@configID", SqlDbType.VarChar) { Value = configID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCyberplatConf", parameters).Tables[0];
            }
            catch(Exception Err)
            {

            }
            return dt;
        }
    }
}
