﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class RechargeBillPaymentsDB
    {
        public static long result;
        public static DataTable dt = new DataTable();

        public static long saveRechargeAndBillPaymentOrder(string customerID, string orderTypeID, string serviceID, string amount, string IpAddress)
        {

            try
            {
                DateTime createdAt = DateTime.Now;
                SqlParameter[] parameters =
                {

                    new SqlParameter("@customerID", SqlDbType.VarChar) {Value = customerID},
                    new SqlParameter("@orderTypeID", SqlDbType.VarChar) {Value = orderTypeID},
                    new SqlParameter("@serviceID", SqlDbType.VarChar) {Value = serviceID},
                    new SqlParameter("@amount", SqlDbType.VarChar) {Value = amount},
                    new SqlParameter("@IpAddress", SqlDbType.VarChar) {Value = IpAddress},
                    new SqlParameter("@id", SqlDbType.Int) {Direction = ParameterDirection.InputOutput },
                    new SqlParameter("createdAt",SqlDbType.DateTime){Value =createdAt}
            };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_saveRechargeAndBillPaymentOrder", parameters);
                
            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static long saveMobileRechargeData(string OrderID, string networkType, string network, string mobileNumber)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@OrderID", SqlDbType.VarChar) {Value = OrderID},
                    new SqlParameter("@networkType", SqlDbType.VarChar) {Value = networkType},
                    new SqlParameter("@network", SqlDbType.VarChar) {Value = network},
                    new SqlParameter("@mobileNumber", SqlDbType.VarChar) {Value = mobileNumber}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_saveMobileRechargeData", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static long updateRechargeOrdersData(string OrderID, string error, string res, string rechargeStatus, string cert, string Paydate, string TxID)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@OrderID", SqlDbType.VarChar) {Value = OrderID},
                    new SqlParameter("@error", SqlDbType.VarChar) {Value = error},
                    new SqlParameter("@result", SqlDbType.VarChar) {Value = res},
                    new SqlParameter("@rechargeStatus", SqlDbType.VarChar) {Value = rechargeStatus},
                    new SqlParameter("@cert", SqlDbType.VarChar) {Value = cert},
                    new SqlParameter("@TxID", SqlDbType.VarChar) {Value = TxID}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_updateRechargeOrdersData", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static DataTable getCurrentCustomerRBOrderID(string customerID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.VarChar) { Value = customerID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCurrentCustomerRBOrderID", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static long saveDthRechargeData(string OrderID, string operatorId, string cardNumber)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@OrderID", SqlDbType.VarChar) {Value = OrderID},
                    new SqlParameter("@operatorId", SqlDbType.VarChar) {Value = operatorId},
                    new SqlParameter("@cardNumber", SqlDbType.VarChar) {Value = cardNumber}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_saveDthRechargeData", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static long saveElectricityBillPaymentData(string OrderID, string boardNumber, string customerNumber)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@OrderID", SqlDbType.VarChar) {Value = OrderID},
                    new SqlParameter("@boardNumber", SqlDbType.VarChar) {Value = boardNumber},
                    new SqlParameter("@customerNumber", SqlDbType.VarChar) {Value = customerNumber}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_saveElectricityBillPaymentData", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static long saveGasBillPaymentData(string OrderID, string operatorCode, string customerNumber, string billgroupNumber)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@OrderID", SqlDbType.VarChar) {Value = OrderID},
                    new SqlParameter("@operatorCode", SqlDbType.VarChar) {Value = operatorCode},
                    new SqlParameter("@customerNumber", SqlDbType.VarChar) {Value = customerNumber},
                    new SqlParameter("@billgroupNumber", SqlDbType.VarChar) {Value = billgroupNumber}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_saveGasBillPaymentData", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static DataTable checkUserDailyMonthlyTrasctions(int CustomerId, int isDaily,int txType)
        {
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@CustomerId", SqlDbType.VarChar) { Value = CustomerId },
                                                new SqlParameter("@isDaily", SqlDbType.VarChar) { Value = isDaily },
                                                new SqlParameter("@txType", SqlDbType.VarChar) { Value = txType }
                                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_checkUserDailyMonthlyTrasctions", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static DataTable getRechargeOrdersByID(int OrderID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderID", SqlDbType.Int) { Value = OrderID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getRechargeOrdersById", parameters).Tables[0];
            }
            catch (Exception Err)
            {

            }
            return dt;
        }
    }
}
