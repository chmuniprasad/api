﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DBLibrary
{
   public class HomePageDB
    {
        public static long result;
        public static DataTable dt;

        public static DataTable GetHomepageBanners(string Banners= "Banners")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Banners", SqlDbType.VarChar) { Value = Banners }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetBanners", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable GetHomepageCatetories(string Categories = "Categories")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Categories", SqlDbType.VarChar) { Value = Categories }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetHomePageCategories", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable shopbyCategories(string Categories = "ShopbyCategories")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Categories", SqlDbType.VarChar ) { Value = Categories }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_ShopByCategories", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable newproductlist1(string newproducts = "newproducts")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@newproducts", SqlDbType.VarChar ) { Value = newproducts }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getnewproductlist1", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable newproductlist2(string newproduct2 = "newproduct2")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@newproduct2", SqlDbType.VarChar ) { Value = newproduct2 }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getnewproductlist2", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable CheckProductDelivery(string zipcode, int id_product)
        {

            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@zipcode", SqlDbType.VarChar ) { Value = zipcode },
                    new SqlParameter("@id_product", SqlDbType.VarChar ) { Value = id_product }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CheckDeliverybyPostCode", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable activeServices(string activeServices = "activeServices")
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@activeServices", SqlDbType.VarChar) { Value = activeServices }
                                            };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_activeServices", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable GetStates(int RowsPerPage,int PageNumber)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@RowsPerPage", SqlDbType.Int) { Value = RowsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }
                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetStates", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;
        }
        public static DataTable ShippingCarrier(int id_customer)
        {
            try
            {
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@id_customer", SqlDbType.Int) { Value = id_customer }
                   
                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getShippingCarriers", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;
        }
       
    }
}
