﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace DBLibrary
{
    public class ReviewsDB
    {
        public static long result;
        public static DataTable dt;
        public static long WriteProductReviews(int ProductId,string Title,string ReviewText,int Rating,int HelpfulYesTotal,int HelpfulNoTotal, int CustomerId,string IpAddress,bool IsApproved, DateTime CreatedOnUtc, DateTime UpdatedOnUtc)
        {
            try
            {
              
                SqlParameter[] parameters =
                {
                    
                    new SqlParameter("@ProductId",SqlDbType.Int) {Value = ProductId},
                    new SqlParameter("@Title",SqlDbType.NVarChar ){ Value=Title},
                    new SqlParameter("@ReviewText",SqlDbType.NVarChar ){Value=ReviewText},
                    new SqlParameter("@Rating",SqlDbType.Int){Value=Rating},
                    new SqlParameter("@HelpfulYesTotal", SqlDbType.Int) {Value = HelpfulYesTotal},
                    new SqlParameter("@HelpfulNoTotal",SqlDbType.VarChar){Value=HelpfulNoTotal},
                    new SqlParameter("@CustomerID",SqlDbType.Int){Value=CustomerId},
                    new SqlParameter("@IpAddress",SqlDbType.NVarChar){Value=IpAddress},
                    new SqlParameter("@IsApproved",SqlDbType.Bit){Value=IsApproved},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc},
                    new SqlParameter("@UpdatedOnUtc",SqlDbType.DateTime){Value=UpdatedOnUtc}

                };
               
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_WriteProductReview", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static DataTable GetProductReviews(int ProductID, int ItemsPerPage,int PageNumber)
        {
            try
            {
               
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@ProductID", SqlDbType.Int) { Value = ProductID },
                     new SqlParameter("@ItemsPerPage",SqlDbType.Int){Value =ItemsPerPage},
                    new SqlParameter("@PageNumber",SqlDbType.Int){Value =PageNumber}
                    
                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetProductReviews", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }
        public static DataTable GetCustomerName(int id_customer)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                    new SqlParameter("@id_customer",SqlDbType.Int ){Value =id_customer}
                   
                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCustomerFullname", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }
        public static DataTable ReviewCheckStatus(int customerid, int reviewid)
        {
            try
            {
                dt = null;
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@customerid",SqlDbType.Int ){Value =customerid},
                    new SqlParameter("@reviewid",SqlDbType.Int ){Value =reviewid}

                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCustomerRecviewCheckStatus", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }
        public static DataTable GetCustomerRole(int CustomerID)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                    new SqlParameter("@CustomerID", SqlDbType.Int) { Value = CustomerID }

                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCustomerRole", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }

        public static DataTable GetProductReviewsByCustomer(int CustomerID,int ItemsPerPage,int PageNumber)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                    new SqlParameter("@CustomerID", SqlDbType.Int) { Value = CustomerID },
                    new SqlParameter("@ItemsPerPage", SqlDbType.Int) { Value = ItemsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }

                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetReviewsByCustomerID", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }

        public static long ProductReviewHelpfulYES(int CustomerId, string IpAddress, bool IsApproved, DateTime CreatedOnUtc, DateTime UpdatedOnUtc, int ProductReviewId,bool is_useful)
        {
            try
            {
                bool WasHelpful = is_useful;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@CustomerId",SqlDbType.Int) {Value = CustomerId},
                    new SqlParameter("@IpAddress",SqlDbType.VarChar ){ Value=IpAddress},
                    new SqlParameter("@IsApproved",SqlDbType.Bit ){Value=IsApproved},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc},
                    new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = UpdatedOnUtc},
                    new SqlParameter("@ProductReviewId", SqlDbType.Int ) {Value = ProductReviewId},
                    new SqlParameter("@WasHelpful",SqlDbType.Bit){Value=WasHelpful}


                };

                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_productreviewHelpfulYES", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static long ProductReviewHelpfulNo(int CustomerId, string IpAddress, bool IsApproved, DateTime CreatedOnUtc, DateTime UpdatedOnUtc, int ProductReviewId)
        {
            try
            {
                bool WasHelpful = false;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@CustomerId",SqlDbType.Int) {Value = CustomerId},
                    new SqlParameter("@IpAddress",SqlDbType.VarChar ){ Value=IpAddress},
                    new SqlParameter("@IsApproved",SqlDbType.Bit ){Value=IsApproved},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc},
                    new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = UpdatedOnUtc},
                    new SqlParameter("@ProductReviewId", SqlDbType.Int ) {Value = ProductReviewId},
                    new SqlParameter("@WasHelpful",SqlDbType.Bit){Value=WasHelpful}


                };

                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_productreviewHelpfulNO", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static DataTable ProductReviewHelpfulYesNO(int reviewId)
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@reviewId",SqlDbType.Int){Value=reviewId}
                };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getreivewusefulYesNo", parameters).Tables[0];

            }
            catch (Exception e)
            {
            }
            return dt;
        }
    }
}
