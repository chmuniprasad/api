﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DBLibrary
{
   public  class BillingAddressDB
    {
        public static long result;
        public static DataTable dt;
        //Customer Address

        //Getting the Billing Address from Database
        public static DataTable GetCustomerBillingAddresbyCustomerID(int custometid,int RowsPerPage,int PageNumber)
        {
            try
            {

                SqlParameter[] parameters =
                    {
                    new SqlParameter("@customerID", SqlDbType.Int) { Value = custometid },
                    new SqlParameter("@RowsPerPage", SqlDbType.Int) { Value = RowsPerPage },
                    new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }

                    };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCustomerBillingAddresByCustomerID", parameters).Tables[0];
            }
            catch (Exception e)
            { }
            return dt;

        }

        //Adding New Billing Address for the Customer 
        public static long AddCustomerBillingAddress(string FirstName, string LastName,  int CountryId, int StateProvinceId,
            string City, string Address1, string Address2, string ZipPostalCode, string PhoneNumber,
            int Customerid)
        {
            try
            {
                string FaxNumber = ""; DateTime CreatedOnUtc = DateTime.Now; string Salutation= ""; string Company = ""; string Title = "";
                string Email = "";
                SqlParameter[] parameters =
                {
                    new SqlParameter("@FirstName",SqlDbType.VarChar ) {Value = FirstName},
                    new SqlParameter("@LastName",SqlDbType.VarChar ){ Value=LastName},
                    new SqlParameter("@Email",SqlDbType.VarChar ){Value=Email},
                    new SqlParameter("@Company",SqlDbType.VarChar){Value=Company},
                    new SqlParameter("@CountryId", SqlDbType.Int) {Value = CountryId},
                    new SqlParameter("@StateProvinceId", SqlDbType.Int) {Value = StateProvinceId},
                    new SqlParameter("@City",SqlDbType.VarChar){Value=City},
                    new SqlParameter("@Address1",SqlDbType.VarChar){Value =Address1},
                    new SqlParameter("@Address2", SqlDbType.VarChar) {Value = Address2},
                    new SqlParameter("@ZipPostalCode", SqlDbType.VarChar ) {Value = ZipPostalCode},
                    new SqlParameter("@PhoneNumber", SqlDbType.VarChar) {Value = PhoneNumber},
                    new SqlParameter("@FaxNumber",SqlDbType.VarChar){Value=FaxNumber},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc},
                    new SqlParameter("@Salutation",SqlDbType.VarChar){Value=Salutation},
                    new SqlParameter("@Title",SqlDbType.VarChar){Value=Title},
                    new SqlParameter("@Customerid",SqlDbType.Int ){Value=Customerid}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_AddCustomerBillingAddress", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        //Updating the Billing Address of Customer 
        public static long UpdateCustomerBillingAddress(string FirstName, string LastName,  int CountryId, int StateProvinceId,
            string City, string Address1, string Address2, string ZipPostalCode, string PhoneNumber, int Customerid, int Address_Id)
        {
            try
            {
                string FaxNumber = ""; DateTime CreatedOnUtc = DateTime.Now; string Salutation = ""; string Company = ""; string Title = "";
                string Email = "";
                SqlParameter[] parameters =
                {
                   new SqlParameter("@FirstName",SqlDbType.VarChar ) {Value = FirstName},
                    new SqlParameter("@LastName",SqlDbType.VarChar ){ Value=LastName},
                    new SqlParameter("@Email",SqlDbType.VarChar ){Value=Email},
                    new SqlParameter("@Company",SqlDbType.VarChar){Value=Company},
                    new SqlParameter("@CountryId", SqlDbType.Int) {Value = CountryId},
                    new SqlParameter("@StateProvinceId", SqlDbType.Int) {Value = StateProvinceId},
                    new SqlParameter("@City",SqlDbType.VarChar){Value=City},
                    new SqlParameter("@Address1",SqlDbType.VarChar){Value =Address1},
                    new SqlParameter("@Address2", SqlDbType.VarChar) {Value = Address2},
                    new SqlParameter("@ZipPostalCode", SqlDbType.VarChar ) {Value = ZipPostalCode},
                    new SqlParameter("@PhoneNumber", SqlDbType.VarChar) {Value = PhoneNumber},
                    new SqlParameter("@FaxNumber",SqlDbType.VarChar){Value=FaxNumber},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc},
                    new SqlParameter("@Salutation",SqlDbType.VarChar){Value=Salutation},
                    new SqlParameter("@Title",SqlDbType.VarChar){Value=Title},
                    new SqlParameter("@Customerid",SqlDbType.Int ){Value=Customerid},
                    new SqlParameter("@Address_ID",SqlDbType.Int){Value=Address_Id}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_UpdateCustomerBillingAddress", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        //Delete the Billing Address of Customer 
        public static long DeleteCustomerBillingAddress(int CustomerID, int Address_ID)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@Customer_Id", SqlDbType.Int) {Value = CustomerID},
                    new SqlParameter("@Address_Id", SqlDbType.VarChar) {Value = Address_ID}


                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_DeleteCustomerBillingAddress", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }


    }
}
