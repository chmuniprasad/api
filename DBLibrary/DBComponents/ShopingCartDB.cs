﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DBLibrary
{
   public  class ShopingCartDB
    {
        public static long result;
        public static DataTable dt;
     
        public static long AddItemstoCart(  int customerId, int? ParentItemId, int? BundleItemId, int ProductId, string AttributesXml, int Quantity)
        {
            try
            {

                int StoreId = 1;
              
                double CustomerEnteredPrice = 0.00;
                DateTime CreatedOnUtc = DateTime.Now;
                DateTime UpdatedOnUtc = DateTime.Now;
                int ShoppingCartTypeId = 1;// idicates that Shoping Cart
                SqlParameter[] parameters =
                {
                    new SqlParameter("@StoreId",SqlDbType.Int) {Value = StoreId},
                    new SqlParameter("@ParentItemId",SqlDbType.Int ){ Value=ParentItemId},
                    new SqlParameter("@BundleItemId",SqlDbType.Int ){Value=BundleItemId},
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId},
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
                    new SqlParameter("@AttributesXml",SqlDbType.VarChar){Value=AttributesXml},
                    new SqlParameter("@CustomerEnteredPrice",SqlDbType.Money){Value =CustomerEnteredPrice},
                    new SqlParameter("@Quantity", SqlDbType.VarChar) {Value = Quantity},
                    new SqlParameter("@CreatedOnUtc", SqlDbType.DateTime) {Value = CreatedOnUtc},
                    new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = UpdatedOnUtc}

                };
                 result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_AddShopingCartItems", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static long UpdateCartItems( int customerId, int ProductId, int Quantity)
        {
            try
            {
                DateTime UpdatedOnUtc = DateTime.Now;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
                    new SqlParameter("@Quantity", SqlDbType.VarChar) {Value = Quantity},
                    new SqlParameter("@UpdatedOnUtc", SqlDbType.DateTime) {Value = DateTime.Now.ToShortTimeString()}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_UpdateShopingCartItems", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static long DeleteCartItems(int customerId, int ProductId)
        {
            try
            {

                int ShoppingCartTypeId = 1;// idicates that Shoping Cart

                SqlParameter[] parameters =
                {

                    new SqlParameter("@CustomerId", SqlDbType.Int) {Value = customerId},
                    new SqlParameter("@ProductId", SqlDbType.VarChar) {Value = ProductId},
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId}


                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_DeleteShopingCartItems", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static DataTable GetCartItemsDetailsByCustomerID(int customerId, int ItemsPerPage, int PageNumber)
        {
            try
            {
                int ShoppingCartTypeId = 1;// idicates that Shoping Cart
                SqlParameter[] parameters = {
                    new SqlParameter("@customerID", SqlDbType.Int) { Value = customerId },
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId},
                    new SqlParameter("@ItemsPerPage",SqlDbType.Int){Value=ItemsPerPage},
                    new SqlParameter("@PageNumber",SqlDbType.Int){Value=PageNumber}
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_ShopingCartItems_Get", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }
        public static DataTable AddShopingCartItem(int customerId,int productid)
        {
            try
            {
               
                SqlParameter[] parameters = {
                    new SqlParameter("@customerId", SqlDbType.Int) { Value = customerId },
                    new SqlParameter("@productid",SqlDbType.Int){Value=productid}
                   
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_checkproductavailableatCART", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }

        public static DataTable GetMaxProductQtyAllow(int ProductId)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productID", SqlDbType.Int) { Value = ProductId } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetProductMaxQtyOrder", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }
//08.02.2018
        public static DataTable CheckProductIncludesBundle(int ProductId)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@productID", SqlDbType.Int) { Value = ProductId } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_checkproductBundleitems", parameters).Tables[0];
            }
            catch (Exception e)
            {


            }
            return dt;
        }
        public static DataTable GetProductParentItemId(int ProductId,int CustomerId)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@ProductId", SqlDbType.Int) { Value = ProductId },
                    new SqlParameter("@CustomerId", SqlDbType.Int) { Value = CustomerId }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetProductParentId", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }
        public static DataTable CheckCustomerIsvalid(int CustomerId)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@CustomerId", SqlDbType.Int) { Value = CustomerId }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CheckCustomerIsvalid", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }
        public static DataTable GetProductAttribuuteXml(int ProductId, int VariantId)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@ProductId", SqlDbType.Int) { Value = ProductId },
                    new SqlParameter("@VariantId", SqlDbType.Int) { Value = VariantId }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetProdcutAttributeXml", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }

        public static DataTable GetCartItemsCount(int customerId)
        {
            try
            {
                int ShoppingCartTypeId = 1;// idicates that Shoping Cart
                SqlParameter[] parameters = {
                    new SqlParameter("@customerID", SqlDbType.Int) { Value = customerId },
                    new SqlParameter("@ShoppingCartTypeId",SqlDbType.Int){Value=ShoppingCartTypeId}
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_ShopingCartItemsCount", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }

        public static DataTable getproductdetails(int customerid, int productid)
        {
            try
            {
               
                SqlParameter[] parameters = {
                    new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid },
                    new SqlParameter("@productid",SqlDbType.Int){Value=productid}
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getproductCartdetails", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }
        public static DataTable cartTotal(int customerid)
        {
            try
            {

                SqlParameter[] parameters = {
                    new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid }
                  
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCartTotal", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;

        }
        public static DataTable GetCartdetails(int customerid)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid }
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getCustomerCartdetails", parameters).Tables[0];
            }
            catch (Exception e)
            {
            }
            return dt;
        }
        public static long InsertGuestId(bool IsSystemAccount)
  
        {
            var CustomerGuid = Guid.NewGuid().ToString();
            int PasswordFormatId = 0;
            bool IsTaxExempt = false;
            int AffiliateId = 0;
            bool Active = true;
            bool Deleted = false;
            //bool IsSystemAccount = false;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@CustomerGuid",SqlDbType.VarChar){Value=CustomerGuid},
                    new SqlParameter("@PasswordFormatId",SqlDbType.Int ){Value=PasswordFormatId},
                    new SqlParameter("@IsTaxExempt",SqlDbType.Bit ){Value=IsTaxExempt},
                    new SqlParameter("@AffiliateId",SqlDbType.Int ){Value=AffiliateId},
                    new SqlParameter("@Active",SqlDbType.Bit ){Value=Active},
                    new SqlParameter("@Deleted",SqlDbType.Bit  ){Value=Deleted},
                    new SqlParameter("@IsSystemAccount",SqlDbType.Bit ){Value=IsSystemAccount},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime ){Value=DateTime.Now.ToString()},
                    new SqlParameter("@LastActivityDateUtc",SqlDbType.DateTime ){Value=DateTime.Now.ToString()},
                    new SqlParameter("@Id",SqlDbType.Int){ Direction = ParameterDirection.InputOutput }


            };

                // result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_ConfirmOrder", parameters);
                result = ClsDAL.ExecuteNonQueryWithOutPut(null, CommandType.StoredProcedure, "sp_InsertGuestId", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }





    }
}
