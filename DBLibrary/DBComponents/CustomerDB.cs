﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DBLibrary
{
    public class CustomerDB
    {
        public static long result;
        public static DataTable dt = new DataTable();

        public static long addCustomer(string email, string password, string phoneNumber)
        {
            try
            {
                var guid = Guid.NewGuid().ToString();
                string PasswordSalt =Encryption.CreateSaltKey(5);
                password = Encryption.CreatePasswordHash(password, PasswordSalt, "SHA1");
                bool active = false;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@Username", SqlDbType.VarChar) {Value = email},
                    new SqlParameter("@Email", SqlDbType.VarChar) {Value = email},
                    new SqlParameter("@Password", SqlDbType.VarChar) {Value = password},
                    new SqlParameter("@CustomerGuid", SqlDbType.VarChar) {Value = guid},
                    new SqlParameter("@PasswordSalt", SqlDbType.VarChar) {Value = PasswordSalt},
                    new SqlParameter("@CreatedOnUtc", SqlDbType.DateTime) {Value = DateTime.Now.ToString()},
                    new SqlParameter("@mobile",SqlDbType.VarChar){Value =phoneNumber},
                    new SqlParameter("@last_passwd_gen",SqlDbType.DateTime){Value =DateTime.Now.ToString()},
                    new SqlParameter("@active",SqlDbType.Bit){Value=active},
                    new SqlParameter("@id",SqlDbType.Int){ Direction = ParameterDirection.InputOutput }
                };
               // result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_createCustomer", parameters);
                result = ClsDAL.ExecuteNonQueryWithOutPut(null, CommandType.StoredProcedure, "sp_createCustomer", parameters);

            }
            catch(Exception e)
            {
                
            }
            return result;
        }
        public static long UpdateOtpValidation(int userId, int isActive, int isDeleted)
        {
            try
            {
                DateTime CreatedOnUtc = DateTime.Now;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@userId", SqlDbType.Int) {Value = userId},
                    new SqlParameter("@isActive", SqlDbType.Int ) {Value = isActive},
                    new SqlParameter("@isDeleted", SqlDbType.Int) {Value = isDeleted},
                    new SqlParameter("@CreatedOnUtc",SqlDbType.DateTime){Value=CreatedOnUtc}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "Sp_UpdateCustomerValidation", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static long UpdateCustomerRole(int customer, int role)
        {
            try
            {
                DateTime CreatedOnUtc = DateTime.Now;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@customer", SqlDbType.Int) {Value = customer},
                    new SqlParameter("@role", SqlDbType.Int ) {Value = role},
                    
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_insertcustomerRole", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static DataTable GetOtpbyUserid(int UserId)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@UserId", SqlDbType.Int) { Value = UserId }

                                          };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "SP_GetOtpbyUserId", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static long InsertOtp(string otp, string purpose, int userId, int isActive, int isDeleted, DateTime createdAt, DateTime updatedAt)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@otp", SqlDbType.Int) {Value = otp},
                    new SqlParameter("@purpose", SqlDbType.VarChar) {Value = purpose},
                    new SqlParameter("@userId", SqlDbType.Int) {Value = userId},
                    new SqlParameter("@isActive", SqlDbType.Int ) {Value = isActive},
                    new SqlParameter("@isDeleted", SqlDbType.Int) {Value = isDeleted},
                    new SqlParameter("@createdAt",SqlDbType.DateTime){Value=createdAt},
                    new SqlParameter("@updatedAt",SqlDbType.DateTime){Value=updatedAt}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "SP_newOtp", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static DataTable getcustomerbyEmail(string email)
        {
            try
            {
                
                SqlParameter[] parameters =
                {

                    new SqlParameter("@Email", SqlDbType.VarChar) {Value = email}
                  
                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getcustomerbyemail", parameters).Tables[0];

            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable getcustomerbyphone(string mobile)
        {
            try
            {
                SqlParameter[] parameters =
                {

                    new SqlParameter("@mobile", SqlDbType.VarChar) {Value = mobile}

                };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getcustomerbymobile", parameters).Tables[0];

            }
            catch (Exception e)
            {

            }
            return dt;
        }
        public static DataTable login(string userName, string password)
        {
            try
            {
                dt = null;

                SqlParameter[] parameters = {
                        new SqlParameter("@userName ", SqlDbType.VarChar, 50) { Value = userName },
                        new SqlParameter("@password ", SqlDbType.VarChar, 50) { Value = password }
                };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_customerLogin", parameters).Tables[0].AsEnumerable().Take(1).CopyToDataTable();

            }
            catch(Exception e)
            {

            }

            return dt;
        }
        public static DataTable ChangePassword(string userName, string password)
        {
            try
            {
                dt = null;

                SqlParameter[] parameters = {
                        new SqlParameter("@userName ", SqlDbType.VarChar, 50) { Value = userName },
                        new SqlParameter("@password ", SqlDbType.VarChar, 50) { Value = password }
                };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_customerLoginChangePassword", parameters).Tables[0].AsEnumerable().Take(1).CopyToDataTable();

            }
            catch (Exception e)
            {

            }

            return dt;
        }
        public static DataTable RegisterDetails(string email, string mobile)
        {
            try
            {
                SqlParameter[] parameters = {
                        new SqlParameter("@email ", SqlDbType.VarChar, 50) { Value = email },
                        new SqlParameter("@mobile ", SqlDbType.VarChar, 50) { Value = mobile }
                };

                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_customerRegisterDetails", parameters).Tables[0];

            }
            catch (Exception e)
            {

            }

            return dt;
        }

        public static DataTable getCustomerDetailsByUserName(string phoneNumber)
        {
            try
            {
                dt = null;
                SqlParameter[] parameters = { 
                                              new SqlParameter("@phoneNumber", SqlDbType.VarChar) { Value = phoneNumber }
                                          };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CustomerDetailsByUserName", parameters).Tables[0];
            }
            catch(Exception e)
            {

            }
            return dt;
        }
        public static DataTable getCustomerDetailsByUser(string userName,bool MobileOrEmail)
        {
            try
            {
                dt = null;
                SqlParameter[] parameters = { new SqlParameter("@userName", SqlDbType.VarChar) { Value = userName },
                new SqlParameter("@MobileOrEmail", SqlDbType.Bit) { Value = MobileOrEmail }

                                          };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CustomerDetailsByUser", parameters).Tables[0];
            }
            catch (Exception e)
            {

            }
            return dt;
        }

        public static DataTable getCustomerDetailsByCustomerID(int customerID)
        {

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.Int) { Value = customerID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CustomerDetailsByCustomerID", parameters).Tables[0];

            }
            catch (Exception e)
            {
                

            }
            return dt;
        }

        public static DataTable getWalletHistory(int customerID)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@customerID", SqlDbType.Int) { Value = customerID } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_CustomerDetailsByCustomerID", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }

        public static long UpdateCustomerProfile(int CustomerID, string Gender, string FirstName, string LastName, string DateOfBirth, string Company, string City, string Phone, string Signature, string VatNumber)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@CustomerID",SqlDbType.Int) {Value = CustomerID},
                    new SqlParameter("@Gender",SqlDbType.VarChar ){ Value=Gender},
                    new SqlParameter("@FirstName",SqlDbType.VarChar ){Value=FirstName},
                    new SqlParameter("@LastName",SqlDbType.VarChar){Value=LastName},
                    new SqlParameter("@DateOfBirth", SqlDbType.VarChar) {Value = DateOfBirth},
                    new SqlParameter("@Company", SqlDbType.VarChar) {Value = Company},
                    new SqlParameter("@City",SqlDbType.VarChar){Value=City},
                    new SqlParameter("@Phone",SqlDbType.VarChar){Value =Phone},
                    new SqlParameter("@Signature", SqlDbType.VarChar) {Value = Signature},
                    new SqlParameter("@VatNumber", SqlDbType.VarChar) {Value = VatNumber}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_UpdateProfile", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static long updatecustomerImage(int customerid, string imagename)
        {
            try
            {

                SqlParameter[] parameters =
                {

                    new SqlParameter("@customerid",SqlDbType.Int) {Value = customerid},
                    new SqlParameter("@imagename",SqlDbType.VarChar ){ Value=imagename}
                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_updatecustomerImage", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
        public static DataTable GetRegisteredCustomers(int customer)
        {

            try
            {
               

                SqlParameter[] parameters = { new SqlParameter("@customerRole", SqlDbType.Int) { Value =customer } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetRegisteredCustomers", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }
        public static DataTable getcustomerotp(int customer)
        {

            try
            {


                SqlParameter[] parameters = { new SqlParameter("@customer", SqlDbType.Int) { Value = customer } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_getcustomerotp", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }
        public static DataTable CustomerProfileDetails(int customerid)
        {

            try
            {
                dt = null;

                SqlParameter[] parameters = { new SqlParameter("@customerid", SqlDbType.Int) { Value = customerid } };
                dt = ClsDAL.ExecuteDataset(null, CommandType.StoredProcedure, "sp_GetCustomerProfile", parameters).Tables[0];

            }
            catch (Exception e)
            {


            }
            return dt;
        }

        public static long UpdateCustomerData(int id_customer, int id_gender, string firstname, string lastname, string birthday)
        {
            try
            {
                string date = birthday;
                DateTime dob = Convert.ToDateTime(date);
                SqlParameter[] parameters =
                {

                    new SqlParameter("@id_customer",SqlDbType.Int) {Value = id_customer},
                    new SqlParameter("@id_gender",SqlDbType.Int  ){ Value=id_gender},
                    new SqlParameter("@firstname",SqlDbType.VarChar ){Value=firstname},
                    new SqlParameter("@lastname",SqlDbType.VarChar){Value=lastname},
                    new SqlParameter("@birthday", SqlDbType.DateTime) {Value = dob}

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "sp_updateCustomerData", parameters);

            }
            catch (Exception e)
            {

            }
            return result;
        }
    }
}
