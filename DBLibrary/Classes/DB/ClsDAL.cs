﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Runtime.Remoting.Messaging;


namespace DBLibrary
{
    public sealed class ClsDAL
    {

        #region private utility methods & constructors

        private ClsDAL() { }

        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            if (command == null) throw new ArgumentNullException("command");

            if (commandParameters != null)
            {
                string param = string.Empty;
                foreach (SqlParameter p in commandParameters)
                {
                    if (p != null)
                    {
                        // Check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput ||
                            p.Direction == ParameterDirection.Input) &&
                            (p.Value == null))
                        {
                            p.Value = DBNull.Value;
                        }
                        command.Parameters.Add(p);
                        param = param + p.Value.ToString() + ",";
                    }
                }

                //ClsLog.LogApp(param);
            }
        }

        private static void AssignParameterValues(SqlParameter[] commandParameters, string strpara)
        {
            if ((commandParameters == null) || (strpara == null))
            {
                return;
            }
            else
            {
                commandParameters[0].Value = strpara;
            }
        }

        //Start Method
        private static void AssignParameterValues(SqlParameter[] commandParameters, long rowId)
        {
            if ((commandParameters == null) || (rowId == null))
            {
                return;
            }
            else
            {
                commandParameters[0].Value = rowId;
            }
        }

        //End Method

        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, out bool mustCloseConnection)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (commandText == null || commandText.Length == 0) throw new ArgumentNullException("commandText");
            if (connection == null)
            {
                connection = new SqlConnection(ClsUtility.ConnectionString);
                command.Connection = connection;
            }
            if (connection.State != ConnectionState.Open)
            {
                mustCloseConnection = true;
                connection.Open();
            }
            else
            {
                mustCloseConnection = false;
            }

            command.Connection = connection;
            command.CommandText = commandText;
            command.CommandTimeout = 180;

            if (transaction != null)
            {
                if (transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
                command.Transaction = transaction;
            }

            command.CommandType = commandType;

            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
            return;
        }

        #endregion private utility methods & constructors

        #region ExecuteNonQuery

        public static void SqlMessageEventHandler(object sender, SqlInfoMessageEventArgs e)
        {
            // print message
        }

        public static int ExecuteNonQuery(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            ////if (transaction == null) throw new ArgumentNullException("transaction");
            ////if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");

            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;

            //PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            PrepareCommand(cmd, null, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            int retval = cmd.ExecuteNonQuery();
            if (mustCloseConnection) cmd.Connection.Close();
            cmd.Parameters.Clear();
            return retval;
        }

        //Execute the code with last ID
        public static int ExecuteNonQueryWithOutPut(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            ////if (transaction == null) throw new ArgumentNullException("transaction");
            ////if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");

            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;

            //PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            PrepareCommand(cmd, null, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            int retval = cmd.ExecuteNonQuery();
            string id = cmd.Parameters["@id"].Value.ToString();
            if (mustCloseConnection) cmd.Connection.Close();
            cmd.Parameters.Clear();
            return Convert.ToInt32(id);
        }


        public static int ExecuteNonQuery(SqlTransaction transaction, string spName, string strpara)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            SqlParameter[] commandParameters = DALParameterCache.GetSpParameterSet(spName);

            if (strpara != null)
            {
                AssignParameterValues(commandParameters, strpara);

                return ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, commandParameters);
            }
            else
            {
                return ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
            }
        }

        public static int ExecuteNonQuery(SqlTransaction transaction, string spName, long rowId)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            SqlParameter[] commandParameters = DALParameterCache.GetSpParameterSet(spName);

            if (rowId != null)
            {
                AssignParameterValues(commandParameters, rowId);

                return ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, commandParameters);
            }
            else
            {
                return ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
            }
        }

        private delegate int ExecuteNonQueryDelegateTCC(SqlTransaction Transaction, CommandType commandType, string commandText);
        private delegate int ExecuteNonQueryDelegateTCCC(SqlTransaction Transaction, CommandType commandType, string commandText, params SqlParameter[] CommandParameters);
        private delegate int ExecuteNonQueryDelegateTSS(SqlTransaction Transaction, string spName, string strpara);

        public static IAsyncResult BeginExecuteNonQuery(SqlTransaction Transaction, CommandType CommandType, string CommandText, AsyncCallback ac, Object state)
        {
            ExecuteNonQueryDelegateTCC d = new ExecuteNonQueryDelegateTCC(ExecuteNonQuery);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteNonQuery(SqlTransaction Transaction, CommandType CommandType, string CommandText, SqlParameter[] CommandParameters, AsyncCallback ac, Object state)
        {
            ExecuteNonQueryDelegateTCCC d = new ExecuteNonQueryDelegateTCCC(ExecuteNonQuery);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, CommandParameters, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteNonQuery(SqlTransaction Transaction, string spName, string strpara, AsyncCallback ac, Object state)
        {
            ExecuteNonQueryDelegateTSS d = new ExecuteNonQueryDelegateTSS(ExecuteNonQuery);
            IAsyncResult result = d.BeginInvoke(Transaction, spName, strpara, ac, state);
            return result;
        }

        public static int EndExecuteNonQuery(IAsyncResult result)
        {
            if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteNonQueryDelegateTCC))
            {
                ExecuteNonQueryDelegateTCC d = (ExecuteNonQueryDelegateTCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteNonQueryDelegateTCCC))
            {
                ExecuteNonQueryDelegateTCCC d = (ExecuteNonQueryDelegateTCCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else
            {
                ExecuteNonQueryDelegateTSS d = (ExecuteNonQueryDelegateTSS)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
        }

        #endregion ExecuteNonQuery

        #region ExecuteDataset

        public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            return ExecuteDataset(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");

            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            //PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            PrepareCommand(cmd, null, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                cmd.Parameters.Clear();
                if (mustCloseConnection) cmd.Connection.Close();
                return ds;
            }

        }

        public static DataSet ExecuteDataset(SqlTransaction transaction, string spName, string strpara)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            SqlParameter[] commandParameters = DALParameterCache.GetSpParameterSet(spName);

            if (strpara != null)
            {
                AssignParameterValues(commandParameters, strpara);
                return ExecuteDataset(transaction, CommandType.StoredProcedure, spName, commandParameters);
            }
            else
            {
                return ExecuteDataset(transaction, CommandType.StoredProcedure, spName);
            }
        }

        private delegate DataSet ExecuteDatasetDelegateTCC(SqlTransaction Transaction, CommandType commandType, string commandText);
        private delegate DataSet ExecuteDatasetDelegateTCCC(SqlTransaction Transaction, CommandType commandType, string commandText, params SqlParameter[] CommandParameters);
        private delegate DataSet ExecuteDatasetDelegateTSS(SqlTransaction Transaction, string spName, string strpara);

        public static IAsyncResult BeginExecuteDataset(SqlTransaction Transaction, CommandType CommandType, string CommandText, AsyncCallback ac, Object state)
        {
            ExecuteDatasetDelegateTCC d = new ExecuteDatasetDelegateTCC(ExecuteDataset);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteDataset(SqlTransaction Transaction, CommandType CommandType, string CommandText, SqlParameter[] CommandParameters, AsyncCallback ac, Object state)
        {
            ExecuteDatasetDelegateTCCC d = new ExecuteDatasetDelegateTCCC(ExecuteDataset);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, CommandParameters, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteDataset(SqlTransaction Transaction, string spName, string strpara, AsyncCallback ac, Object state)
        {
            ExecuteDatasetDelegateTSS d = new ExecuteDatasetDelegateTSS(ExecuteDataset);
            IAsyncResult result = d.BeginInvoke(Transaction, spName, strpara, ac, state);
            return result;
        }

        public static DataSet EndExecuteDataset(IAsyncResult result)
        {
            if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteDatasetDelegateTCC))
            {
                ExecuteDatasetDelegateTCC d = (ExecuteDatasetDelegateTCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteDatasetDelegateTCCC))
            {
                ExecuteDatasetDelegateTCCC d = (ExecuteDatasetDelegateTCCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else
            {
                ExecuteDatasetDelegateTSS d = (ExecuteDatasetDelegateTSS)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
        }

        #endregion ExecuteDataset

        #region ExecuteReader

        private enum SqlConnectionOwnership
        {
            /// <summary>Connection is owned and managed by DAL</summary>
            //Internal,
            /// <summary>Connection is owned and managed by the caller</summary>
            External
        }

        public static SqlDataReader ExecuteReader(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteReader(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static SqlDataReader ExecuteReader(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");

            // Pass through to private overload, indicating that the connection is owned by the caller
            return ExecuteReader(transaction.Connection, transaction, commandType, commandText, commandParameters, SqlConnectionOwnership.External);
        }

        private static SqlDataReader ExecuteReader(SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, SqlConnectionOwnership connectionOwnership)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            bool mustCloseConnection = false;
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            try
            {
                PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

                SqlDataReader dataReader;

                // Call ExecuteReader with the appropriate CommandBehavior
                if (connectionOwnership == SqlConnectionOwnership.External)
                {
                    dataReader = cmd.ExecuteReader();
                }
                else
                {
                    dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }

                bool canClear = true;
                foreach (SqlParameter commandParameter in cmd.Parameters)
                {
                    if (commandParameter.Direction != ParameterDirection.Input)
                        canClear = false;
                }
                if (mustCloseConnection) cmd.Connection.Close();

                if (canClear)
                {
                    cmd.Parameters.Clear();
                }

                return dataReader;
            }
            catch
            {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
        }

        public static SqlDataReader ExecuteReader(SqlTransaction transaction, string spName, string strpara)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            SqlParameter[] commandParameters = DALParameterCache.GetSpParameterSet(spName);

            if (strpara != null)
            {
                AssignParameterValues(commandParameters, strpara);
                return ExecuteReader(transaction, CommandType.StoredProcedure, spName, commandParameters);
            }
            else
            {
                return ExecuteReader(transaction, CommandType.StoredProcedure, spName);
            }
        }

        private delegate SqlDataReader ExecuteReaderDelegateTCC(SqlTransaction Transaction, CommandType commandType, string commandText);
        private delegate SqlDataReader ExecuteReaderDelegateTCCC(SqlTransaction Transaction, CommandType commandType, string commandText, params SqlParameter[] CommandParameters);
        private delegate SqlDataReader ExecuteReaderDelegateTSS(SqlTransaction Transaction, string spName, string strpara);

        public static IAsyncResult BeginExecuteReader(SqlTransaction Transaction, CommandType CommandType, string CommandText, AsyncCallback ac, Object state)
        {
            ExecuteReaderDelegateTCC d = new ExecuteReaderDelegateTCC(ExecuteReader);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteReader(SqlTransaction Transaction, CommandType CommandType, string CommandText, SqlParameter[] CommandParameters, AsyncCallback ac, Object state)
        {
            ExecuteReaderDelegateTCCC d = new ExecuteReaderDelegateTCCC(ExecuteReader);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, CommandParameters, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteReader(SqlTransaction Transaction, string spName, string strpara, AsyncCallback ac, Object state)
        {
            ExecuteReaderDelegateTSS d = new ExecuteReaderDelegateTSS(ExecuteReader);
            IAsyncResult result = d.BeginInvoke(Transaction, spName, strpara, ac, state);
            return result;
        }

        public static SqlDataReader EndExecuteReader(IAsyncResult result)
        {
            if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteReaderDelegateTCC))
            {
                ExecuteReaderDelegateTCC d = (ExecuteReaderDelegateTCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteReaderDelegateTCCC))
            {
                ExecuteReaderDelegateTCCC d = (ExecuteReaderDelegateTCCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else
            {
                ExecuteReaderDelegateTSS d = (ExecuteReaderDelegateTSS)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
        }

        #endregion ExecuteReader

        #region ExecuteScalar

        public static object ExecuteScalar(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteScalar(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static object ExecuteScalar(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");

            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            //PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            PrepareCommand(cmd, null, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            // Execute the command & return the results
            object retval = cmd.ExecuteScalar();
            if (mustCloseConnection) cmd.Connection.Close();

            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            return retval;
        }

        public static object ExecuteScalar(SqlTransaction transaction, string spName, string strpara)
        {
            //if (transaction == null) throw new ArgumentNullException("transaction");
            //if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            SqlParameter[] commandParameters = DALParameterCache.GetSpParameterSet(spName);

            if (strpara != null)
            {
                AssignParameterValues(commandParameters, strpara);
                return ExecuteScalar(transaction, CommandType.StoredProcedure, spName, commandParameters);
            }
            else
            {
                return ExecuteScalar(transaction, CommandType.StoredProcedure, spName);
            }
        }

        private delegate object ExecuteScalarDelegateTCC(SqlTransaction Transaction, CommandType commandType, string commandText);
        private delegate object ExecuteScalarDelegateTCCC(SqlTransaction Transaction, CommandType CommandType, string CommandText, params SqlParameter[] CommandParameters);
        private delegate object ExecuteScalarDelegateTSS(SqlTransaction Transaction, string spName, string strpara);

        public static IAsyncResult BeginExecuteScalar(SqlTransaction Transaction, CommandType CommandType, string CommandText, AsyncCallback ac, Object state)
        {
            ExecuteScalarDelegateTCC d = new ExecuteScalarDelegateTCC(ExecuteScalar);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteScalar(SqlTransaction Transaction, CommandType CommandType, string CommandText, SqlParameter[] CommandParameters, AsyncCallback ac, Object state)
        {
            ExecuteScalarDelegateTCCC d = new ExecuteScalarDelegateTCCC(ExecuteScalar);
            IAsyncResult result = d.BeginInvoke(Transaction, CommandType, CommandText, CommandParameters, ac, state);
            return result;
        }
        public static IAsyncResult BeginExecuteScalar(SqlTransaction Transaction, string spName, string strpara, AsyncCallback ac, Object state)
        {
            ExecuteScalarDelegateTSS d = new ExecuteScalarDelegateTSS(ExecuteScalar);
            IAsyncResult result = d.BeginInvoke(Transaction, spName, strpara, ac, state);
            return result;
        }

        public static object EndExecuteScalar(IAsyncResult result)
        {
            if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteScalarDelegateTCC))
            {
                ExecuteScalarDelegateTCC d = (ExecuteScalarDelegateTCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else if ((((AsyncResult)result).AsyncDelegate).GetType() == typeof(ExecuteScalarDelegateTCCC))
            {
                ExecuteScalarDelegateTCCC d = (ExecuteScalarDelegateTCCC)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
            else
            {
                ExecuteScalarDelegateTSS d = (ExecuteScalarDelegateTSS)((AsyncResult)result).AsyncDelegate;
                return d.EndInvoke(result);
            }
        }

        #endregion ExecuteScalar

        #region FillDataset

        public static void FillDataset(SqlTransaction transaction, CommandType commandType, string commandText, DataSet dataSet, string[] tableNames)
        {
            FillDataset(transaction, commandType, commandText, dataSet, tableNames, null);
        }

        public static void FillDataset(SqlTransaction transaction, CommandType commandType,
            string commandText, DataSet dataSet, string[] tableNames,
            params SqlParameter[] commandParameters)
        {
            FillDataset(transaction.Connection, transaction, commandType, commandText, dataSet, tableNames, commandParameters);
        }

        private static void FillDataset(SqlConnection connection, SqlTransaction transaction, CommandType commandType,
             string commandText, DataSet dataSet, string[] tableNames,
             params SqlParameter[] commandParameters)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            if (dataSet == null) throw new ArgumentNullException("dataSet");

            // Create a command and prepare it for execution
            SqlCommand command = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(command, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            // Create the DataAdapter & DataSet
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
            {
                // Add the table mappings specified by the user
                if (tableNames != null && tableNames.Length > 0)
                {
                    string tableName = "Table";
                    for (int index = 0; index < tableNames.Length; index++)
                    {
                        if (tableNames[index] == null || tableNames[index].Length == 0) throw new ArgumentException("The tableNames parameter must contain a list of tables, a value was provided as null or empty string.", "tableNames");
                        dataAdapter.TableMappings.Add(tableName, tableNames[index]);
                        tableName += (index + 1).ToString();
                    }
                }

                // Fill the DataSet using default values for DataTable names, etc
                dataAdapter.Fill(dataSet);

                // Detach the SqlParameters from the command object, so they can be used again
                command.Parameters.Clear();
            }
            if (mustCloseConnection) command.Connection.Close();

            if (mustCloseConnection && connection != null)
                connection.Close();
        }

        #endregion

        #region UpdateDataset

        public static void UpdateDataset(SqlCommand insertCommand, SqlCommand deleteCommand, SqlCommand updateCommand, DataSet dataSet, string tableName)
        {
            if (insertCommand == null) throw new ArgumentNullException("insertCommand");
            if (deleteCommand == null) throw new ArgumentNullException("deleteCommand");
            if (updateCommand == null) throw new ArgumentNullException("updateCommand");
            if (tableName == null || tableName.Length == 0) throw new ArgumentNullException("tableName");

            // Create a SqlDataAdapter, and dispose of it after we are done
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter())
            {
                // Set the data adapter commands
                dataAdapter.UpdateCommand = updateCommand;
                dataAdapter.InsertCommand = insertCommand;
                dataAdapter.DeleteCommand = deleteCommand;

                // Update the dataset changes in the data source
                dataAdapter.Update(dataSet, tableName);

                // Commit all the changes made to the DataSet
                dataSet.AcceptChanges();
            }
        }
        #endregion

        #region CreateCommand

        public static SqlCommand CreateCommand(SqlConnection connection, string spName, params string[] sourceColumns)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            // Create a SqlCommand
            SqlCommand cmd = new SqlCommand(spName, connection);
            cmd.CommandType = CommandType.StoredProcedure;

            // If we receive parameter values, we need to figure out where they go
            if ((sourceColumns != null) && (sourceColumns.Length > 0))
            {
                // Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                SqlParameter[] commandParameters = DALParameterCache.GetSpParameterSet(connection, spName);

                // Assign the provided source columns to these parameters based on parameter order
                for (int index = 0; index < sourceColumns.Length; index++)
                    commandParameters[index].SourceColumn = sourceColumns[index];

                // Attach the discovered parameters to the SqlCommand object
                AttachParameters(cmd, commandParameters);
            }
            return cmd;
        }
        #endregion
    }

    public sealed class DALParameterCache
    {
        #region private methods, variables, and constructors

        private DALParameterCache() { }

        private static readonly Hashtable paramCache = Hashtable.Synchronized(new Hashtable());

        private static SqlParameter[] DiscoverSpParameterSet(SqlConnection connection, string spName, bool includeReturnValueParameter)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            SqlCommand cmd = new SqlCommand(spName, connection);
            cmd.CommandType = CommandType.StoredProcedure;

            connection.Open();
            SqlCommandBuilder.DeriveParameters(cmd);
            connection.Close();

            if (!includeReturnValueParameter)
            {
                cmd.Parameters.RemoveAt(0);
            }

            SqlParameter[] discoveredParameters = new SqlParameter[cmd.Parameters.Count];

            cmd.Parameters.CopyTo(discoveredParameters, 0);

            // Init the parameters with a DBNull value
            foreach (SqlParameter discoveredParameter in discoveredParameters)
            {
                discoveredParameter.Value = DBNull.Value;
            }
            return discoveredParameters;
        }

        private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
        {
            SqlParameter[] clonedParameters = new SqlParameter[originalParameters.Length];

            for (int i = 0, j = originalParameters.Length; i < j; i++)
            {
                clonedParameters[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
            }

            return clonedParameters;
        }

        #endregion private methods, variables, and constructors

        #region caching functions

        public static void CacheParameterSet(string connectionString, string commandText, params SqlParameter[] commandParameters)
        {
            if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0) throw new ArgumentNullException("commandText");

            string hashKey = connectionString + ":" + commandText;

            paramCache[hashKey] = commandParameters;
        }

        public static SqlParameter[] GetCachedParameterSet(string connectionString, string commandText)
        {
            if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0) throw new ArgumentNullException("commandText");

            string hashKey = connectionString + ":" + commandText;

            SqlParameter[] cachedParameters = paramCache[hashKey] as SqlParameter[];
            if (cachedParameters == null)
            {
                return null;
            }
            else
            {
                return CloneParameters(cachedParameters);
            }
        }

        #endregion caching functions

        #region Parameter Discovery Functions

        public static SqlParameter[] GetSpParameterSet(string connectionString, string spName)
        {
            return GetSpParameterSet(connectionString, spName, false);
        }

        public static SqlParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
        {
            if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                return GetSpParameterSetInternal(connection, spName, includeReturnValueParameter);
            }
        }

        internal static SqlParameter[] GetSpParameterSet(string spName)
        {
            SqlConnection conn = new SqlConnection(ClsUtility.ConnectionString);
            return GetSpParameterSet(conn, spName, false);
        }

        internal static SqlParameter[] GetSpParameterSet(SqlConnection connection, string spName)
        {
            return GetSpParameterSet(connection, spName, false);
        }

        internal static SqlParameter[] GetSpParameterSet(SqlConnection connection, string spName, bool includeReturnValueParameter)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            using (SqlConnection clonedConnection = (SqlConnection)((ICloneable)connection).Clone())
            {
                return GetSpParameterSetInternal(clonedConnection, spName, includeReturnValueParameter);
            }
        }

        private static SqlParameter[] GetSpParameterSetInternal(SqlConnection connection, string spName, bool includeReturnValueParameter)
        {
            if (connection == null) throw new ArgumentNullException("connection");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

            string hashKey = connection.ConnectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");

            SqlParameter[] cachedParameters;

            cachedParameters = paramCache[hashKey] as SqlParameter[];
            if (cachedParameters == null)
            {
                SqlParameter[] spParameters = DiscoverSpParameterSet(connection, spName, includeReturnValueParameter);
                paramCache[hashKey] = spParameters;
                cachedParameters = spParameters;
            }

            return CloneParameters(cachedParameters);
        }

        #endregion Parameter Discovery Functions

    }
}
