﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Web;


namespace DBLibrary
{
    public static class ClsUtility
    {
        private static string connectionString;

        static ClsUtility()
        {
           // log4net.Config.XmlConfigurator.Configure();
        }

        public static string ConnectionString
        {
            get
            {
                if (connectionString == null)
                {
                    connectionString = ConfigurationManager.ConnectionStrings["Pay2cartConnStr"].ConnectionString;
                }
                return connectionString;
            }
            set { connectionString = value; }
        }

       /* public static void LogError(string message, Exception err)
        {
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");

            logger.Error(message, err);
        }
        public static void LogMessage(string message)
        {
            ILog logger = log4net.LogManager.GetLogger("Information");
            logger.Error(message);
        }*/

    }
}
