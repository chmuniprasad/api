﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class GasBill
    {
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerID Must be a Number.")]
        //public string customerID { get; set; } previous parameter
        public string userId { get; set; }//new parameter
        [RegularExpression(@"([0-9]+)", ErrorMessage = "orderID Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "orderID must be greater than or equals to {1}")]
        public string orderID { get; set; }
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "operatorCode Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "operatorCode must be greater than or equals to {1}")]
        //public string operatorCode { get; set; }
        public string gas_provider { get; set; }

        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerNumber Must be a Number.")]
        // public string customerNumber { get; set; }  previous parameter
        public string customerId { get; set; }
        [Required]
        [Range(10, int.MaxValue, ErrorMessage = "amount must be greater than or equals to {1}")]
        public string amount { get; set; }
        public string billGroupNumber { get; set; }
        

    }
}