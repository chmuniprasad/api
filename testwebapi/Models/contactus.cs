﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testwebapi.Models
{
    public class contactus
    {
        public string from { get; set; }
        public string id_contact { get; set; }
        public int id_order { get; set; }
        public int id_product { get; set; }
        public string message { get; set; }
        public int id_customer { get; set; }
        public string  file_attachment { get; set; }
        public string mobile { get; set; }


    }
}