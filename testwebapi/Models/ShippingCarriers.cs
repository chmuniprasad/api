﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class ShippingCarrier
    {
      public int id_customer { get; set; }
      public int id_address_delivery { get; set; }
    }

    public class Homecontactus
    {
        public string id_contact { get; set; }
        public string from { get; set; }
        public string name { get; set; }
        public string message { get; set; }
        public string id_customer { get; set; }
    }
}