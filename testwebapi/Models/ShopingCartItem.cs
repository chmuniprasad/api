﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Pay2CartAPI.Models
{

    public class UpdateCartItems
    {

        public int id_customer { get; set; }
        [Required]
        public int id_product { get; set; }
        public int quantity { get; set; }
       
    }

    public class AdditemstoCart
    {
       
        public string id_customer { get; set; }
      
        public string id_guest { get; set; }
        [Required]
        public int id_product { get; set; }
        public string  id_product_variant { get; set; }
        [Required]
        public int quantity { get; set; }
      
    }
  
    public class DeleteCartItems
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product { get; set; }
    }

   
   

    
}