﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;

namespace Pay2CartAPI.Models
{
    public class GiftCards
    {
       
        [Required]
        public string RecipientName { get; set; }
        [Required]
        public string RecipientEmail { get; set; }
        [Required]
        public string SenderName { get; set; }
        [Required]
        public string SenderEmail { get; set; }
        public string Message { get; set; }
      
        public int CustomerId { get; set; }
        [Required]
        public int ProductId { get; set; }
     
        [Required]
        public int Quantity { get; set; }
     

        public string AddGiftCardAttribute(
            
             string recipientName,
             string recipientEmail,
             string senderName,
             string senderEmail,
             string giftCardMessage)
        {
            var result = string.Empty;

            try
            {
                string attributesXml = "";
                recipientName = RecipientName;
                recipientEmail = RecipientEmail;
                senderName = SenderName;
                senderEmail = SenderEmail;
                giftCardMessage = Message;
                string AttributesXml = "";
                 var xmlDoc = new XmlDocument();
                if (string.IsNullOrEmpty(AttributesXml))
                {
                    var element1 = xmlDoc.CreateElement("Attributes");
                    xmlDoc.AppendChild(element1);
                }
                else
                {
                    xmlDoc.LoadXml(attributesXml);
                }

                var rootElement = (XmlElement)xmlDoc.SelectSingleNode(@"//Attributes");

                var giftCardElement = (XmlElement)xmlDoc.SelectSingleNode(@"//Attributes/GiftCardInfo");
                if (giftCardElement == null)
                {
                    giftCardElement = xmlDoc.CreateElement("GiftCardInfo");
                    rootElement.AppendChild(giftCardElement);
                }

                var recipientNameElement = xmlDoc.CreateElement("RecipientName");
                recipientNameElement.InnerText = recipientName;
                giftCardElement.AppendChild(recipientNameElement);

                var recipientEmailElement = xmlDoc.CreateElement("RecipientEmail");
                recipientEmailElement.InnerText = recipientEmail;
                giftCardElement.AppendChild(recipientEmailElement);

                var senderNameElement = xmlDoc.CreateElement("SenderName");
                senderNameElement.InnerText = senderName;
                giftCardElement.AppendChild(senderNameElement);

                var senderEmailElement = xmlDoc.CreateElement("SenderEmail");
                senderEmailElement.InnerText = senderEmail;
                giftCardElement.AppendChild(senderEmailElement);

                var messageElement = xmlDoc.CreateElement("Message");
                messageElement.InnerText = giftCardMessage;
                giftCardElement.AppendChild(messageElement);

                result = xmlDoc.OuterXml;

              
            }
            catch (Exception Err)
            {
                
            }

            return result;
            
        }

    }



  

    public class DeleteGiftCardFromCart
    {
      
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int Quantity { get; set; }
       
       
    }
}