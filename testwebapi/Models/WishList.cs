﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Pay2CartAPI.Models
{

    public class AddItemstoWishList
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product { get; set; }
        public int id_product_variant { get; set; }
        [Required]
        public int quantity { get; set; }

    }

    public class DeleteWishlistItems
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product { get; set; }
    }
    public class UpdateWishlistitems
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product { get; set; }
        public int quantity { get; set; }
    }
}