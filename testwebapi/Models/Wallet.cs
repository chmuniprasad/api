﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    
    public class addMoneyToWallet
    {

        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerID Must be a Number.")]
        public int customer_id { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "amount must be greater than or equals to {1}")]
        public int amount { get; set; }
    }

    public class SendMoney
    {
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerID Must be a Number.")]
        public int customer_id { get; set; }
        [Required]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Mobile Number.")]
        public string mobile_number { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "amount must be greater than or equals to {1}")]
        public int amount { get; set; }
        //[RegularExpression(@"([0-9]+)", ErrorMessage = "orderID Must be a Number.")]
        //[Range(1, int.MaxValue, ErrorMessage = "orderID must be greater than or equals to {1}")]
        public string orderID { get; set; }
    }

    public class Rsa
    {
        public string order_id { get; set; }
    }
}