﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Pay2CartAPI.Models
{
    //Add billing address model
    public class AddCustomerAddress
    {
        [Required]
        public int id_country { get; set; }
        [Required]
        public string postcode { get; set; }
        [Required]
        public string firstname { get; set; }
       
        [Required]
        public int id_state { get; set; }
        [Required]
        public string address1 { get; set; }
        public string address2 { get; set; }
        [Required]
        public string city { get; set; }
        [Required]
        public string phone_mobile { get; set; }
        [Required]
        public int id_customer { get; set; }
       
       
    }

    //Update Customer billing address model
    public class UpdateCustomerBillAddress
    {
        [Required]
        public int id_country { get; set; }
        [Required]
        public string postcode { get; set; }
        [Required]
        public string firstname { get; set; }
       
        [Required]
        public int id_state { get; set; }
        [Required]
        public string address1 { get; set; }
        public string address2 { get; set; }
        [Required]
        public string city { get; set; }
        [Required]
        public string phone_mobile { get; set; }
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int addressId { get; set; }
    
       
    }

    public class CustomerBillAddressDelete
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_address { get; set; }
    }
}