﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class ViewBill
    {
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerNumber Must be a Number.")]
        public string cardNumber { get; set; }
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "operatorId Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "operatorId must be greater than or equals to {1}")]
        public string operatorCode { get; set; }
        public string mbgNo { get; set; }
    }
}