﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class Summary
    {
       public int id_customer { get; set; }
       public int id_address_delivery { get; set; }
    }

    public class ReOrder
    {
        public int customerID { get; set; }
        public int orderID { get; set; }
    }
}