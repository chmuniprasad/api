﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pay2CartAPI
{
    public class productsInfo
    {
        public  int id_product { get; set; }
        public int id_category_default { get; set; }
        public int delivery_available_in { get; set; }
        public string category { get; set; }
        public int quantity { get; set; }
        public int minimal_quantity { get; set; }
        public decimal price { get; set; }
        public decimal price_tax_exc { get; set; }
        public decimal price_without_reduction { get; set; }
        public int out_of_stock { get; set;}
        public int quantity_discount { get; set; }
        public string condition { get; set; }
        public string description { get; set; }
        public string description_short { get; set; }
        public string name { get; set; }
        public int id_image { get; set; }
        public string link_rewrite { get; set; }
        public string link { get; set; }
        public int isExistedInCart { get; set; }
        public int quantityExistsInCart { get; set; }
        public object image { get; set; }
        public decimal  original_cost { get; set; }
        public int discount_percentage { get; set; }

    }

}