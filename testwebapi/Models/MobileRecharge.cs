﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class MobileRecharge
    {
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerID Must be a Number.")]
        public string userId { get; set; }
        [RegularExpression(@"([0-9]+)", ErrorMessage = "orderID Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "orderID must be greater than or equals to {1}")]
        public string orderID { get; set; }
        [Required]
       // public string networkType { get; set; } previous parameter
        public string network_type { get; set; }
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "network Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "network must be greater than or equals to {1}")]
        public int network { get; set; }
        [Required]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Provide a valid Phone number")]
        //public string mobileNumber { get; set; } previous parameter
        public string mobile_number { get; set; }
        [Required]
        [Range(10, int.MaxValue, ErrorMessage = "amount must be greater than or equals to {1}")]
        //public int amount { get; set; }
        public int rechargeAmount { get; set; }
    }
}