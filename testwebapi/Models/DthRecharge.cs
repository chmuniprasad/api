﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class DthRecharge
    {
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "customerID Must be a Number.")]
        public string userId { get; set; }
        [RegularExpression(@"([0-9]+)", ErrorMessage = "orderID Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "orderID must be greater than or equals to {1}")]
        public string orderID { get; set; }
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "operatorId Must be a Number.")]
        [Range(1, int.MaxValue, ErrorMessage = "operatorId must be greater than or equals to {1}")]
        public int operator_code { get; set; }
        [Required]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "cardNumber Must be a Number.")]
        public string card_number { get; set; }
        [Required]
        [Range(10, int.MaxValue, ErrorMessage = "amount must be greater than or equals to {1}")]
        public int rechargeAmount { get; set; }
        public string deviceID { get; set; }
    }
}