﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Pay2CartAPI.Models
{
    public class WriteProductReview
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product { get; set; }
        [Required]
        public string rating { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        public string content { get; set; }
       
    }
    public class ReviewsUsefulNo
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product_comment { get; set; }
      
    }
    public class ReviewsUsefulYes
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public int id_product_comment { get; set; }
        
        public int  is_useful { get; set; }
    }
}