﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Models
{
    public class addCustomer
    {
        [Required]
        public string email { get; set; }
        [Required]
        public string mobile { get; set; }
        [Required]
        public string passwd { get; set; }
        public string id_guest { get; set; }
    }

    public class CustomerLogin
    {
        [Required]
        public string userName { get; set; }
        [Required]
        public string password { get; set; }
        public string id_guest { get; set; }
    }

    public class otpvaidate
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string mobile { get; set; }
        
        public string otp { get; set; }
    }

    public class CustomerProfile
    {
        [Required]
        public int id_customer { get; set; }
        public string months { get; set; }
        public string days { get; set; }
        public string years { get; set; }
        public string customerimage { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string id_gender { get; set; }


        //public string DateOfBirth { get; set; }
        public string Company { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Signature { get; set; }
        public string VatNumber { get; set; }
      

    }
    public class Customerdetails
    {
        public int id_customer { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string birthday { get; set; }
        public string customerimage { get; set; }
        public string adharnumber { get; set; }
        public string id_gender { get; set; }
        public string passwd { get; set; }
        public string message { get; set; }
       

    }
    public class PasswordReset
    {
        [Required]
        public string email { get; set; }
    }

        public class ChangePassword
    {
        [Required]
        public int id_customer { get; set; }
        [Required]
        public string currentpasswd { get; set; }
        [Required]
        public string confirmation { get; set; }
        [Required]
        public string passwd { get; set; }
       
    }
}