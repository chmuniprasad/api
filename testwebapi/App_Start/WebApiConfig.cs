﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Pay2CartAPI
{
    public static class WebApiConfig
    {
        /*public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }*/
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");
            config.Routes.MapHttpRoute(
                name: "DefaultApiWithID",
                routeTemplate: "api/{controller}/{id}",
                defaults: null
            );
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}
