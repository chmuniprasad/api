﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using CCA.Util;
using System.Configuration;
using Pay2CartAPI.WalletService;
using System.Net;
using System.Data;

namespace Pay2CartAPI.PaymentCallBack
{
    public partial class Paymentcallback : System.Web.UI.Page
    {
        private float amount;
        private string orderID;

        protected void Page_Load(object sender, EventArgs e)
        {

            string workingKey = ConfigurationManager.AppSettings["WorkingKey"];//put in the 32bit alpha numeric key in the quotes provided here
            CCACrypto ccaCrypto = new CCACrypto();
            string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"],workingKey);

            string OrderStatus = "";
            NameValueCollection Params = new NameValueCollection();
            string[] segments = encResponse.Split('&');
            int j = 0;
            foreach (string seg in segments)
            {
                string[] parts = seg.Split('=');
                if (parts.Length > 0)
                {
                    string Key = parts[0].Trim();
                    string Value = parts[1].Trim();
                    Params.Add(Key, Value);
                    if (j == 0)
                    {
                        orderID = Value;
                    }
                    if (j == 3)
                    {
                        OrderStatus = Value;
                      
                    }
                    if(j==10)
                    {
                        amount = (float)Convert.ToDouble(Value);
                    }
                }
                j++;
            }

            Wallet.SaveUpdate_PaymentCallbackLog(true, orderID,"", encResponse.ToString(), DateTime.Now,DateTime.Now);
            if (OrderStatus == "Success")
            {
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                string IpAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
                int userID = 0;
                string sub = orderID.Substring(0, 3);
                if (sub == "wlt" || sub == "snd")
                {
                    orderID = orderID.Substring(3);
                    userID = Wallet.getWalletOrderDetailsByOrderId(Convert.ToInt32(orderID));
                }
                else if (sub == "odr")
                {
                    orderID = orderID.Substring(3);
                    DataTable OrdersData = Wallet.getOrderDetailsByOrderId(Convert.ToInt32(orderID));
                    if (OrdersData.Rows.Count > 0)
                    {
                        userID = Convert.ToInt32(OrdersData.Rows[0]["id_customer"]);
                    }
                }
                else
                {
                    DataTable OrdersData = Wallet.getRechargeOrdersById(Convert.ToInt32(orderID));

                    if (OrdersData.Rows.Count > 0)
                    {
                        userID = Convert.ToInt32(OrdersData.Rows[0]["customerID"]);
                    }
                }
                DataTable WalletDetails = Wallet.checkWalletOrderExists(Convert.ToInt32(orderID), 6);
                if (WalletDetails.Rows.Count == 0)
                {
                    if (userID > 0)
                    {
                        float CurrentBalance = Wallet.getCustomerCurrentWalletBalance(userID);
                        float balanceAmount = CurrentBalance + amount;
                        //Adding Balance to the customer wallet
                        Wallet.saveWalletBalance(userID.ToString(), orderID.ToString(), amount.ToString(), balanceAmount.ToString(), 1, 6, IpAddress, "Add Money");
                    }
                }


                
               
            }
            else if (OrderStatus == "Failure")
            {
                Response.Write("Thank you for shopping with us.However,the transaction has been declined.");
            }
            else if (OrderStatus == "Aborted")
            {
                Response.Write("Thank you for shopping with us.However,the transaction has been declined.");
            }
            else
            {
                Response.Write("Security Error. Illegal access detected.");
            }

            for (int i = 0; i < Params.Count; i++)
            {
                Response.Write(Params.Keys[i] + " = " + Params[i] + "<br>");
            }

        }
    }
}