﻿using CompLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using Pay2CartAPI.Classes;
using Pay2CartAPI.Response;
using Pay2CartAPI.WalletService;
//using DBLibrary;

namespace Pay2CartAPI.Cyberplat
{
    public class CPGasBill
    {
        public static string doGasBillPayment(string customerID, string operatorCode, string customerNumber, string amount, string billGroupNumber, string OrderID)
        {
            string res = "";
            string msg = "";
            int rechargeStatus = 0;
            RechargeBillPaymentsObj ResponseObj = new RechargeBillPaymentsObj();
            try
            {
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                string IpAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
                //Validate monthly and daily transaction limit

                bool monthlylimit = true;
                bool dailyLimit = true;
                // checking Daily transaction SUM of user
                DataTable dailyLimitDt = RechargeBillPaymentsComp.checkUserDailyMonthlyTrasctions(Convert.ToInt32(customerID), 1, 2);
                if (dailyLimitDt.Rows.Count > 0)
                {
                    int dailytxAmount = 0;
                    try
                    {
                        if (dailyLimitDt.Rows[0]["Amount"] != null)
                        {
                            dailytxAmount = Convert.ToInt32(dailyLimitDt.Rows[0]["Amount"]);
                        }
                    }
                    catch (Exception Err)
                    {

                    }

                    dailytxAmount = dailytxAmount + Convert.ToInt32(amount);
                    int ActualLimit = Convert.ToInt32(ConfigurationManager.AppSettings["DailyLimit"]);

                    if (dailytxAmount > ActualLimit)
                    {
                        dailyLimit = false;
                    }
                }
                // checking monthly transaction SUM of user
                DataTable MonthlyLimitDt = RechargeBillPaymentsComp.checkUserDailyMonthlyTrasctions(Convert.ToInt32(customerID), 2, 2);
                if (MonthlyLimitDt.Rows.Count > 0)
                {
                    int monthlytxAmount = 0;
                    try
                    {
                        if (MonthlyLimitDt.Rows[0]["Amount"] != null)
                        {
                            monthlytxAmount = Convert.ToInt32(MonthlyLimitDt.Rows[0]["Amount"]);
                        }
                    }
                    catch (Exception Err)
                    {

                    }
                    monthlytxAmount = monthlytxAmount + Convert.ToInt32(amount);
                    int ActualLimit = Convert.ToInt32(ConfigurationManager.AppSettings["MonthlyLimit"]);

                    if (monthlytxAmount > ActualLimit)
                    {
                        monthlylimit = false;
                    }
                }

                if (monthlylimit == true && dailyLimit == true)
                {
                    int orderTypeID = 2;
                    int CurrentCustomerOrderID = 0;
                    //get customer current balance
                    float CurrentBalacne = Wallet.getCustomerCurrentWalletBalance(Convert.ToInt32(customerID));
                    if (CurrentBalacne >= Convert.ToInt32(amount))
                    {
                        string url = "";

                        DataTable CyberConfing = CyberplatConfComp.getCyberPlatConf(operatorCode);
                        if (CyberConfing.Rows.Count > 0)
                        {

                            if (OrderID == "" || OrderID == null)
                            {
                                string lastID = RechargeBillPaymentsComp.saveRechargeAndBillPaymentOrder(customerID, orderTypeID.ToString(), "4", amount, IpAddress).ToString();
                                DataTable dt = RechargeBillPaymentsComp.getCurrentCustomerRBOrderID(customerID);
                                CurrentCustomerOrderID = Convert.ToInt32(dt.Rows[0]["orderID"]);
                                OrderID = CurrentCustomerOrderID.ToString();
                                string suc = RechargeBillPaymentsComp.saveGasBillPaymentData(OrderID, operatorCode, customerNumber, billGroupNumber).ToString();

                            }

                            if (OrderID != "" || OrderID != null || OrderID != "0")
                            {


                                url = CyberConfing.Rows[0]["verificationUrl"].ToString();
                                string data = "{'providerID':'" + operatorCode + "','customerNumber':'" + customerNumber + "','amount':'" + amount + "','orderID':'" + OrderID + "','url':'" + url + "','billGroupNumber':'" + billGroupNumber + "'}";
                                string gasBillUrl = ConfigurationManager.AppSettings["gasBillUrl"];
                                string jsonData = ClsUtility.HttpPost(gasBillUrl, data);

                                CyberResponse CyberResponse = JsonConvert.DeserializeObject<CyberResponse>(jsonData);

                                int status = CyberResponse.status;

                                if (status == 1)
                                {
                                    string Result = CyberResponse.result.ToString();
                                    string[] splitString = Result.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                                    string API_CERT = splitString[1];
                                    string API_PAYDATE = splitString[2];
                                    string API_PAYSESSION = splitString[3];
                                    string API_ERROR = splitString[4];
                                    string API_RESULT = splitString[5];
                                    string API_TRANSID = splitString[6];

                                    string[] Api_CertArr = API_CERT.Split('=');
                                    string[] Api_PayDateArr = API_PAYDATE.Split('=');
                                    string[] Api_PaySessionArr = API_PAYSESSION.Split('=');
                                    string[] Api_ErrArr = API_ERROR.Split('=');
                                    string[] Api_ResArr = API_RESULT.Split('=');
                                    string[] Api_TransIDArr = API_TRANSID.Split('=');

                                    string cert = Api_CertArr[1].ToString();
                                    int error = Convert.ToInt32(Api_ErrArr[1]);
                                    int result = Convert.ToInt32(Api_ResArr[1]);
                                    if (Api_PaySessionArr[1].ToString() != "")
                                    {
                                        OrderID = Api_PaySessionArr[1].ToString();
                                    }

                                    string Paydate = Api_PayDateArr[1];
                                    string TxID = Api_TransIDArr[1].ToString();


                                    int txStatus = 0;

                                    //error is 0 and result also 0 then go for the recharge or bill payment.
                                    if (error == 0 && result == 0)
                                    {

                                        string API_AUTHCODE = splitString[7];
                                        string API_TRNXSTATUS = splitString[8];
                                        string[] Api_AuthCodeArr = API_AUTHCODE.Split('=');
                                        string[] Api_TrnxStatusArr = API_TRNXSTATUS.Split('=');
                                        txStatus = Convert.ToInt32(Api_TrnxStatusArr[1]);
                                        float PrsntBlnc = CurrentBalacne - Convert.ToInt32(amount);
                                        if (txStatus == 7)
                                        {
                                            //deducting the wallet balance
                                            Wallet.saveWalletBalance(customerID, OrderID, amount, PrsntBlnc.ToString(), 2, 4, IpAddress, "Gas Bill Payment");
                                            rechargeStatus = 1;
                                            msg = "Gas bill payment success.";

                                        }
                                        else if (txStatus == 3)
                                        {
                                            //deducting the wallet balance
                                            Wallet.saveWalletBalance(customerID, OrderID, amount, PrsntBlnc.ToString(), 2, 4, IpAddress, "Gas Bill Payment");
                                            rechargeStatus = 2;
                                            msg = "Gas bill payment success.";

                                        }

                                        DataTable CustomerDetails = CustomerComp.getCustomerDetails(Convert.ToInt32(customerID));
                                        if (CustomerDetails.Rows.Count > 0)
                                        {
                                            String Mobile = CustomerDetails.Rows[0]["mobile"].ToString();
                                            string SMSText = "Dear Customer, Your gas bill payment successfully paid with the customer number " + customerNumber;
                                            Common.sendSMS(Mobile, SMSText);
                                        }
                                    }
                                    else
                                    {

                                        DataTable CustomerDetails = CustomerComp.getCustomerDetails(Convert.ToInt32(customerID));
                                        if (CustomerDetails.Rows.Count > 0)
                                        {
                                            String Mobile = CustomerDetails.Rows[0]["mobile"].ToString();
                                            string SMSText = "Dear Customer, Your gas bill payment failed with customer number " + customerNumber;
                                            Common.sendSMS(Mobile, SMSText);
                                        }

                                        rechargeStatus = 3;
                                        ResponseObj.status = 0;
                                        msg = "Gas bill payment failed.";

                                    }

                                    RechargeBillPaymentsComp.updateRechargeOrdersData(OrderID.ToString(), error.ToString(), result.ToString(), rechargeStatus.ToString(), cert, Paydate, TxID);

                                }
                                else
                                {
                                    ResponseObj.status = 0;
                                    ResponseObj.message = Common.OopsSomethingWentWrong;
                                }
                            }
                            else
                            {
                                ResponseObj.status = 0;
                                ResponseObj.message = Common.OopsSomethingWentWrong;
                            }
                        }
                        else
                        {

                            ResponseObj.status = 0;
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                        }
                    }
                    else
                    {
                        RechargeBillPaymentsComp.saveRechargeAndBillPaymentOrder(customerID, orderTypeID.ToString(), "4", amount, IpAddress).ToString();
                        DataTable dtr = RechargeBillPaymentsComp.getCurrentCustomerRBOrderID(customerID);
                        CurrentCustomerOrderID = Convert.ToInt32(dtr.Rows[0]["orderID"]);

                        RechargeBillPaymentsComp.saveGasBillPaymentData(CurrentCustomerOrderID.ToString(), operatorCode, customerNumber, billGroupNumber).ToString();

                        InsufficientRechargeFundsObj ISRFObj = new InsufficientRechargeFundsObj();

                        ISRFObj.status = 0;
                        ISRFObj.message = "InSufficient Funds";
                        float pendingAmount = Convert.ToInt32(amount) - CurrentBalacne;
                        ISRFObj.pendingAmount = Convert.ToInt32(pendingAmount);

                        ISRFObj.orderID = CurrentCustomerOrderID;
                        res = JsonConvert.SerializeObject(ISRFObj).ToString();
                        return res;
                    }
                }
                else
                {
                    string trans = Common.CustomerDailyMonthlyTransaction(monthlylimit, dailyLimit);
                    ResponseObj.message = trans;
                    ResponseObj.pendingAmount = 0;
                    ResponseObj.status = 2;
                    res = JsonConvert.SerializeObject(ResponseObj).ToString();
                    return res;

                }

            }
            catch (Exception Err)
            {
                ResponseObj.status = 0;
                ResponseObj.message = Common.OopsSomethingWentWrong;
            }
            if (rechargeStatus > 0)
            {
                ResponseObj.message = msg;
                ResponseObj.status = 1;
                if (rechargeStatus == 3)
                {
                    ResponseObj.status = 0;
                }

                rbresult rbresult = new rbresult();
                rbresult.message = msg;
                rbresult.order_id = OrderID;
                ResponseObj.result = rbresult;
            }
            res = JsonConvert.SerializeObject(ResponseObj).ToString();
            return res;
        }
    }
}