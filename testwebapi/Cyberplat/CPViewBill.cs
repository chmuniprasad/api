﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pay2CartAPI.Response;
using Pay2CartAPI.Classes;
using System.Data;
using Pay2CartAPI.Cyberplat;
using CompLibrary;
using System.Configuration;
using Newtonsoft.Json;

namespace Pay2CartAPI.Cyberplat
{
    public class CPViewBill
    {

        public static string ViewBill(string operatorCode, string cardNumber, string mbgNo)
        {

            string res = "";
            viewBillObj viewBillObj = new viewBillObj();
            try
            {
                DataTable CyberConfing = CyberplatConfComp.getCyberPlatConf(operatorCode);
                if (CyberConfing.Rows.Count > 0)
                {
                    string url = CyberConfing.Rows[0]["verificationUrl"].ToString();
                    string SessionNo = DateTime.Parse(DateTime.Now.ToString()).ToString("dddMMMddyyyyHHmmss");

                    string data = "{'operatorCode':'" + operatorCode + "','customerNumber':'" + cardNumber + "','amount':'" + 10 + "','orderID':'" + SessionNo + "','url':'" + url + "','billGroupNumber':'" + mbgNo + "'}"; 
                    string electricityBillUrl = ConfigurationManager.AppSettings["viewBillUrl"];
                    string jsonData = ClsUtility.HttpPost(electricityBillUrl, data);

                    CyberResponse CyberResponse = JsonConvert.DeserializeObject<CyberResponse>(jsonData);

                    int status = CyberResponse.status;
                    if(status == 1)
                    {
                        string Result = CyberResponse.result.ToString();
                        string[] splitString = Result.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                        string API_ERROR = splitString[4];
                        string API_RESULT = splitString[5];

                        string[] Api_ErrArr = API_ERROR.Split('=');
                        string[] Api_ResArr = API_RESULT.Split('=');

                        int error = Convert.ToInt32(Api_ErrArr[1]);
                        int result = Convert.ToInt32(Api_ResArr[1]);
                        if(error == 7 && result == 1)
                        {
                            string API_PRICE = splitString[8];
                            string[] Api_PriceArr = API_PRICE.Split('=');

                            double price = Convert.ToDouble(Api_PriceArr[1]);
                            if(price > 0)
                            {
                                viewBillObj.status = 1;
                                viewBillObj.billAmount = (Int32)price;
                                viewBillObj.message = "Success.";
                            }
                            else
                            {
                                viewBillObj.status = 0;
                                viewBillObj.billAmount = 0;
                                viewBillObj.message = "You don't have due bill to pay.";
                            }

                        }
                        else
                        {
                            viewBillObj.status = 0;
                            viewBillObj.billAmount = 0;
                            viewBillObj.message = "You don't have due bill to pay.";
                        }
                    }
                    else
                    {
                        viewBillObj.status = 0;
                        viewBillObj.message = Common.OopsSomethingWentWrong;
                    }
                }
                else
                {
                    viewBillObj.message = Common.OopsSomethingWentWrong;
                }

            }catch(Exception Err)
            {
                viewBillObj.message = Common.OopsSomethingWentWrong; 
            }
            res = JsonConvert.SerializeObject(viewBillObj).ToString();
            return res;
        }
    }
}