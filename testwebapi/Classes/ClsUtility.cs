﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Pay2CartAPI.Classes
{
    public class ClsUtility
    {
        public static string HttpPost(string service, string param)
        {
            string response = string.Empty;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(service);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(param.Trim(new Char[] { ' ', '"', '.' }));
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    response = streamReader.ReadToEnd();
                }
            }

            return response;
        }
    }
}