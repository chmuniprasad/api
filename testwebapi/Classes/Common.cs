﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;

namespace Pay2CartAPI.Classes
{
    public class Common
    {
        private static string oopssomethingwentwrong;
        private static string AllFieldsRequired;
        private static string recordsExists;
        private static string norecordsExists;
        //added on 12.02.2018
        private static string IpAddress;
        //added on 20.03.2018
        private static string customerTransaction;
       
        public static string OopsSomethingWentWrong
        {
            get
            {
                if (oopssomethingwentwrong == null)
                {
                    oopssomethingwentwrong = "Oops! Something went wrong.";
                }
                return oopssomethingwentwrong;
            }
            set { oopssomethingwentwrong = value; }
        }

        public static string allFieldsRequired
        {
            get
            {
                if (AllFieldsRequired == null)
                {
                    AllFieldsRequired = "Please provide all required fields.";
                }
                return AllFieldsRequired;
            }
            set { AllFieldsRequired = value; }
        }
        public static string RecordsExists
        {
            get
            {
                if (recordsExists == null)
                {
                    recordsExists = "Records Exists.";
                }
                return recordsExists;
            }
            set { recordsExists = value; }
        }

        public static string NoRecordsExists
        {
            get
            {
                if (norecordsExists == null)
                {
                    norecordsExists = "No Records Exists.";
                }
                return norecordsExists;
            }
            set { norecordsExists = value; }
        }

        //added on 12.02.2018
        public static string IPaddress
        {
            get
            {
                if (IpAddress == null)
                {
                    IpAddress =  new WebClient().DownloadString("http://icanhazip.com");
                }
                return IpAddress;
            }
            set { IpAddress = value; }
        }

        public static string CustomerDailyMonthlyTransaction(bool monthly,bool daily)
        {
            try
            {
                if (monthly == false && daily==true )
                {
                    customerTransaction = "Your Monthly Transaction limit exceeded.";
                }
                else
                {
                    customerTransaction = "Your Daily Transaction limit exceeded.";
                }
            }
            catch (Exception Err) { }
            return customerTransaction;

        }

        //sms to send mobile number
        public static string sendSMS(string mobile, string message)
        {
            message = HttpUtility.UrlEncode(message);
            using (var wb = new WebClient())
            {
                byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , "IMdDydxNVCc-FZnwyrL5ArXJzcdANGEdcLuZbuqKPr"},
                {"numbers" , mobile},
                {"message" , message},
                {"sender" , "PYCART"}
                });
                string result = System.Text.Encoding.UTF8.GetString(response);
                return result;
            }
        }
    }
}