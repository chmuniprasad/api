﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pay2CartAPI.Response
{
    public class ResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object result { get; set; }
    }
    public class ProductReviewResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public productreviews result { get; set; }
    }
    public class productreviews
    {
        public string text { get; set; }
        public int isChecked { get; set; }
        //public string message { get; set; }
    }
    public class ProductReviewsResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public ProductReviews result { get; set; }
    }

    public class ProductReviews
    {
        public object reviews { get; set; }
    }
    public class CustomerProfileResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object customerdetails { get; set; }
    }
    public class RechargerHistoryResponseObj
    {
        public int status { get; set; }
        public object result { get; set; }
    }
    public class RechargerHistory
    {
        public object history { get; set; }
    }


    public class OrderDetailsByOrderidResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object result { get; set; }
        public object deliveryAddress { get; set; }
        public object delivery_days { get; set; }
        public object orderDetails { get; set; }
    }


    public class CreateOrderResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object order_id { get; set; }
    }

    public class CreateOrderPaymentResponseObj
    {
        public createorderPaymentResult result { get; set; }
        public int status { get; set; }
       
    }
    public class createorderPaymentResult
    {
        public string message { get; set; }
        public object order_id { get; set; }
    }

    public class SummaryResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object pricingDetails { get; set; }
        public object paymentmethods { get; set; }
        public object userWalletAmount { get; set; }
        public string deliverDays { get; set; }
        //public decimal subtotal { get; set; }
        //public decimal shippingtotal { get; set; }
        //public decimal  Total { get; set; }
    }

    public class reOrderResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        
    }
    public class ProductBysubCategoryResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public ProductBysubCategory result { get; set; }
    }
    public class ProductBysubCategory
    {
       public object products { get; set; }
    }

    public class ProductsSubCategoryResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public SubCategory result { get; set; }
    }
    public class ProductStockResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        
    }

    public class SubCategory
    {
        public object subcategories { get; set; }

    }
    public class RechargeResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public Rechargenetworks result { get; set; }
    }
    public class Rechargenetworks
    {
        public object networks { get; set; }
       
    }

    public class DthResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public DthOperators result { get; set; }
    }
    public class DthOperators
    {
        public object operators { get; set; }

    }

    public class ElectricityResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public ElectricityBoards result { get; set; }
    }
    public class ElectricityBoards
    {
        public object electricityBoards { get; set; }

    }

    public class GasbillResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public gasprovider result { get; set; }
    }
    public class gasprovider
    {
        public object gasBillProviders { get; set; }

    }
    public class BroadbandResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public broadbandprovider result { get; set; }
    }
    public class broadbandprovider
    {
        public object broadBandProviders { get; set; }

    }
    public class landlineResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public landlinedprovider result { get; set; }
    }
    public class landlinedprovider
    {
        public object landLineProviders { get; set; }

    }
    public class UserLoginResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object Userdetails { get; set; }
    }

    public class ShopingCartCountResponse
    {
        public int status { get; set; }
        public string message { get; set; }
       
        public int cartCount { get; set; }
    }
    public class ShopingCartDeleteItemResponse
    {
        public string message { get; set; }
        public int status { get; set; }
        public int cartTotalPrice { get; set; }
    }
    public class ShopingCartQuantityChange
    {
        public string message { get; set; }
        public int status { get; set; }
        public int quantity { get; set; }
        public int stockAvailable { get; set; }
        public decimal productPrice { get; set; }
        public int minimalQuantity { get; set; }
        public object cartTotalPrice { get; set; }

    }
    public class UserAddResponseObj
    {
        public string message { get; set; }
        public int statuscode { get; set; }
        public object id_customer { get; set; }
    }
    public class UserOtpResponseObj
    {
        public string message { get; set; }
        public int statuscode { get; set; }
        public object Userdetails { get; set; }
    }
    public class ChangePasswordResponse
    {
        public string message { get; set; }
        public int status { get; set; }
        public object customerdetails { get; set; }
    }
    public class ProductDetailsResobj
    {
        public string message { get; set; }
        public int status { get; set; }
        public productDetails result { get; set; }
        public productsfromsameCategory ProductsFromSameCategory { get; set; }
    }
    public class productDetails
    {
        public object ProductDetails { get; set; }
        public decimal totalRatingOfProduct { get; set; }
        public int totalReviews { get; set; }

    }
    public class productsfromsameCategory
    {
        public object Title { get; set; }
        public object productsFromSameCategory { get; set; }

    }

    public class ShopingCartResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public ShopingCartresult result { get; set; }
    }

    public class ShopingCartresult
    {
        public object cartProducts { get; set; }
        public object totalPrice { get; set; }
       
    }


    public class HomepageResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public Homepageresult result { get; set; }
    }

    public class Homepageresult
    {
        public object activeServices { get; set; }
        public object banners { get; set; }
        public object fullBanners { get; set; }
        public object categories { get; set; }
        public object shopByCategory { get; set; }
        public HomePageProductList1 productlist1 { get; set; }
        public HomePageProductList2 productlist2 { get; set; }
        public BestOffers1 bestOffers1 { get; set; }
        public BestOffers2 bestOffers2 { get; set; }
        public BestOffers3 bestOffers3 { get; set; }
    }
    public class BestOffers1
    {
        public object title { get; set; }
        public object bestOffers1 { get; set; }
        public object url { get; set; }
    }
    public class BestOffers2
    {
        public object title { get; set; }
        public object bestOffers2 { get; set; }
        public object url { get; set; }
    }
    public class BestOffers3
    {
        public object title { get; set; }
        public object bestOffers3 { get; set; }
        public object url { get; set; }
    }

    public class HomePageProductList1
    {
        public object title { get; set; }
        public object newProducts { get; set; }
        public object url { get; set; }
    }

    public class DeliveryResponseObj
    {
        public object status { get; set; }
        public object message { get; set; }
        
    }

    public class HomePageProductList2
    {
        public object title { get; set; }
        public object FeaturedProducts { get; set; }
        public object url { get; set; }
    }

    public class RechargeBillPaymentsObj
    {
        public int status { get; set; }
        public string message { get; set; }
        public rbresult result { get; set; }
        public int pendingAmount { get; set; }
    }

    public class rbresult
    {
        public string message { get; set; }
        public string order_id { get; set; }
    }
    public class CyberResponse
    {
        public int status { get; set; }
        public object result { get; set; }
    }

    public class InsufficientRechargeFundsObj
    {
        public int status { get; set; }
        public string message { get; set; }
        public int pendingAmount { get; set; }
        public int orderID { get; set; }

    }

    public class InsufficientRechargeFundsObjWallet
    {
        public int status { get; set; }
        public string message { get; set; }
        public int pendingAmount { get; set; }
        public string orderID { get; set; }

    }
    public class viewBillObj
    {
        public int status { get; set; }
        public string message { get; set; }
        public int billAmount { get; set; }
    }

    public class AddShopingCartResponseObj
    {
        public string message { get; set; }
        public int status { get; set; }
        public object guestid { get; set; }
        public object cartid { get; set; }
        public object quantity { get; set; }
        public object productPrice { get; set; }
        public object cartTotalPrice { get; set; }
    }
    public class ShippingCarrierResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public object carriers { get; set; }
    }
    public class ContactusResponse
    {
        public int status { get; set; }
        public string message { get; set; }
       
    }
    public class GetStatesResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public AllStates result { get; set; }
    }
    public class AllStates
    {
        public object states { get; set; }
        
    }

}