﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Pay2CartAPI.Response;
using Pay2CartAPI.Models;
using Pay2CartAPI.Classes;
using Pay2CartAPI.Cyberplat;

namespace Pay2CartAPI.Controllers
{
    public class ViewBillController : ApiController
    {
        public string response = "";

        [HttpPost]
        [Authorize]
        public HttpResponseMessage getbill([FromBody][Required]ViewBill viewBill)
        {
            ResponseObj ResponseObj = new ResponseObj();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                if (viewBill != null)
                {
                    if (ModelState.IsValid)
                    {
                        if (viewBill.operatorCode == "62" && (viewBill.mbgNo == "" || viewBill.mbgNo == null))
                        {
                            ResponseObj.message = "Bill group number is required.";
                        }
                        else
                        {
                            response = CPViewBill.ViewBill(viewBill.operatorCode.ToString(), viewBill.cardNumber.ToString(), viewBill.mbgNo.ToString());
                            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
                            return HttpResponseMessage;
                        }
                    }
                    else
                    {
                        string msg;
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                        {
                            msg = error.ErrorMessage;
                        }
                        else
                        {
                            msg = Common.allFieldsRequired;
                        }
                        ResponseObj.message = msg;
                    }
                }
                else
                {
                    ResponseObj.message = Common.allFieldsRequired;
                }
            }
            catch(Exception Err)
            {
                ResponseObj.message = Common.OopsSomethingWentWrong;
            }
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

       
    }
}
