﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using CCA.Util;
using System.Collections.Specialized;
using System.Xml;
using System.Web.Http;
using System.Net.Http;
using System.Text;
using Pay2CartAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace Pay2CartAPI.Controllers
{
    public class RsaController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage getRsa([FromBody][Required]Rsa rsa )
        {
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            String message = "";
            try
            {
                if (rsa.order_id !="" || rsa.order_id != null )
                {
                    string queryUrl = "https://secure.ccavenue.com/transaction/getRSAKey";
                    string vParams = "access_code=AVHV77FD40AW47VHWA&order_id=" + rsa.order_id;
                    // Url Connection
                    message = postPaymentRequestToGateway(queryUrl, vParams);
                }
                
            }
            catch (Exception exp)
            {
                //Response.Write("Exception " + exp);
            }
            HttpResponseMessage.Content = new StringContent(message, Encoding.UTF8, "application/text");
            return HttpResponseMessage;
     
        }

        private string postPaymentRequestToGateway(String queryUrl, String urlParam)
        {
            String message = "";
            try
            {
                StreamWriter myWriter = null;// it will open a http connection with provided url
                WebRequest objRequest = WebRequest.Create(queryUrl);//send data using objxmlhttp object
                objRequest.Method = "POST";
                //objRequest.ContentLength = TranRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";//to set content type
                myWriter = new System.IO.StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(urlParam);//send data
                myWriter.Close();//closed the myWriter object

                // Getting Response
                System.Net.HttpWebResponse objResponse = (System.Net.HttpWebResponse)objRequest.GetResponse();//receive the responce from objxmlhttp object 
                using (System.IO.StreamReader sr = new System.IO.StreamReader(objResponse.GetResponseStream()))
                {
                    message = sr.ReadToEnd();
                }
            }
            catch (Exception exception)
            {
                Console.Write("Exception occured while connection." + exception);
            }
            return message;
        }
    }
}
