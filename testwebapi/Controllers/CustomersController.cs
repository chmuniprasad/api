﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pay2CartAPI.Models;
using Pay2CartAPI.Response;
using System.Text;
using Pay2CartAPI.Classes;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Web;
using System.Drawing.Imaging;
using System.Globalization;

namespace Pay2CartAPI.Controllers
{
    public class CustomersController : ApiController
    {
        [HttpPost]
        [Authorize]
        public HttpResponseMessage addCustomer([FromBody][Required]addCustomer addCustomer)
        {
            string response = "";
            UserAddResponseObj ResponseObj = new UserAddResponseObj();
            try
            {
                if (addCustomer != null)
                {
                    if (ModelState.IsValid)
                    {
                        DataTable getcustomerbyEmail = CustomerComp.getcustomerbyEmail(addCustomer.email);
                        DataTable getcustomerbyphone = CustomerComp.getcustomerbyphone(addCustomer.mobile);
                         if (getcustomerbyEmail.Rows.Count <= 0 && getcustomerbyphone.Rows.Count <= 0)
                        {
                            long res = CustomerComp.addCustomer(addCustomer.email, addCustomer.passwd.ToString(), addCustomer.mobile);
                            if (res > 0)
                            {
                                //DataTable regiseter = CustomerComp.RegisterDetails(addCustomer.email, addCustomer.mobile.ToString());

                                Random randOtp = new Random();
                                String otp = randOtp.Next(0, 999999).ToString("D6");

                                int userId = (Int32)res;

                                long newOtpRegister = CustomerDB.InsertOtp(otp, "PhoneValidation", userId, 1, 1, DateTime.Now, DateTime.Now);
                                string SMSText = "Hi, Please use this otp to validate your account with pay2cart  " + otp;

                                Common.sendSMS(addCustomer.mobile, SMSText);
                               // string JSONresult;
                               // JSONresult = JsonConvert.SerializeObject(regiseter);
                               // JSONresult = JSONresult.Replace("[", "");
                                //JSONresult = JSONresult.Replace("]", "");
                                //JObject jsonObject = JObject.Parse(JSONresult);


                                ResponseObj.statuscode = 1;
                                ResponseObj.message = "Registration Success. Please validate Phone";
                                ResponseObj.id_customer = res;
//insert guest cart items to new registered customer
                               try
                                {
                                    if (int.Parse(addCustomer.id_guest) > 0)
                                    {
                                        DataTable getGuestCartdetails = ShopingCartComp.GetCartdetails(int.Parse(addCustomer.id_guest));
                                        if (getGuestCartdetails.Rows.Count > 0)
                                        {
                                            for (int k = 0; k <= getGuestCartdetails.Rows.Count - 1; k++)
                                            {
                                                // checking product had bundle items or not?
                                                DataTable ProdctBudles = ShopingCartComp.CheckProductIncluesBundle((Int32)getGuestCartdetails.Rows[k]["ProductId"]);
                                                int? ParentItemId = null; int? BundleItemId = null;
                                                if (ProdctBudles.Rows.Count != 0)
                                                {
                                                    int cnt = 0;
                                                    string AttributesXml = "";
                                                    DataTable checkprocutinCART = ShopingCartComp.AddShopingCartItem(userId, (Int32)getGuestCartdetails.Rows[k]["ProductId"]);
                                                    if (checkprocutinCART.Rows.Count > 0)
                                                    {
                                                        long result = ShopingCartComp.UpdateCartItmes(userId, (Int32)getGuestCartdetails.Rows[k]["ProductId"], (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                    }
                                                    else
                                                    {
                                                        long result = ShopingCartComp.AddShopingCartItem(userId, ParentItemId, BundleItemId, (Int32)getGuestCartdetails.Rows[k]["ProductId"], AttributesXml, (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                    }

                                                    int parentitem = 0;
                                                    DataTable bundle = ShopingCartComp.GetParentItemID((Int32)getGuestCartdetails.Rows[k]["ProductId"], userId);
                                                    parentitem = (Int32)bundle.Rows[0][0];
                                                    for (int i = 0; i <= ProdctBudles.Rows.Count - 1; i++)
                                                    {
                                                        string AttributesXm = "";
                                                        long result = ShopingCartComp.AddShopingCartItem(userId, parentitem, (Int32)ProdctBudles.Rows[i]["Id"], (Int32)ProdctBudles.Rows[i]["ProductId"], AttributesXm, (Int32)ProdctBudles.Rows[i]["Quantity"]);
                                                        cnt += 1;
                                                    }
                                                }
                                                else
                                                {
                                                    string AttributesXml = "";
                                                    DataTable checkprocutinCART = ShopingCartComp.AddShopingCartItem(userId, (Int32)getGuestCartdetails.Rows[k]["ProductId"]);
                                                    long result1 = 0;
                                                    if (checkprocutinCART.Rows.Count > 0)
                                                    {
                                                        result1 = ShopingCartComp.UpdateCartItmes(userId, (Int32)getGuestCartdetails.Rows[k]["ProductId"], (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                    }
                                                    else
                                                    {
                                                        result1 = ShopingCartComp.AddShopingCartItem(userId, ParentItemId, BundleItemId, (Int32)getGuestCartdetails.Rows[k]["ProductId"], AttributesXml, (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                    }
                                                }
                                            }
                                            //After adding guest cart items to LoginUser remove items from guest cart.
                                            long removeitemsfromGuestCart = OrdersComp.RemoveFromCart(int.Parse(addCustomer.id_guest));
                                        }
                                    }
                                }
                                catch (Exception Err) { }

                            }
                            else
                            {
                                ResponseObj.message = Common.OopsSomethingWentWrong;
                                ResponseObj.statuscode = 0;
                            }
                        }
                        else
                        {
                            ResponseObj.statuscode = 0;
                            ResponseObj.message = "User Already Registered Please Login";
                            ResponseObj.id_customer = "";
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage login([FromBody][Required]CustomerLogin CustomerLogin)
        {
            string response = "";
            UserLoginResponseObj ResponseObj = new UserLoginResponseObj();
            try
            {
                if (CustomerLogin != null)
                {
                    if (ModelState.IsValid)
                    {
                        bool MobileOrEmail = false;
                        if (CustomerLogin.userName.Contains("@")) { MobileOrEmail = true; }
                        
                        DataTable UserByUserName = CustomerComp.getCustomerDetailsByUser(CustomerLogin.userName, MobileOrEmail);
                        if (UserByUserName.Rows.Count > 0)
                        {
                            string PasswordSlatKey = UserByUserName.Rows[0]["PasswordSalt"].ToString();
                            CustomerLogin.password = Encryption.CreatePasswordHash(CustomerLogin.password, PasswordSlatKey, "SHA1"); ;
                            DataTable LoginUser = new DataTable();
                            LoginUser = CustomerComp.login(UserByUserName.Rows[0]["Username"].ToString(), CustomerLogin.password.ToString());

                            if (LoginUser != null)
                            {
                                DataTable cusotmerDet = CustomerComp.CustomerProfileDetails((Int32)LoginUser.Rows[0]["id_customer"]);
                                DataTable userDetails = CustomerComp.getCustomerDetails((Int32)LoginUser.Rows[0]["id_customer"]);
                                DataTable LoginDetails = new DataTable();
                                try
                                {
                                    if (userDetails.Rows.Count > 0)
                                    {
                                       
                                        if(userDetails.Rows[0]["id_gender"].ToString()=="" || userDetails.Rows[0]["id_gender"].ToString() == null)
                                        {
                                            LoginUser.Rows[0]["id_gender"] = 0;
                                        }
                                        else
                                        {
                                            LoginUser.Rows[0]["id_gender"] = userDetails.Rows[0]["id_gender"].ToString();
                                        }
                                        LoginUser.Rows[0]["firstname"] = userDetails.Rows[0]["firstname"].ToString();
                                       // LoginUser.Rows[0]["birthday"] = (DateTime)userDetails.Rows[0]["birthday"];
                                        LoginUser.Rows[0]["lastname"] = userDetails.Rows[0]["lastname"].ToString();

                                        LoginDetails = LoginUser.Clone();
                                        LoginDetails.Columns["birthday"].DataType = typeof(System.String);
                                        foreach (DataRow row in LoginUser.Rows)
                                        {
                                            LoginDetails.ImportRow(row);
                                        }
                                        if(LoginUser.Rows[0]["birthday"].ToString() != "" || LoginUser.Rows[0]["birthday"] != null)
                                        {
                                            DateTime dob = (DateTime)userDetails.Rows[0]["birthday"];
                                            string BirthDay = dob.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                                            LoginDetails.Rows[0]["birthday"] = BirthDay;
                                        }
                                        
                                    }
                                }
                                catch (Exception Err) { }

                                string JSONresult;
                                JSONresult = JsonConvert.SerializeObject(LoginDetails);
                                JSONresult = JSONresult.Replace("[", "");
                                JSONresult = JSONresult.Replace("]", "");
                                JObject jsonObject = JObject.Parse(JSONresult);

                                ResponseObj.status = 1;
                                ResponseObj.message = "Login successfully.";
                                ResponseObj.Userdetails = jsonObject;

                                if (int.Parse(CustomerLogin.id_guest) > 0)
                                {
                                    DataTable getGuestCartdetails = ShopingCartComp.GetCartdetails(int.Parse(CustomerLogin.id_guest));
                                    if (getGuestCartdetails.Rows.Count > 0)
                                    {
                                        for (int k = 0; k <= getGuestCartdetails.Rows.Count - 1; k++)
                                        {
                                            // checking product had bundle items or not?
                                            DataTable ProdctBudles = ShopingCartComp.CheckProductIncluesBundle((Int32)getGuestCartdetails.Rows[k]["ProductId"]);
                                            int? ParentItemId = null; int? BundleItemId = null;
                                            if (ProdctBudles.Rows.Count != 0)
                                            {
                                                int cnt = 0;
                                                string AttributesXml = "";
                                                DataTable checkprocutinCART = ShopingCartComp.AddShopingCartItem((Int32)LoginUser.Rows[0]["id_customer"], (Int32)getGuestCartdetails.Rows[k]["ProductId"]);
                                                if (checkprocutinCART.Rows.Count > 0)
                                                {
                                                    long res = ShopingCartComp.UpdateCartItmes((Int32)LoginUser.Rows[0]["id_customer"], (Int32)getGuestCartdetails.Rows[k]["ProductId"], (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                }
                                                else
                                                {
                                                    long res = ShopingCartComp.AddShopingCartItem((Int32)LoginUser.Rows[0]["id_customer"], ParentItemId, BundleItemId, (Int32)getGuestCartdetails.Rows[k]["ProductId"], AttributesXml, (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                }

                                                int parentitem = 0;
                                                DataTable bundle = ShopingCartComp.GetParentItemID((Int32)getGuestCartdetails.Rows[k]["ProductId"], (Int32)LoginUser.Rows[0]["id_customer"]);
                                                parentitem = (Int32)bundle.Rows[0][0];
                                                for (int i = 0; i <= ProdctBudles.Rows.Count - 1; i++)
                                                {
                                                    string AttributesXm = "";
                                                    long result = ShopingCartComp.AddShopingCartItem((Int32)LoginUser.Rows[0]["id_customer"], parentitem, (Int32)ProdctBudles.Rows[i]["Id"], (Int32)ProdctBudles.Rows[i]["ProductId"], AttributesXm, (Int32)ProdctBudles.Rows[i]["Quantity"]);
                                                    cnt += 1;
                                                }
                                            }
                                            else
                                            {
                                                string AttributesXml = "";
                                                DataTable checkprocutinCART = ShopingCartComp.AddShopingCartItem((Int32)LoginUser.Rows[0]["id_customer"], (Int32)getGuestCartdetails.Rows[k]["ProductId"]);
                                                long res = 0;
                                                if (checkprocutinCART.Rows.Count > 0)
                                                {
                                                    res = ShopingCartComp.UpdateCartItmes((Int32)LoginUser.Rows[0]["id_customer"], (Int32)getGuestCartdetails.Rows[k]["ProductId"], (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                }
                                                else
                                                {
                                                    res = ShopingCartComp.AddShopingCartItem((Int32)LoginUser.Rows[0]["id_customer"], ParentItemId, BundleItemId, (Int32)getGuestCartdetails.Rows[k]["ProductId"], AttributesXml, (Int32)getGuestCartdetails.Rows[k]["Quantity"]);
                                                }
                                            }
                                        }
                                        //After adding guest cart items to LoginUser remove items from guest cart.
                                    long removeitemsfromGuestCart = OrdersComp.RemoveFromCart(int.Parse(CustomerLogin.id_guest));
                                    }
                                }
                            }
                            else
                            {
                                ResponseObj.status = 0;
                                ResponseObj.message = "Authentication Failed. Please try with valid credentials.";
                            }
                        }
                        else
                        {
                            ResponseObj.status = 0;
                            ResponseObj.message = "Customer does not exists. Login failed.";
                        }

                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj);
          
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage Otpvalidation([FromBody][Required]otpvaidate otpvaidat)
        {
            string response = "";
            UserOtpResponseObj ResponseObj = new UserOtpResponseObj();
            try
            {
                if (otpvaidat != null)
                {
                    if (ModelState.IsValid)
                    {
                        DataTable getcustomerbyEmail = CustomerComp.getcustomerbyEmail(otpvaidat.email);
                        DataTable getcustomerbyphone = CustomerComp.getcustomerbyphone(otpvaidat.mobile);
                        if (getcustomerbyEmail.Rows.Count <= 0 && getcustomerbyphone.Rows.Count <= 0)
                        {
                            
                            DataTable userOtp = CustomerDB.GetOtpbyUserid(otpvaidat.id_customer);
                            DataTable getCustomerOTP = CustomerComp.getCustomerOtp(otpvaidat.id_customer);
                            string currentOtp = getCustomerOTP.Rows[0]["otp"].ToString();
                            if (otpvaidat.otp != null)
                            {
                                string systemgen = otpvaidat.otp.ToString();
                                if (currentOtp == systemgen)
                                {
                                    string startTime = userOtp.Rows[0]["createdAt"].ToString();
                                    string endTime = DateTime.Now.ToString();

                                    TimeSpan duration = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime));

                                    if (duration.TotalMinutes < 30)
                                    {
                                        long res = CustomerDB.UpdateOtpValidation(otpvaidat.id_customer, 1, 0);
                                        long resuslt = CustomerDB.UpdateCustomerRole(otpvaidat.id_customer, 3);
                                        DataTable regiseter = CustomerComp.RegisterDetails(otpvaidat.email, otpvaidat.mobile.ToString());
                                        string JSONresult;
                                        JSONresult = JsonConvert.SerializeObject(regiseter);
                                        JSONresult = JSONresult.Replace("[", "");
                                        JSONresult = JSONresult.Replace("]", "");
                                        JObject jsonObject = JObject.Parse(JSONresult);


                                        ResponseObj.statuscode = 1;
                                        ResponseObj.message = "Phone vaildate sucessfully.";
                                        ResponseObj.Userdetails = jsonObject;
                                    }
                                    else
                                    {
                                        long res = CustomerDB.UpdateOtpValidation(otpvaidat.id_customer, 0, 0);
                                        ResponseObj.statuscode = 0;
                                        ResponseObj.message = "OTP timed out";

                                    }
                                }
                                else
                                {
                                    long res = CustomerDB.UpdateOtpValidation(otpvaidat.id_customer, 0, 0);

                                    ResponseObj.statuscode = 0;
                                    ResponseObj.message = "Please enter valid OTP";
                                    
                                }


                            }
                            else
                            {
                                ResponseObj.message = "OTP should not empty";
                                ResponseObj.statuscode = 0;
                            }
                        }
                        else
                        {
                            ResponseObj.statuscode = 0;
                            ResponseObj.message = "mobile not validated.";
                            ResponseObj.Userdetails = "";
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }


        [HttpGet]
        [Authorize]
        public HttpResponseMessage getCustomerDetails(int id_customer)
        {
            
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";

            if (id_customer > 0)
            {
                DataTable userDetails = CustomerComp.getCustomerDetails(id_customer);
                if (userDetails.Rows.Count > 0)
                {
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = userDetails;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;
                    ResponseObj.result = "Customerid is not valid";
                }
            }
            else
            {
                ResponseObj.status = 0;
                ResponseObj.message = "CustomerID is required.";
            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage UpdateProfile([FromBody]CustomerProfile customerProfile)
        {
            string response = "";
            CustomerProfileResponseObj ResponseObj = new CustomerProfileResponseObj();
            try
            {
                if (customerProfile != null)
                {
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            if (customerProfile.customerimage != null && customerProfile.customerimage != "")
                            {
                                byte[] imageBytes = Convert.FromBase64String(customerProfile.customerimage);
                                MemoryStream ms9 = new MemoryStream(imageBytes, 0, imageBytes.Length);
                                ms9.Write(imageBytes, 0, imageBytes.Length);
                                Image image9 = Image.FromStream(ms9, true);
                               
                                string imageName9 = DateTime.Parse(DateTime.Now.ToString()).ToString("ddyyyyHHmmss") + ".png";
                                var pat= Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/"), imageName9);
                                image9.Save(pat, ImageFormat.Png);
                                long profileImage = CustomerComp.updatecustomerImage(customerProfile.id_customer, imageName9);

                            }
                        }
                        catch (Exception Eff) { }
                       
                        string Gender = "";
                        if (int.Parse(customerProfile.id_gender) == 2)
                        {
                            Gender = "F";
                        }
                        else if (int.Parse(customerProfile.id_gender) == 1)
                        {
                            Gender = "M";
                        }
                        else
                        {
                            Gender = "";
                        }
                        string DOB = "";
                        try
                        {
                            if (customerProfile.years == "" || customerProfile.years == null)
                            {
                                DOB = "";
                            }
                            else if (customerProfile.months == "" || customerProfile.months == null)
                            {
                                DOB = "";
                            }
                            else if (customerProfile.days == "" || customerProfile.days == null)
                            {
                                 DOB = "";
                            }
                            else
                            {
                                 DOB = customerProfile.years + "-" + customerProfile.months + "-" + customerProfile.days;
                            }
                        }
                        catch (Exception Err) { }
                       if( customerProfile.Company == "" || customerProfile.Company == null) { customerProfile.Company = ""; } 
                       if(customerProfile.City == "" || customerProfile.City == null) { customerProfile.City = ""; }
                       if(customerProfile.Phone == "" || customerProfile.Phone == null) { customerProfile.Phone = ""; }
                       if(customerProfile.Signature == "" || customerProfile.Signature == null) { customerProfile.Signature = ""; }
                       if(customerProfile.VatNumber == "" || customerProfile.VatNumber == null) { customerProfile.VatNumber = ""; }
                        long res = CustomerComp.UpdateCustomerProfile(customerProfile.id_customer, Gender, customerProfile.firstname, customerProfile.lastname, DOB, customerProfile.Company, customerProfile.City, customerProfile.Phone, customerProfile.Signature, customerProfile.VatNumber);
                        DataTable cusotmerDet = CustomerComp.CustomerProfileDetails(customerProfile.id_customer);
                        
                        Customerdetails customerdetails = new Customerdetails();
                       
                        if (res == -1)
                        {
                            try
                            {
                                long result = CustomerComp.UpdateCustomerData(customerProfile.id_customer, int.Parse(customerProfile.id_gender), customerProfile.firstname, customerProfile.lastname, DOB);
                            }
                            catch (Exception Err) { }
                            DataTable userDetails = CustomerComp.getCustomerDetails(customerProfile.id_customer);
                            customerdetails.id_customer = customerProfile.id_customer;
                            if (cusotmerDet.Rows.Count>0)
                            {
                                for(int i=0;i<= cusotmerDet.Rows.Count-1;i++)
                                {
                                    if(cusotmerDet.Rows[i]["Key"].ToString()== "Gender")
                                    {
                                        if(cusotmerDet.Rows[i]["Value"].ToString()=="M")
                                        {
                                            customerdetails.id_gender = "1";
                                        }
                                        else if (cusotmerDet.Rows[i]["Value"].ToString() == "F")
                                        {
                                            customerdetails.id_gender = "2";
                                        }
                                        else
                                        {
                                            customerdetails.id_gender = "0";
                                        }
                                    }
                                    else if (cusotmerDet.Rows[i]["Key"].ToString() == "FirstName")
                                    {
                                      customerdetails.firstname = cusotmerDet.Rows[i]["Value"].ToString();
                                    }
                                    else if (cusotmerDet.Rows[i]["Key"].ToString() == "LastName")
                                    {
                                        customerdetails.lastname = cusotmerDet.Rows[i]["Value"].ToString();
                                        //customerdetails.lastname = customerProfile.lastname;
                                    }
                                    else if (cusotmerDet.Rows[i]["Key"].ToString() == "Phone")
                                    {
                                        customerdetails.mobile = cusotmerDet.Rows[i]["Value"].ToString();
                                    }
                                    else if (cusotmerDet.Rows[i]["Key"].ToString() == "DateOfBirth")
                                    {
                                        customerdetails.birthday = cusotmerDet.Rows[i]["Value"].ToString();
                                    }
                                    else
                                    {
                                        customerdetails.birthday = "";
                                       
                                        customerdetails.message = "";
                                    }

                                    if(customerdetails.lastname == "" || customerdetails.lastname == null )
                                    {
                                        customerdetails.lastname = customerProfile.lastname;
                                    }
                                }
                                try
                                {
                                    if (userDetails.Rows.Count > 0)
                                    {
                                        DataTable LoginDetails = new DataTable();
                                        customerdetails.firstname = userDetails.Rows[0]["firstname"].ToString();
                                        customerdetails.birthday = userDetails.Rows[0]["birthday"].ToString();
                                        customerdetails.lastname = userDetails.Rows[0]["lastname"].ToString();
                                        customerdetails.mobile = userDetails.Rows[0]["mobile"].ToString();
                                        customerdetails.email = userDetails.Rows[0]["Email"].ToString();
                                        customerdetails.passwd = userDetails.Rows[0]["Password"].ToString();
                                        customerdetails.customerimage = userDetails.Rows[0]["customerimage"].ToString();
                                        customerdetails.adharnumber = userDetails.Rows[0]["adharnumber"].ToString();

                                        LoginDetails = userDetails.Clone();
                                        LoginDetails.Columns["birthday"].DataType = typeof(System.String);
                                        foreach (DataRow row in userDetails.Rows)
                                        {
                                            LoginDetails.ImportRow(row);
                                        }
                                        DateTime dob = (DateTime)userDetails.Rows[0]["birthday"];
                                        string BirthDay = dob.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                                        userDetails.Rows[0]["birthday"] = BirthDay;
                                        customerdetails.birthday = BirthDay;
                                    }
                                    //customerdetails.id_gender = Gender;
                                }
                                catch (Exception Err) { }
                               

                            }
                            ResponseObj.status = 1;
                            ResponseObj.message = "Profile Updated Sucessfully.";
                            ResponseObj.customerdetails = customerdetails;
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage getRegisteredCustomers(string customer)
        {

            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
           
            try
            {
                customer = customer.ToLower();
                int customerRole=3;
                if (customer == "guest" || customer== "registered"  )
                {
                    
                    if (customer == "guest")
                    {
                        customerRole = 4;//Guest users can display
                    }
                    else if (customer == "registered")
                    {
                        customerRole = 3;  //Registered  users will display
                    }
                    

                   
                    DataTable userDetails = CustomerComp.RegisteredCustomers(customerRole);
                    if (userDetails.Rows.Count > 0)
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = userDetails;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message =Common.NoRecordsExists;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "Please enter valid text to get customers.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;

        }


    }
}
