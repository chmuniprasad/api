﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System.IO;
using DBLibrary;
using System.Web;
using System.Text;
using Pay2CartAPI.Classes;

namespace testwebapi.Controllers
{
    public class aboutusController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage aboutus()
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                ResponseObj.result = "About Us";
                ResponseObj.status = 1;
                ResponseObj.message = "We Pay2Cart.com is a one stop to all your online shopping platform, " +
                    "where you been delighted with immense variety of all product categories. " +
                    "Pay2Cart.com is having all that is needed when you look to for a speed and quality " +
                    "product delivery for your special occasion or any important event been remembered by " +
                    "gifting someone special a product that makes their life easier. " +
                    "We are having immense varieties of all popular brands in our products at a cost that " +
                    "gives a happy shopping experience with us. Pay2Cart.com is not just a medium of selling " +
                    "but also to grow your business where you by just sitting in your office you be able to target " +
                    "customer’s country wide by which the business can be increased in just few months by doubling " +
                    "your profits through reaching right customer at right time. Pay2Cart.com is the preferred " +
                    "choice of hundreds of thousands of online shoppers.Once you have zeroed in on your favourite products, " +
                    "simply place the order by filling in the details; the products will be delivered right at your doorstep. " +
                    "Come to Pay2Cart.com and make your shopping easy and memorable.";

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }


    }
}
