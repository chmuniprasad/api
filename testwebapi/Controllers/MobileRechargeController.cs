﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Pay2CartAPI.Models;
using Pay2CartAPI.Response;
using Pay2CartAPI.Cyberplat;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class MobileRechargeController : ApiController
    {
        public string response = "";

        [HttpPost]
        [Authorize]
        public HttpResponseMessage doMobileRecharge([FromBody][Required]MobileRecharge MobileRecharge)
        {
           
            ResponseObj ResponseObj = new ResponseObj();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                if (MobileRecharge != null)
                {
                    
                    if (ModelState.IsValid)
                    {
                        response = CPMobileRecharge.doMobileRecharge(MobileRecharge.userId.ToString(), MobileRecharge.network_type.ToString(), MobileRecharge.network.ToString(), MobileRecharge.mobile_number.ToString(), MobileRecharge.rechargeAmount.ToString(), MobileRecharge.orderID);
                        HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
                        return HttpResponseMessage;
                    }
                    else
                    {
                        ResponseObj.message = Common.allFieldsRequired;
                    }
                }
                else
                {
                    ResponseObj.message = Common.OopsSomethingWentWrong;
                }
            }
            catch (Exception Err)
            {

            }
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        


    }
}
