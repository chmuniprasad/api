﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System;
using System.Linq;
using System.Text;
using Pay2CartAPI.Classes;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace testwebapi.Controllers
{
    public class HomeController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage home()
        {
            HomepageResponseObj ResponseObj = new HomepageResponseObj();
            string response = "";
           
            try
            {
                DataTable storedetails = PasswordComp.StoreInformation();
                DataTable activeServices = HomePageComp.activeServices();
                for(int i=0;i<= activeServices.Rows.Count-1;i++ )
                {
                    string icon = activeServices.Rows[i]["serviceIcon"].ToString();
                    var imgurl = storedetails.Rows[0]["Url"].ToString();
                    activeServices.Rows[i]["serviceIcon"] = imgurl + "Content/images/rbillservices/" + icon;
                }
               

                DataTable Banners = HomePageComp.GetHomepageBanners();

                for (int i = 0; i <= Banners.Rows.Count - 1; i++)
                {
                    string img = Banners.Rows[i]["mobilebanner"].ToString();
                    var imgurl = storedetails.Rows[0]["Url"].ToString();
                    //Banners.Rows[i]["image"] = imgurl + "Uploads/banners/" + img;
                    Banners.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/banners/" + img;

                }


                DataTable FullBanners= new DataTable();
                DataTable BestOffers1 = new DataTable();
                DataTable BestOffers2 = new DataTable();
                DataTable BestOffers3 = new DataTable();
                DataTable orrd = new DataTable();
                DataTable Categories = HomePageComp.GetHomepageCategories();
                DataTable shopbyCategories = HomePageComp.shopbyCategories();
                try
                {
                    orrd = shopbyCategories.Clone();
                    orrd.Columns["image"].DataType = typeof(System.String);
                    orrd.Columns["mobileimage"].DataType = typeof(System.String);
                    foreach (DataRow row in shopbyCategories.Rows)
                    {
                        orrd.ImportRow(row);
                    }

                    if (orrd.Rows.Count > 0)
                    {
                        for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                        {
                            DataTable storedetail = new DataTable();
                            DataTable getImagesLinks = new DataTable();
                          try
                            {
                                 storedetail = PasswordComp.StoreInformation();
                                 getImagesLinks = ProductsComp.GetImageFullNames((Int32)shopbyCategories.Rows[i]["mobileimage"]);

                            }
                            catch (Exception Err) { }

                            string mimetype = "";

                            var imgurl = storedetail.Rows[0]["Url"].ToString();
                            if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                            {
                                mimetype = ".jpg";
                            }
                            else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                            {
                                mimetype = ".png";
                            }
                            string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                            string  imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();
                           

                            orrd.Rows[i]["mobileimage"] = "" + imgName + mimetype + "";
                            orrd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";
                            orrd.Rows[i]["url"] = imgurl + "" + imgName;
                           
                        }
                    }
                }
                catch (Exception Err) { }
                DataTable newproductlist1 = HomePageComp.newproductlist1();
                DataTable newproductlist2 = HomePageComp.newproductlist2();

                 if (shopbyCategories.Rows.Count > 0)
                 {
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                    DataTable newProd = new DataTable();
                    try
                    {
                        newProd = newproductlist1.Clone();
                        newProd.Columns["image"].DataType = typeof(System.String);
                        newProd.Columns.Add("discount_percentage", typeof(System.Int32));

                        foreach (DataRow row in newproductlist1.Rows)
                        {
                            newProd.ImportRow(row);
                        }

                        if (newProd.Rows.Count > 0)
                        {
                            for (int i = 0; i <= newProd.Rows.Count - 1; i++)
                            {
                                if (newProd.Rows[i]["image"] != null)
                                {
                                    DataTable storedetail = PasswordComp.StoreInformation();
                                    try
                                    {
                                        DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)newproductlist1.Rows[i]["image"]);
                                        string mimetype = "";

                                        var imgurl = storedetail.Rows[0]["Url"].ToString();
                                        if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                                        {
                                            mimetype = ".jpg";
                                        }
                                        else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                                        {
                                            mimetype = ".png";
                                        }
                                        string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                                        string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();

                                        newProd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";

                                    }
                                    catch (Exception Err) { newProd.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG";  }
                                    
                                    
                                    try
                                    {
                                        if ((decimal)newProd.Rows[i]["original_cost"] != 0)
                                        {
                                            int percentComplete = 0;
                                            percentComplete = (Int32)Math.Round((decimal)(100 * ((decimal)newProd.Rows[i]["original_cost"] - (decimal)newProd.Rows[i]["orderprice"])) / (decimal)newProd.Rows[i]["original_cost"]);
                                            newProd.Rows[i]["discount_percentage"] = percentComplete;
                                        }
                                        else
                                        {
                                            newProd.Rows[i]["discount_percentage"] = 0;
                                        }
                                    }
                                    catch (Exception Err) { newProd.Rows[i]["discount_percentage"] = 0; }

                                    //replacing html strings
                                    string str = newProd.Rows[i]["description"].ToString();
                                    Regex rx = new Regex("<[^>]*>");
                                    // replace all matches with empty strin
                                    str = rx.Replace(str, "");
                                    RegexOptions options = RegexOptions.None;
                                    Regex regex = new Regex("[ ]{2,}", options);
                                    str = regex.Replace(str, " ");
                                    str = str.Trim();
                                    newProd.Rows[i]["description"] = str;

                                    // replace html strings in string
                                    string str2 = newProd.Rows[i]["description_short"].ToString();
                                    Regex rx2 = new Regex("<[^>]*>");
                                    str2 = rx2.Replace(str2, "");
                                    RegexOptions options2 = RegexOptions.None;
                                    Regex regex2 = new Regex("[ ]{2,}", options2);
                                    str2 = regex2.Replace(str2, " ");
                                    str2 = str2.Trim();
                                    newProd.Rows[i]["description_short"] = str2;
                                }
                                else
                                {
                                    newProd.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG";
                                }
                            }
                        }
                    }
                    catch (Exception Err) { }

                    HomePageProductList1 homePageProductList1 = new HomePageProductList1();
                    homePageProductList1.title = "New Arrivals";
                    homePageProductList1.newProducts = newProd;
                    homePageProductList1.url = "";

                    DataTable newProd2 = new DataTable();
                    try
                    {
                        newProd2 = newproductlist2.Clone();
                        newProd2.Columns["image"].DataType = typeof(System.String);
                        newProd2.Columns.Add("discount_percentage", typeof(System.Int32));
                        foreach (DataRow row in newproductlist2.Rows)
                        {
                            newProd2.ImportRow(row);
                        }

                        if (newProd2.Rows.Count > 0)
                        {
                            for (int i = 0; i <= newProd2.Rows.Count - 1; i++)
                            {
                                if (newProd2.Rows[i]["image"] != null)
                                {
                                    DataTable storedetail = PasswordComp.StoreInformation();
                                    try
                                    { 
                                    DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)newproductlist2.Rows[i]["image"]);
                                    string mimetype = "";

                                    var imgurl = storedetail.Rows[0]["Url"].ToString();
                                    if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                                    {
                                        mimetype = ".jpg";
                                    }
                                    else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                                    {
                                        mimetype = ".png";
                                    }
                                    string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                                    string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();


                                    newProd2.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";
                                    }
                                    catch (Exception Err) { newProd2.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG"; }
                                    try
                                    {
                                        //showing discount percentage of product
                                        if ((decimal)newProd2.Rows[i]["original_cost"] != 0)
                                        {
                                            int percentComplete = 0;
                                            percentComplete = (Int32)Math.Round((decimal)(100 * ((decimal)newProd2.Rows[i]["original_cost"] - (decimal)newProd2.Rows[i]["orderprice"])) / (decimal)newProd2.Rows[i]["original_cost"]);
                                            newProd2.Rows[i]["discount_percentage"] = percentComplete;
                                        }
                                        else
                                        {
                                            newProd2.Rows[i]["discount_percentage"] = 0;
                                        }
                                    }
                                    catch (Exception Err) { newProd2.Rows[i]["discount_percentage"] = 0; }
                                    //replacing html strings
                                    string str = newProd2.Rows[i]["description"].ToString();
                                    Regex rx = new Regex("<[^>]*>");
                                    str = rx.Replace(str, "");
                                    RegexOptions options = RegexOptions.None;
                                    Regex regex = new Regex("[ ]{2,}", options);
                                    str = regex.Replace(str, " ");
                                    str = str.Trim();
                                    newProd2.Rows[i]["description"] = str;

                                    // replace html strings in string
                                    string str2 = newProd2.Rows[i]["description_short"].ToString();
                                    Regex rx2 = new Regex("<[^>]*>");
                                    str2 = rx2.Replace(str2, "");
                                    RegexOptions options2 = RegexOptions.None;
                                    Regex regex2 = new Regex("[ ]{2,}", options2);
                                    str2 = regex2.Replace(str2, " ");
                                    str2 = str2.Trim();
                                    newProd2.Rows[i]["description_short"] = str2;
                                }
                                else
                                {
                                    newProd2.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG";
                                }
                            }
                        }
                    }
                    catch (Exception Err) { }

                    HomePageProductList2 homePageProductList2 = new HomePageProductList2();
                    homePageProductList2.title = "Featured Products";
                    homePageProductList2.FeaturedProducts = newProd2;
                    homePageProductList2.url = "";

                    BestOffers1 bestOffers1 = new BestOffers1();
                    bestOffers1.title = "BestOffers1";
                    bestOffers1.bestOffers1 = BestOffers1;
                    bestOffers1.url = "";

                    BestOffers2 bestOffers2 = new BestOffers2();
                    bestOffers2.title = "BestOffers2";
                    bestOffers2.bestOffers2 = BestOffers2;
                    bestOffers2.url = "";

                    BestOffers3 bestOffers3 = new BestOffers3();
                    bestOffers3.title = "bestOffers3";
                    bestOffers3.bestOffers3 = BestOffers3;
                    bestOffers3.url = "";

                    Homepageresult Homepageresult = new Homepageresult();
                    Homepageresult.activeServices = activeServices;
                    Homepageresult.banners=Banners;
                    Homepageresult.fullBanners = FullBanners;
                    Homepageresult.categories = Categories;
                    //Homepageresult.shopByCategory = shopbyCategories;
                    Homepageresult.shopByCategory = orrd;
                    Homepageresult.productlist1 = homePageProductList1;
                    Homepageresult.productlist2 = homePageProductList2;
                    Homepageresult.bestOffers1 = bestOffers1;
                    Homepageresult.bestOffers2 = bestOffers2;
                    Homepageresult.bestOffers3 = bestOffers3;

                    ResponseObj.result = Homepageresult;
                 }
                 else
                 {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;

                 }
               
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage Banners()
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                DataTable Banners = HomePageComp.GetHomepageBanners();

                if (Banners.Rows.Count > 0)
                {
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = Banners;

                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;

                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage terms_conditions()
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                ResponseObj.result = "TERMS AND CONDITIONS";
                ResponseObj.status = 1;
                ResponseObj.message = "Terms and conditions may apply in order for you to avail specific " +
                    "Pay2Cart Services and to specific portions or features of the Pay2Cart Platform, including contests, " +
                    "promotions or other similar features, all of which terms are made a part of these T&Cs by this reference." +
                    " You agree to abide by such other terms and conditions, including where applicable representing that you are of " +
                    "sufficient legal age to use or participate in such service or feature. If there is a conflict between these T&Cs " +
                    "and the terms posted for or applicable to a specific portion of the Pay2Cart Platform or for any Pay2Cart Service " +
                    "offered on or through the Pay2Cart Platform, the latter terms shall control with respect to your use of that portion " +
                    "of the Pay2Cart Platform or the specific Pay2Cart Service.Pay2Cart may make changes to any Pay2Cart Services offered " +
                    "on the Pay2Cart Platform, or to the applicable terms for any such Pay2Cart Services, at any time, without notice.";

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage checkDeliverAvailableByZipcode([FromBody]CheckDelivey checkDelivey)
        {
            DeliveryResponseObj ResponseObj = new DeliveryResponseObj();
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                    DataTable ProductDelivery = HomePageComp.CheckProductDelivery(checkDelivey.zipcode, checkDelivey.id_product);

                    if (ProductDelivery.Rows.Count > 0)
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = "Delivered in "  + ProductDelivery.Rows[0]["DeliveryDays"]; ;
                        
                    }
                    else 
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = "Delivered in  7-10 working days"; 

                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.OopsSomethingWentWrong;
                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }


        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetStates()
        {
            GetStatesResponse ResponseObj = new GetStatesResponse();
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                    int RowsPerPage = 100; // fixed items to display in a page
                    int PageNumber = 0 ; // Page Number
                    DataTable statesAvail = HomePageComp.GetStates(RowsPerPage,PageNumber);

                    if (statesAvail.Rows.Count > 0)
                    {
                        AllStates  states = new AllStates();
                        states.states = statesAvail;
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = states;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.OopsSomethingWentWrong;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "Zipcode is required";
                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage shippingCarriers(ShippingCarrier shippingCarriers)
        {
            ShippingCarrierResponse ResponseObj = new ShippingCarrierResponse();
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                   
                    DataTable shippingcarriers = HomePageComp.ShippingCarrier(shippingCarriers.id_customer);
                   
                    if (shippingcarriers.Rows.Count > 0)
                    {
                        for(int i=0;i<= shippingcarriers.Rows.Count-1;i++)
                        {
                            if(shippingcarriers.Rows[i]["price_tax_exc"] != null )
                            {
                               shippingcarriers.Rows[i]["price_tax_exc"]=0;
                                shippingcarriers.Rows[i]["delay"] = 0;
                            }
                            else
                            {
                                shippingcarriers.Rows[i]["price_tax_exc"] = 0;
                                shippingcarriers.Rows[i]["delay"] = 0;
                            }
                        }
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.carriers = shippingcarriers;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "Delivery Address Required.. ";
                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage ContactUs(Homecontactus homecontactus)
        {
            ContactusResponse ResponseObj = new ContactusResponse();
            string response = "";
            try
            {
                if (ModelState.IsValid)
                {
                    DataTable emailData = PasswordComp.GetEmaillerData();
                    //DataTable shippingcarriers = HomePageComp.ShippingCarrier(shippingCarriers.id_customer);
                    DataTable StoreInfo = PasswordComp.StoreInformation();
                    DataTable userDetails = new DataTable();
                    string str = "";
                    if (emailData.Rows.Count > 0)
                    {
                        if(homecontactus.id_customer != "" && homecontactus.id_customer != null )
                        {
                            try
                            {
                                userDetails = CustomerComp.getCustomerDetails(int.Parse(homecontactus.id_customer));
                                str = "Contact request," +
                               "" + homecontactus.name + " has sent a request." +
                               "Email:" + userDetails.Rows[0]["Email"].ToString() + "   " +
                               "" + homecontactus.message + "";
                                SendEmail(emailData, StoreInfo, str);
                            }
                            catch (Exception Err) { }
                        }
                        else
                        {
                            str = "Contact request," +
                           "" + homecontactus.name + " has sent a request." +
                           "Email:" + homecontactus.from + "   " +
                           "" + homecontactus.message + "";
                            SendEmail(emailData, StoreInfo, str);
                        }


                        ResponseObj.status = 1;
                        ResponseObj.message = "Your enquiry has been sent successfully.";
                       
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.OopsSomethingWentWrong;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message =  Common.allFieldsRequired;
                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }


        private static long SendEmail(DataTable emailer,DataTable store, string msg)
        {
            long res = 0;
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(emailer.Rows[0]["Host"].ToString());

                //mail.From = new MailAddress(emailer.Rows[0]["Email"].ToString());
                //mail.To.Add(email);


                mail.From= new MailAddress(emailer.Rows[0]["Username"].ToString());
                mail.To.Add(emailer.Rows[0]["Email"].ToString());


                mail.Subject = ""+ store.Rows[0]["Name"].ToString() +". Contact us ";
                mail.Body = msg;

                SmtpServer.Port = (Int32)emailer.Rows[0]["Port"];
                SmtpServer.Credentials = new System.Net.NetworkCredential(emailer.Rows[0]["Username"].ToString(), emailer.Rows[0]["Password"].ToString());
                SmtpServer.EnableSsl = (bool)emailer.Rows[0]["EnableSsl"];

                SmtpServer.Send(mail);
                res = 1;
            }

            catch (Exception ex)
            {

            }
            return res;
        }



    }
}
