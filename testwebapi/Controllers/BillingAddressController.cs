﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DBLibrary;
using System.Web;
using System.Text;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class BillingAddressController : ApiController
    {
        //Customer Billing Address
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetCustomerBillingAddress(int id_customer, int start=0)
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                if (id_customer > 0 )
                {
                    int RowsPerPage = 10; // fixed items to display in a page
                    int PageNumber = (start); // Page Number
                    DataTable BillingAddressDataTable = BillingAddressComp.GetCustomerBillingAddressByID(id_customer, RowsPerPage, PageNumber);
                    if (BillingAddressDataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i <= BillingAddressDataTable.Rows.Count - 1; i++)
                        {
                            object address1 = BillingAddressDataTable.Rows[i]["address2"];
                            if (address1 == DBNull.Value)
                            {
                                BillingAddressDataTable.Rows[i]["address2"] = "";
                            }
                            object address2 = BillingAddressDataTable.Rows[i]["address2"];
                            if (address2 == DBNull.Value)
                            {
                                BillingAddressDataTable.Rows[i]["address2"] = "";
                            }

                            
                        }


                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = BillingAddressDataTable;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                    }



                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CustomerId is required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        //Add Customer Billing/Shipping Address
        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddCustomerAddress([FromBody][Required]AddCustomerAddress addCustomerAddress)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (addCustomerAddress != null)
                {
                    if (ModelState.IsValid)
                    {
                        string  lastname = "";
                        long res = BillingAddressComp.AddCustomerAddress(addCustomerAddress.firstname, lastname,  addCustomerAddress.id_country,
                            addCustomerAddress.id_state, addCustomerAddress.city, addCustomerAddress.address1, addCustomerAddress.address2, addCustomerAddress.postcode, addCustomerAddress.phone_mobile,
                             addCustomerAddress.id_customer);
                        if (res == 2)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Address added Successfylly.";
                        }
                        else
                        {
                            ResponseObj.message =Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
                else
                {
                    ResponseObj.message = Common.OopsSomethingWentWrong;
                    ResponseObj.status = 0;
                    ResponseObj.result = "Please provide input parameters";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        //update customer billing address
        [HttpPost]
        [Authorize]
        public HttpResponseMessage UpdateCustomerBillingAddress([FromBody]UpdateCustomerBillAddress updateCustomerBillAddress)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (updateCustomerBillAddress != null)
                {
                    if (ModelState.IsValid)
                    {
                        string lastname = "";
                        long res = BillingAddressComp.UpdateCustomerBillAddress(updateCustomerBillAddress.firstname, lastname, updateCustomerBillAddress.id_country,
                            updateCustomerBillAddress.id_state, updateCustomerBillAddress.city, updateCustomerBillAddress.address1, updateCustomerBillAddress.address2, updateCustomerBillAddress.postcode, updateCustomerBillAddress.phone_mobile,
                             updateCustomerBillAddress.id_customer, updateCustomerBillAddress.addressId);
                        if (res == 1)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Customer Address Updated Successfylly.";
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }

                } 
                else
                {
                    ResponseObj.message = Common.OopsSomethingWentWrong;
                    ResponseObj.status = 0;
                }
            }

            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        //Delete Billing Address
        [HttpPost]
        [Authorize]
        public HttpResponseMessage DeleteCustomerBillingAddress([FromBody]CustomerBillAddressDelete customerBillAddressDelete)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (customerBillAddressDelete != null)
                {
                    if (ModelState.IsValid)
                    {

                        DataTable getaddressofcustomer = OrdersComp.getcustomeraddress(customerBillAddressDelete.id_customer, customerBillAddressDelete.id_address);
                        if (getaddressofcustomer.Rows.Count > 0)
                        {
                            long res = BillingAddressComp.DeleteCustomerBillingAddress(customerBillAddressDelete.id_customer, customerBillAddressDelete.id_address);
                            if (res == 1)
                            {
                                ResponseObj.status = 1;
                                ResponseObj.message = "Address Deleted Successfylly.";
                            }
                            else
                            {
                                ResponseObj.message = Common.OopsSomethingWentWrong;
                                ResponseObj.status = 0;
                            }
                        }
                        else
                        {
                            ResponseObj.message = "Customer address not matched";
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

    }
}
