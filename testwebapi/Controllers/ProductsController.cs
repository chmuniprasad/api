﻿using CompLibrary;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Pay2CartAPI.Response;
using Pay2CartAPI.Classes;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Helpers;
using System.Collections;
using Pay2CartAPI.Models;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Pay2CartAPI.Controllers
{
    public class ProductsController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getProducts(string categoryID = "", string searchText="", string orderby="", string orderway="",int start=0)
        {
            DataTable products = new DataTable();
            DataTable orrd = new DataTable();
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                
                if ((categoryID != "" && categoryID != "0") || (searchText != "" && searchText != null))
                {
                    if (orderby == "")
                    {
                        orderby = null;
                    }
                    if (orderway == "")
                    {
                        orderway = null;
                    }
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start; // Page Number
                    products = ProductsComp.getProducts(categoryID, searchText, orderby, orderway, ItemsPerPage, PageNumber);
                    products.Columns.Add("out_of_stock",typeof(System.Int32));
                    products.Columns.Add("quantity_discount", typeof(System.Int32));
                    products.Columns.Add("suppliername");
                    products.Columns.Add("discount_percentage", typeof(System.Int32));

                    orrd = products.Clone();
                    orrd.Columns["image"].DataType = typeof(System.String);
                    foreach (DataRow row in products.Rows)
                    {
                        orrd.ImportRow(row);
                    }

                    if (orrd.Rows.Count > 0)
                    {
                        for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                        {
                            DataTable storedetails = PasswordComp.StoreInformation();
                            DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)products.Rows[i]["image"]);
                            string mimetype = "";
                            
                            var imgurl = storedetails.Rows[0]["Url"].ToString();
                            if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                            {
                                mimetype = ".jpg";
                            }
                            else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                            {
                                mimetype = ".png";
                            }
                            string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                            string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();
                            orrd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";
                            //displaying discount percentage
                            try
                            {
                                if ((decimal)orrd.Rows[i]["original_cost"] != 0)
                                {
                                    int percentComplete = 0;
                                    percentComplete = (Int32)Math.Round((decimal)(100 * ((decimal)orrd.Rows[i]["original_cost"] - (decimal)orrd.Rows[i]["orderprice"])) / (decimal)orrd.Rows[i]["original_cost"]);
                                    orrd.Rows[i]["discount_percentage"] = percentComplete;
                                }
                                else
                                {
                                    orrd.Rows[i]["discount_percentage"] = 0;
                                }
                            }
                           catch (Exception Err) { orrd.Rows[i]["discount_percentage"] = 0; }
                            
                            //stock checking
                            if ((Int32)orrd.Rows[i]["quantity"] > 0)
                            {
                                orrd.Rows[i]["out_of_stock"] = 0;
                                orrd.Rows[i]["quantity_discount"] = 0;
                            }
                            else
                            {
                                orrd.Rows[i]["out_of_stock"] = 1;
                                orrd.Rows[i]["quantity_discount"] = 0;
                            }
                            orrd.Rows[i]["suppliername"] = "";
                            //replacing html strings
                            string str = orrd.Rows[i]["description"].ToString();
                            Regex rx = new Regex("<[^>]*>");
                            // replace all matches with empty strin
                            str = rx.Replace(str, "");
                            RegexOptions options = RegexOptions.None;
                            Regex regex = new Regex("[ ]{2,}", options);
                            str = regex.Replace(str, " ");
                            str = str.Trim();
                            orrd.Rows[i]["description"] = str;
                        }

                    }
                    if (orrd.Rows.Count > 0)
                    {
                       
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = orrd;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                        ResponseObj.result = products;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CategoryID or searchText Required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
            

        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage getProductDetails(string  id_product,int id_customer=0)
        {
            DataTable products = new DataTable();
            DataTable sameProuducts = new DataTable();
            ResponseObj ResponseObj = new ResponseObj();
            productsInfo productsInfo = new productsInfo();
            ProductDetailsResobj productDetailsResobj = new ProductDetailsResobj();
            productDetails productsde = new productDetails();
            productsfromsameCategory productsfromsameCategory = new productsfromsameCategory();
            string response = "";
            try
            {
                if (id_product != "" && id_product != "0")
                {
                    products = ProductsComp.getProductDetailsbyCustomer(int.Parse(id_product), id_customer);
                    if(id_customer != 0)
                    {
                        DataTable isavailbleincart = ProductsComp.getProductfromcart(int.Parse(id_product), id_customer);
                        if (isavailbleincart.Rows.Count > 0)
                        {
                            productsInfo.isExistedInCart = 1;
                            productsInfo.quantityExistsInCart = (Int32)isavailbleincart.Rows[0]["Quantity"];
                        }
                        else
                        {
                            productsInfo.isExistedInCart = 0;
                            productsInfo.quantityExistsInCart = 0;
                        }
                    }
                    else
                    {
                        productsInfo.isExistedInCart = 0;
                        productsInfo.quantityExistsInCart = 0;
                    }

                    DataTable productRatings = ProductsComp.ProductRatings(int.Parse(id_product));
                    DataTable productimages = ProductsComp.getProductimages(int.Parse(id_product));
                    DataTable storedetails=   PasswordComp.StoreInformation();
                    var imgurl = storedetails.Rows[0]["Url"].ToString();
                    string[] images = new string[productimages.Rows.Count];
                    string imgName = "";
                    string prodName = productimages.Rows[0][1].ToString();
                    string imgId = "";
                    for (int i = 0; i < productimages.Rows.Count; i++)
                    {
                        int ImageId = 0;
                        ImageId =(Int32)productimages.Rows[i][0];
                        DataTable getImagesLinks = ProductsComp.GetImageFullNames(ImageId);
                        string mimetype = "";
                        if((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                        {
                            mimetype = ".jpg";
                        }
                        else if((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                        {
                            mimetype = ".png";
                        }
                         imgId = getImagesLinks.Rows[0]["Id"].ToString();
                        imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();

                        images[i] = imgurl+ "/media/image/"+ imgId +"/"+ imgName + mimetype +"";
                    }

                    try
                    {
                        //DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)products.Rows[i]["image"]);
                        //DataTable GetSameImages = ProductsComp.GetSameImageNames(imgName);
                        DataTable GetSameImages = ProductsComp.GetSameImageNames(prodName, imgName);
                        if (GetSameImages.Rows.Count > 0)
                        {
                            for (int j = 0; j <= GetSameImages.Rows.Count - 1; j++)
                            {
                                if ((Int32)GetSameImages.Rows[j]["Id"] == int.Parse(imgId))
                                {
                                    if (j == 0)
                                    {
                                        imgName = GetSameImages.Rows[j]["SeoFilename"].ToString();
                                    }
                                    else
                                    {
                                        int ee = j+1 ;
                                        imgName = GetSameImages.Rows[j]["SeoFilename"].ToString() + "-" + ee;
                                    }
                                }
                                else
                                {
                                    imgName = GetSameImages.Rows[j]["SeoFilename"].ToString();

                                }
                            }
                        }
                    }
                    catch (Exception Eee) { }

                    products.Rows[0]["link"] = imgurl + "" + imgName;
                    //// string imgName = "";


                    if (products.Rows.Count>0)
                    {
                        productsInfo.id_product = (Int32)products.Rows[0]["id_product"];
                        productsInfo.id_category_default = (Int32)products.Rows[0]["id_category_default"];
                        if( products.Rows[0]["delivery_available_in"].ToString()=="")
                        {
                            productsInfo.delivery_available_in = 0;
                        }
                        else
                        {
                            productsInfo.delivery_available_in = (Int32)products.Rows[0]["delivery_available_in"];
                        }

                        


                        productsInfo.category = products.Rows[0]["category"].ToString();
                        // productsInfo.quantity = (Int32)products.Rows[0]["quantity"];
                        productsInfo.quantity = (Int32)products.Rows[0]["StockQuantity"];
                        productsInfo.minimal_quantity = (Int32)products.Rows[0]["minimal_quantity"];
                        productsInfo.price = (decimal)products.Rows[0]["price"];
                        productsInfo.price_tax_exc = (decimal)products.Rows[0]["price_tax_exc"];
                        productsInfo.price_without_reduction = (decimal)products.Rows[0]["price_without_reduction"];
                        productsInfo.out_of_stock = (Int32)products.Rows[0]["out_of_stock"];
                        productsInfo.quantity_discount = (Int32)products.Rows[0]["quantity_discount"];
                        productsInfo.condition = products.Rows[0]["condition"].ToString();


                        string str= products.Rows[0]["description"].ToString();
                        System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("<[^>]*>");
                        // replace all matches with empty strin
                        str = rx.Replace(str, "");
                        RegexOptions options = RegexOptions.None;
                        Regex regex = new Regex("[ ]{2,}", options);
                        str = regex.Replace(str, " ");
                        str = str.Trim();
                       
                        productsInfo.description = str;// products.Rows[0]["description"].ToString();
                        productsInfo.description_short = products.Rows[0]["description_short"].ToString();
                        productsInfo.name = products.Rows[0]["name"].ToString();
                        productsInfo.link_rewrite = products.Rows[0]["link_rewrite"].ToString();
                        productsInfo.link = products.Rows[0]["link"].ToString();
                        productsInfo.id_image = (Int32)products.Rows[0]["id_image"];
                        productsInfo.image = images;
                        productsInfo.original_cost= (decimal)products.Rows[0]["original_cost"];
                        try
                        {
                            if ((decimal)products.Rows[0]["original_cost"] != 0)
                            {
                                int percentComplete = 0;
                                percentComplete = (Int32)Math.Round((decimal)(100 * ((decimal)products.Rows[0]["original_cost"] - (decimal)products.Rows[0]["price"])) / (decimal)products.Rows[0]["original_cost"]);
                                productsInfo.discount_percentage = percentComplete;
                            }
                            else
                            {
                                productsInfo.discount_percentage = 0;
                            }
                        }
                        catch (Exception Err) { productsInfo.discount_percentage = 0; }

                        if (productRatings.Rows.Count>0)
                        {
                            if (productRatings.Rows[0][0].ToString() == "")
                            {
                                productsde.totalRatingOfProduct = 0;
                            }
                            else
                            {
                                productsde.totalRatingOfProduct = (decimal)productRatings.Rows[0][0];
                            }
                           
                            productsde.totalReviews= (Int32)productRatings.Rows[0][1];
                        }
                        else
                        {
                            productsde.totalRatingOfProduct = 0;
                            productsde.totalReviews = 0;
                        }
                       
                        
                        productsfromsameCategory.Title = "Products from same category";
                        
                        productsde.ProductDetails = productsInfo;
                        //productsde.image = productimages;
                        productDetailsResobj.status = 1;
                        productDetailsResobj.message = Common.RecordsExists;
                        productDetailsResobj.result = productsde;

                        sameProuducts = ProductsComp.GetproductsSameCategory((Int32)products.Rows[0]["id_category_default"]);
                        sameProuducts.Columns.Add("suppliername");
                        sameProuducts.Columns.Add("discount_percentage",typeof(System.Int32));
                        DataTable orrd = new DataTable();
                        try
                        {
                            orrd = sameProuducts.Clone();
                            orrd.Columns["image"].DataType = typeof(System.String);
                            foreach (DataRow row in sameProuducts.Rows)
                            {
                                orrd.ImportRow(row);
                            }

                            if (orrd.Rows.Count > 0)
                            {
                                for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                                {
                                    DataTable storedetail = PasswordComp.StoreInformation();
                                    try
                                    {
                                        DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)sameProuducts.Rows[i]["image"]);
                                        string mimetype = "";

                                        var imgurl2 = storedetail.Rows[0]["Url"].ToString();
                                        if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                                        {
                                            mimetype = ".jpg";
                                        }
                                        else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                                        {
                                            mimetype = ".png";
                                        }
                                        string imgId2 = getImagesLinks.Rows[0]["Id"].ToString();
                                        string imgName2 = getImagesLinks.Rows[0]["SeoFilename"].ToString();

                                        orrd.Rows[i]["image"] = imgurl2 + "/media/image/" + imgId2 + "/" + imgName2 + mimetype + "";

                                        string str2 = orrd.Rows[i]["description"].ToString();
                                        System.Text.RegularExpressions.Regex rx2 = new System.Text.RegularExpressions.Regex("<[^>]*>");
                                        // replace all matches with empty strin
                                        str2 = rx2.Replace(str2, "");
                                        RegexOptions options2 = RegexOptions.None;
                                        Regex regex2 = new Regex("[ ]{2,}", options2);
                                        str2 = regex2.Replace(str2, " ");
                                        str2 = str2.Trim();
                                        orrd.Rows[i]["description"] = str2;
                                    } catch (Exception Err) { orrd.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG";  }
                                    
                                    try
                                    {
                                        if ((decimal)orrd.Rows[i]["original_cost"] != 0)
                                        {
                                            int percentComplete = 0;
                                            percentComplete = (Int32)Math.Round((decimal)(100 * ((decimal)orrd.Rows[i]["original_cost"] - (decimal)orrd.Rows[i]["price"])) / (decimal)orrd.Rows[i]["original_cost"]);
                                            orrd.Rows[i]["discount_percentage"] = percentComplete;
                                        }
                                        else
                                        {
                                            orrd.Rows[i]["discount_percentage"] = 0;
                                        }
                                    }
                                    catch (Exception Err) { orrd.Rows[i]["discount_percentage"] = 0; }
                                   
                                }
                            }
                        }
                        catch (Exception Err) { }



                        productsfromsameCategory.productsFromSameCategory = orrd;
                        productDetailsResobj.ProductsFromSameCategory = productsfromsameCategory;



                    }
                   
                    else
                    {
                        //ResponseObj.status = 0;
                        //ResponseObj.message = Common.NoRecordsExists;
                        //ResponseObj.result = products;
                        productDetailsResobj.status = 0;
                        productDetailsResobj.message = Common.NoRecordsExists;
                        productDetailsResobj.result = productsde;
                    }
                }
                else
                {
                    //ResponseObj.status = 0;
                    productDetailsResobj.status = 0;
                    productDetailsResobj.message = "productID Required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(productDetailsResobj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        
        public HttpResponseMessage subCategories(int id_category, int start=0)
        {
            DataTable products = new DataTable();
            DataTable orrd = new DataTable();
            ProductsSubCategoryResponseObj ResponseObj = new ProductsSubCategoryResponseObj();
            string response = "";
            try
            {
                if (id_category != 0 )
                {
                    int PageNumber = start ; // Page Number
                    int ItemsPerPage = 10; // fixed items to display in a page
                    products = ProductsComp.getProductsbyCategory(id_category,ItemsPerPage, PageNumber);
                   
                    orrd = products.Clone();
                    orrd.Columns["image"].DataType = typeof(System.String);
                    foreach (DataRow row in products.Rows)
                    {
                        orrd.ImportRow(row);
                    }

                    if (orrd.Rows.Count>0)
                    {
                        try
                        {
                            for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                            {
                                DataTable storedetails = PasswordComp.StoreInformation();

                                if (products.Rows[i]["image"] != null)
                                {
                                    try
                                    {
                                        DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)products.Rows[i]["image"]);
                                        string mimetype = "";
                                        int ImageId = 0;
                                        var imgurl = storedetails.Rows[0]["Url"].ToString();
                                        if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                                        {
                                            mimetype = ".jpg";
                                        }
                                        else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                                        {
                                            mimetype = ".png";
                                        }
                                        string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                                        string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();
                                        orrd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";
                                    }
                                    catch (Exception Err) { products.Rows[i]["image"]= "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG"; }
                                }
                                else
                                {
                                    products.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG";
                                }
                            }
                        }

                        catch (Exception Err) { }
                    }

                    if (products.Rows.Count > 0)
                    {
                        SubCategory subCategory = new SubCategory();
                        subCategory.subcategories = orrd;
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = subCategory;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                      
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "productID Required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }
        
        [HttpPost]
        [Authorize]
        public HttpResponseMessage GetProductStock([FromBody][Required]productStock productStock)
        {
            DataTable products = new DataTable();
            ProductStockResponseObj ResponseObj = new ProductStockResponseObj();
            string response = "";
            try
            {
                products = ProductsComp.GetProductStockDetails(productStock.id_customer);
                if (products.Rows.Count > 0)
                {
                    for (int i = 0; i <= products.Rows.Count - 1; i++)
                    {
                        if((Int32)products.Rows[i]["OrderMinimumQuantity"] <= (Int32)products.Rows[i]["Quantity"])
                        {
                            if ((Int32)products.Rows[i]["Quantity"] > (Int32)products.Rows[i]["StockQuantity"])
                            {
                                ResponseObj.status = 0;
                                ResponseObj.message = "Product " + products.Rows[i]["Name"].ToString() + " out of Stock.";
                                break;
                            }
                            //else
                            //{
                            //    ResponseObj.status = 1;
                            //    ResponseObj.message = "Products stock available";
                            //}
                        }
                        else
                        {
                            ResponseObj.status = 0;
                            ResponseObj.message = "Product  " + products.Rows[i]["Name"].ToString() +" minimum order Quantity is: "+ (Int32)products.Rows[i]["OrderMinimumQuantity"];
                            break;
                        }
                        

                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "Please Enter valid Customer Id ";
                }




            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage getAllProducts(string display,int start)
        {
            DataTable products = new DataTable();
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                display.ToLower();
                if (display == "full" && start >= 1)
                {
                   
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start - 1; // Page Number
                    products = ProductsComp.GetAllProducts(display, ItemsPerPage, PageNumber );

                    if (products.Rows.Count > 0)
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = products;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                        ResponseObj.result = products;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "display type is Required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage productsBySubcategory(int id, string orderby="",string orderway="", int start=0)
        {
            DataTable products = new DataTable();
            DataTable orrd = new DataTable();
            ProductBysubCategoryResponseObj ResponseObj = new ProductBysubCategoryResponseObj();
            string response = "";
            try
            {

                if ((orderby == "" || orderby == null))
                {
                    orderby = "name"; 
                }
                if(orderway == "" || orderway == null)
                {
                    orderway = "asc";
                }
                int ItemsPerPage = 10; // fixed items to display in a page
                int PageNumber = start; // Page Number
                products = ProductsComp.GetProductsBysubCategory(id.ToString(), orderby, orderway, ItemsPerPage, PageNumber);
                if (products.Rows.Count > 0)
                {
                    for (int i = 0; i <= products.Rows.Count - 1; i++)
                    {
                        if((Int32)(products.Rows[i]["quantity"])>0)
                        {
                            (products.Rows[i]["out_of_stock"]) = 0;
                        }
                        else
                        {
                            (products.Rows[i]["out_of_stock"]) = 1;
                        }
                    }
                    orrd = products.Clone();
                    orrd.Columns["image"].DataType = typeof(System.String);
                    orrd.Columns .Add("discount_percentage", typeof(System.Int32));
                    foreach (DataRow row in products.Rows)
                    {
                        orrd.ImportRow(row);
                    }

                   


                    if (orrd.Rows.Count > 0)
                    {
                        for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                        {
                            DataTable storedetails = PasswordComp.StoreInformation();
                            try
                            {
                                DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)products.Rows[i]["image"]);
                                string mimetype = "";

                                var imgurl = storedetails.Rows[0]["Url"].ToString();
                                if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                                {
                                    mimetype = ".jpg";
                                }
                                else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                                {
                                    mimetype = ".png";
                                }
                                string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                                string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();
                                orrd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";

                            }
                            catch (Exception Err) { orrd.Rows[i]["image"] = "http://services.pay2cart.com/Uploads/NoImage/NoImageAvailable.JPG"; }
                            
                            if ((decimal)orrd.Rows[i]["original_cost"] != 0)
                            {
                                int percentComplete = (Int32)Math.Round((decimal)(100 * ((decimal)orrd.Rows[i]["original_cost"] - (decimal)orrd.Rows[i]["orderprice"])) / (decimal)orrd.Rows[i]["original_cost"]);
                               orrd.Rows[i]["discount_percentage"] = percentComplete;
                            }
                            else
                            {
                                orrd.Rows[i]["discount_percentage"] = 0;
                            }
                            // replace all html strings with empty string
                            string str = orrd.Rows[i]["description"].ToString();
                            Regex rx = new Regex("<[^>]*>");
                            str = rx.Replace(str, "");
                            RegexOptions options = RegexOptions.None;
                            // replace all excess spaces  in string
                            Regex regex = new Regex("[ ]{2,}", options);
                            str = regex.Replace(str, " ");
                            str = str.Trim();
                            orrd.Rows[i]["description"] = str;
                        }
                    }
                    ProductBysubCategory productBysubCategory = new ProductBysubCategory();
                    productBysubCategory.products = orrd;
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = productBysubCategory;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;
                    
                }


            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage productsSortingList()
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                List<SortingArray> sortlist = new List<SortingArray>();

                sortlist.Add(new SortingArray()
                {
                    value = "price:asc",
                    name = "Price: Lowest first"
                });
                sortlist.Add(new SortingArray()
                {
                    value = "price:desc",
                    name = "Price: Highest first"
                });
                sortlist.Add(new SortingArray()
                {
                    value = "name:asc",
                    name = "Product Name: A-Z"
                });
                sortlist.Add(new SortingArray()
                {
                    value = "name:desc",
                    name = "Product Name: Z-A"
                });
                sortlist.Add(new SortingArray()
                {
                    value = "quantity:desc",
                    name = "In Stock"
                });
                ResponseObj.message = Common.RecordsExists;
                ResponseObj.status = 1;
                ResponseObj.result = sortlist;
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }



    }
}
