﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Pay2CartAPI.Response;
using Pay2CartAPI.WalletService;
using Pay2CartAPI.Classes;
using Pay2CartAPI.Models;
using System.Globalization;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace Pay2CartAPI.Controllers
{
    public class WalletController : ApiController
    {
        public string response = "";

        [HttpGet]
        [Authorize]
        public HttpResponseMessage getCustomerBalance(int customer_id)
        {
         
            ResponseObj ResponseObj = new ResponseObj();

            try
            {

                if(customer_id > 0)
                {

                    float balance = Wallet.getCustomerCurrentWalletBalance(customer_id);
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = balance;
                }
                else
                {
                    ResponseObj.message = "customerID is required.";
                }
            }
            catch(Exception Err)
            {
                ResponseObj.message = Common.OopsSomethingWentWrong;
                ResponseObj.result = 0;
            }

            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage addMoneyToWallet([FromBody][Required]addMoneyToWallet addMoneyToWallet)
        {
            ResponseObj ResponseObj = new ResponseObj();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                if (addMoneyToWallet != null)
                {
                    if (ModelState.IsValid)
                    {
                        response = Wallet.createWalletOrder(addMoneyToWallet.customer_id, addMoneyToWallet.amount).ToString();
                        HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
                        return HttpResponseMessage;
                    }
                    else
                    {
                        string msg;
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                        {
                            msg = error.ErrorMessage;
                        }
                        else
                        {
                            msg = Common.allFieldsRequired;
                        }
                        ResponseObj.message = msg;
                    }
                }
                else
                {
                    ResponseObj.message = Common.allFieldsRequired;
                }

            }catch(Exception Err)
            {
                ResponseObj.message = Common.OopsSomethingWentWrong;
            }

            
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage sendMoney([FromBody][Required]SendMoney SendMoney)
        {
            ResponseObj ResponseObj = new ResponseObj();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                if (SendMoney != null)
                {

                    if (ModelState.IsValid)
                    {
                        DataTable ToUserInfo = CustomerComp.getCustomerDetailsByUserName(SendMoney.mobile_number.ToString());
                        DataTable loginUserInfo = CustomerComp.getCustomerDetails(SendMoney.customer_id);

                        if (loginUserInfo.Rows.Count>0)
                        {

                            if(loginUserInfo.Rows[0]["mobile"].ToString() != SendMoney.mobile_number.ToString())
                            {
                                if (ToUserInfo.Rows.Count > 0)
                                {
                                    int toCustomerID = Convert.ToInt32(ToUserInfo.Rows[0]["Id"]);
                                    int orderID = 0;
                                    if(SendMoney.orderID != "" && SendMoney.orderID != "0" && SendMoney.orderID != null)
                                    {
                                        string sub = SendMoney.orderID.Substring(0, 3);
                                        if (sub == "snd")
                                        {
                                            orderID = Convert.ToInt32(SendMoney.orderID.Substring(3));
                                        }
                                    }
                                    response = Wallet.createSendMoneyWalletOrder(SendMoney.customer_id, toCustomerID, SendMoney.amount,orderID);
                                    HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
                                    return HttpResponseMessage;
                                }
                                else
                                {
                                    ResponseObj.message = "User does not exists.";
                                }
                            }
                            else
                            {
                                ResponseObj.message = "You can not transfer amount to own account.";
                            }
                            
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                        }
                            
                    }
                    else
                    {
                        string msg;
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                        {
                            msg = error.ErrorMessage;
                        }
                        else
                        {
                            msg = Common.allFieldsRequired;
                        }
                        ResponseObj.message = msg;
                    }
                }
                else
                {
                    ResponseObj.message = Common.allFieldsRequired;
                }

            }
            catch (Exception Err)
            {
                ResponseObj.message = Common.OopsSomethingWentWrong;
            }


            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage getWalletHistory(int customer_id, int start=0)
        {
            string response = "";
            DataTable WalletHistory = new DataTable();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if(customer_id > 0)
                {
                    int PageNumber = start; // Page Number
                    int RowsPerPage = 10; // fixed items to display in a page
                    WalletHistory = WalletComp.getWalletHistory(customer_id, RowsPerPage, PageNumber );
                   
                    DataTable walletHist = new DataTable();
                    walletHist.Columns.Add("id",typeof(System.Int32));
                    walletHist.Columns.Add("issucess");
                    walletHist.Columns.Add("balanceamount", typeof(System.Decimal));
                    walletHist.Columns.Add("sendtomobile");
                    walletHist.Columns.Add("receiveamount", typeof(System.Decimal));
                    walletHist.Columns.Add("receivedate");
                    walletHist.Columns.Add("walletamount", typeof(System.Decimal));
                    walletHist.Columns.Add("walletdate");
                    walletHist.Columns.Add("sendamount", typeof(System.Decimal));
                    walletHist.Columns.Add("senddate");
                    walletHist.Columns.Add("transferamount", typeof(System.Decimal));
                    walletHist.Columns.Add("transferdate");
                    walletHist.Columns.Add("type");

                    if (WalletHistory.Rows.Count >0)
                    {
                        for(int i=0;i<= WalletHistory.Rows.Count-1;i++)
                        {
                            DateTime walletdate = (DateTime)WalletHistory.Rows[i]["walletdate"];
                            //DateTime utcTime = walletdate.ToLocalTime();
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(walletdate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                            string walletDT = istdate.ToString("yyyy-MM-dd HH:mm:ss");
                            

                            if ((Int32)WalletHistory.Rows[i]["txServiceID"] == 6 )// add money to wallet
                            {
                                walletHist.Rows.Add(WalletHistory.Rows[i]["id"], WalletHistory.Rows[i]["issucess"], WalletHistory.Rows[i]["balanceamount"], null, null, null, WalletHistory.Rows[i]["walletamount"], walletDT, null, null, null, null, "User");
                            }
                            else if((Int32)WalletHistory.Rows[i]["txServiceID"] == 7)
                            {
                                string id = WalletHistory.Rows[i]["id"].ToString();

                                    if((Int32)WalletHistory.Rows[i]["txType"] ==1)// reecive money from another mobile number
                                    {
                                        DataTable ReceiveFrommobile = WalletComp.getreceiveFromMobile(customer_id, int.Parse(id));
                                        walletHist.Rows.Add(WalletHistory.Rows[i]["id"], WalletHistory.Rows[i]["issucess"], WalletHistory.Rows[i]["balanceamount"], ReceiveFrommobile.Rows[0]["mobile"], ReceiveFrommobile.Rows[0]["txAmount"], walletDT, null, null, null, null, null, null, "User");
                                    }
                                    else//send money to another mobile number
                                    {
                                    DataTable sendTOmobile = WalletComp.getSendtoMobile(customer_id, int.Parse(id));

                                    walletHist.Rows.Add(WalletHistory.Rows[i]["id"], WalletHistory.Rows[i]["issucess"], WalletHistory.Rows[i]["balanceamount"], sendTOmobile.Rows[0]["mobile"], null, null, null, null, sendTOmobile.Rows[0]["amount"], walletDT, null, null, "User");
                                    }
                                
                            }
                            else
                            {
                                
                                walletHist.Rows.Add(WalletHistory.Rows[i]["id"], WalletHistory.Rows[i]["issucess"], WalletHistory.Rows[i]["balanceamount"], null, null, null, WalletHistory.Rows[i]["walletamount"], walletDT, null,null,null,null,"User");
                            }

                            
                        }
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = walletHist;
                    }
                    else
                    {
                        ResponseObj.message = Common.NoRecordsExists;
                        ResponseObj.result = walletHist;
                    }
                }
                else
                {
                    ResponseObj.message = "customerID is required.";
                }

            }catch(Exception Err)
            {
                ResponseObj.message =Common.OopsSomethingWentWrong;
            }
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;

        }
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getRechargeHistory(int customer_id, int start=0)
        {
            string response = "";
            DataTable RechargeHistory = new DataTable();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            RechargerHistoryResponseObj ResponseObj = new RechargerHistoryResponseObj();
            try
            {
                if (customer_id > 0)
                {
                    int PageNumber = start; // Page Number
                    int RowsPerPage = 10; // fixed items to display in a page
                    RechargerHistory rechargerHistory = new RechargerHistory();
                    RechargeHistory = WalletComp.getRechargeHistory(customer_id, RowsPerPage, PageNumber );
                    RechargeHistory.Columns.Add("bbCustomerNum");
                    RechargeHistory.Columns.Add("bbamount");
                    DataTable rechOrders = new DataTable();
                    
                    if (RechargeHistory.Rows.Count > 0)
                    {
                        try
                        {
                            rechOrders = RechargeHistory.Clone();
                            rechOrders.Columns["networkName"].DataType = typeof(System.String);
                            foreach (DataRow row in RechargeHistory.Rows)
                            {
                                rechOrders.ImportRow(row);
                            }

                            for (int i = 0; i <= rechOrders.Rows.Count - 1; i++)
                            {
                                string oper = RechargeHistory.Rows[i]["operatorCode"].ToString();
                               // oper = Regex.Replace(oper, " ");
                                oper = oper.Trim();
                                DataTable OperatorName = NetworkComp.getAllNetworks(int.Parse(oper));
                                rechOrders.Rows[i]["networkName"] = OperatorName.Rows[0]["operatorName"].ToString();
                            }
                        }
                        catch (Exception Err) { }
                        
                        ResponseObj.status = 1;
                        ResponseObj.result = rechOrders;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.result = rechOrders;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                }

            }
            catch (Exception Err)
            {
                ResponseObj.status = 0;
            }
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }
    }
}
