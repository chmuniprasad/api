﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System.IO;
using DBLibrary;
using System.Web;
using System.Text;
using Pay2CartAPI.Classes;
using System;

namespace testwebapi.Controllers
{
    public class contactusController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage Contactus()
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                ResponseObj.result = "Contact Us";
                ResponseObj.status = 1;
                ResponseObj.message = "";

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }
    }
}
