﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Pay2CartAPI.Models;
using Pay2CartAPI.Response;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class CategoryController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getAllCategories()
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";

            DataTable categories = CategoryComp.getAllCategories();
            DataTable orrd = new DataTable();
            if (categories.Rows.Count > 0)
            {
                try
                {
                    orrd = categories.Clone();
                    orrd.Columns["image"].DataType = typeof(System.String);
                   
                    foreach (DataRow row in categories.Rows)
                    {
                        orrd.ImportRow(row);
                    }

                    if (orrd.Rows.Count > 0)
                    {
                        for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                        {
                            DataTable storedetail = new DataTable();
                            DataTable getImagesLinks = new DataTable();
                            try
                            {
                                storedetail = PasswordComp.StoreInformation();
                                getImagesLinks = ProductsComp.GetImageFullNames((Int32)categories.Rows[i]["image"]);

                            }
                            catch (Exception Err) { }

                            string mimetype = "";

                            var imgurl = storedetail.Rows[0]["Url"].ToString();
                            if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                            {
                                mimetype = ".jpg";
                            }
                            else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                            {
                                mimetype = ".png";
                            }
                            string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                            string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();


                           
                            orrd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";
                            

                        }
                    }
                }
                catch (Exception Err) { }

                ResponseObj.status = 1;
                ResponseObj.message = Common.RecordsExists;
                ResponseObj.result = orrd;
            }
            else
            {
                ResponseObj.status = 0;
                ResponseObj.message =Common.NoRecordsExists;
            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
            //return Request.CreateResponse(HttpStatusCode.OK, response);


        }
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getSubCategoriesByCategory(int categoryID=0)
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            if (categoryID >0) {
                
                DataTable categories = CategoryComp.getAllCategories(categoryID);
                if (categories.Rows.Count > 0)
                {
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = categories;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;
                }
            }
            else
            {
                ResponseObj.status = 0;
                ResponseObj.message = "CategoryID is required.";
            }

            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }
    }
}
