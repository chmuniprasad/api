﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Pay2CartAPI.Models;
using Pay2CartAPI.Response;
using Pay2CartAPI.Cyberplat;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class GasBillController : ApiController
    {
        public string response = "";

        [HttpPost]
        [Authorize]
        public HttpResponseMessage doGasBillPayment([FromBody][Required]GasBill GasBill)
        {

            ResponseObj ResponseObj = new ResponseObj();
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                if (GasBill != null)
                {
                    if (ModelState.IsValid)
                    {
                        if (GasBill.gas_provider == "62" && (GasBill.billGroupNumber == "" || GasBill.billGroupNumber == null))
                        {
                            ResponseObj.message = "Bill group number is required.";
                        }
                        else
                        {
                            response = CPGasBill.doGasBillPayment(GasBill.userId.ToString(), GasBill.gas_provider.ToString(), GasBill.customerId.ToString(), GasBill.amount.ToString(), GasBill.billGroupNumber.ToString(), GasBill.orderID);
                            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
                            return HttpResponseMessage;
                        }
                    }
                    else
                    {
                        ResponseObj.message = Common.allFieldsRequired;
                    }
                }
                else
                {
                    ResponseObj.message = Common.OopsSomethingWentWrong;
                }

            }
            catch (Exception Err)
            {

            }

            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

    }
}
