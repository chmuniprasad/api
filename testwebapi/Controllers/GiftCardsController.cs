﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pay2CartAPI.Models;
using Pay2CartAPI.Response;
using System.Text;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class GiftCardsController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetGiftCardOrders(int id_customer, int start=0)
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                if (id_customer > 0)
                {
                   
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start ; // Page Number
                    DataTable userDetails = GiftCardComp.GetgiftCardOrderDetails(id_customer, ItemsPerPage, PageNumber);
                    if (userDetails.Rows.Count > 0)
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = userDetails;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CustomerID is required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddGiftCardtoCart([FromBody][Required]GiftCards giftCards)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (giftCards != null)
                {
                    if (ModelState.IsValid)
                    {
                        //string AttributesXml = "";
                        //var AttributesXml = giftCards.AddGiftCardAttribute(giftCards.AttributesXml, giftCards.RecipientName, giftCards.RecipientEmail, giftCards.SenderName, giftCards.SenderEmail, giftCards.Message);
                        var AttributesXml = giftCards.AddGiftCardAttribute(giftCards.RecipientName, giftCards.RecipientEmail, giftCards.SenderName, giftCards.SenderEmail, giftCards.Message);

                        long res = GiftCardComp.AddGiftCardtoCart( giftCards.CustomerId, giftCards.ProductId, AttributesXml, giftCards.Quantity);
                        if (res == 1)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Item Added to Cart Successfully.";
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage DeleteGiftCardfromCart([FromBody][Required]DeleteGiftCardFromCart deleteGiftCardFromCart)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (deleteGiftCardFromCart != null)
                {
                    if (ModelState.IsValid)
                    {

                       long res = GiftCardComp.DeletGiftCard(deleteGiftCardFromCart.CustomerId, deleteGiftCardFromCart.ProductId, deleteGiftCardFromCart.Quantity);


                        if (res == 1)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "GiftCard Deleted from Cart Successfully.";
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                       
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

    }

}
