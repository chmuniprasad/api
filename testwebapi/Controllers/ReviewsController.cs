﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System.IO;
using DBLibrary;
using System.Web;
using System.Text;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class ReviewsController : ApiController
    {

        [HttpPost]
        [Authorize]
        public HttpResponseMessage WriteProductReview([FromBody]WriteProductReview writeProductReview )
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (writeProductReview != null)
                {
                    if (ModelState.IsValid)
                    {
                        if (writeProductReview.id_customer > 0)
                        {
                            DataTable userDetails = ReviewsComp.CustomerRole(writeProductReview.id_customer);
                            if (userDetails.Rows.Count > 0)
                            {
                                int role = (Int32)userDetails.Rows[0][0];
                                if (role >= 4)
                                {
                                    ResponseObj.message = "Only Registerd users Can write Review";
                                    ResponseObj.status = 0;
                                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ResponseObj.message);
                                }
                                else
                                {
                                    int HelpfulYesTotal = 0;
                                    int HelpfulNoTotal = 0;
                                    string hostName = Dns.GetHostName();
                                   // string IpAddress = new WebClient().DownloadString("http://icanhazip.com");//Dns.GetHostByName(hostName).AddressList[0].ToString();
                                    string IpAddress = Common.IPaddress;
                                    bool IsApproved = true;
                                    DateTime CreatedOnUtc = DateTime.Now;
                                    DateTime UpdatedOnUtc = DateTime.Now;
                                    //int ratings = int.Parse(writeProductReview.rating);
                                    decimal  convertDecimal = Convert.ToDecimal(writeProductReview.rating);
                                    decimal ratings2 = decimal.Round(convertDecimal);//  (Int32)convertDecimal;
                                    int ratings = (Int32)ratings2;

                                    long res = ReviewsComp.WriteProductReview(writeProductReview.id_product, writeProductReview.title, writeProductReview.content,ratings, HelpfulYesTotal, HelpfulNoTotal, writeProductReview.id_customer, IpAddress, IsApproved, CreatedOnUtc, UpdatedOnUtc);
                                    if (res == 2)
                                    {
                                        ResponseObj.status = 1;
                                        ResponseObj.message = "Product Review sucessfully added.";
                                    }
                                    else
                                    {
                                        ResponseObj.message = Common.OopsSomethingWentWrong;
                                        ResponseObj.status = 0;
                                    }
                                }
                            }
                            else
                            {
                                ResponseObj.message = "Only Registerd users Can write Review";
                                ResponseObj.status = 0;
                            }
                        }
                        else
                        {
                            ResponseObj.message = "Please provide Valid UserID";
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetProdutReviewsByProduct(int id_product,int id_customer,  int start=0)
        {
            ProductReviewsResponseObj ResponseObj = new ProductReviewsResponseObj();
            string response = "";
            try
            { 
               
                if (id_product > 0 )
                {
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start ; // Page Number
                    DataTable ProductReviews = ReviewsComp.GetProductReviews(id_product, ItemsPerPage, PageNumber);
                    
                    ProductReviews.Columns.Add("firstname");
                    ProductReviews.Columns.Add("lastname");
                    ProductReviews.Columns.Add("fullCustomerName");
                    int prodRevTotYes; int prodRevTotNo;
                    if (ProductReviews.Rows.Count > 0)
                    {
                        for (int j=0;j<= ProductReviews.Rows.Count-1;j++)
                        {
                            DataTable getCusotmerName = ReviewsComp.GetCustomerName((Int32)ProductReviews.Rows[j]["CustomerId"]);
                            DataTable getCustomerCheckStatus = ReviewsComp.ReviewCheckStatus(id_customer, (Int32)ProductReviews.Rows[j]["review_id"]);
                            if(getCustomerCheckStatus.Rows.Count > 0)
                            {
                                ProductReviews.Rows[j]["isChecked"] = 1;
                            }
                            else
                            {
                                ProductReviews.Rows[j]["isChecked"] = 0;
                            }
                            ProductReviews.Rows[j]["isReported"] = 0;
                            prodRevTotYes = (Int32)ProductReviews.Rows[j]["HelpfulYesTotal"];
                            prodRevTotNo = (Int32)ProductReviews.Rows[j]["HelpfulNoTotal"];
                            decimal totreview = decimal.Add(prodRevTotYes, prodRevTotNo);
                            decimal usefl = decimal.Subtract( totreview , prodRevTotNo);
                            int useffl = (Int32)totreview - (Int32)prodRevTotNo;
                            ProductReviews.Rows[j]["text"] = ""+ useffl + " out of "+ totreview + " people found this review useful";
                            string frstname; string lstname;
                            if (getCusotmerName.Rows.Count > 0)
                            {
                                for (int i = 0; i <= getCusotmerName.Rows.Count - 1; i++)
                                {
                                    if (getCusotmerName.Rows[i]["Key"].ToString().Contains("FirstName"))
                                    {
                                        frstname = getCusotmerName.Rows[i]["Value"].ToString();
                                        ProductReviews.Rows[j]["firstname"] = frstname;

                                    }
                                    else if (getCusotmerName.Rows[i]["Key"].ToString().Contains("LastName"))
                                    {
                                        lstname = getCusotmerName.Rows[i]["Value"].ToString();
                                        ProductReviews.Rows[j]["lastname"] = lstname;
                                    }
                                    ProductReviews.Rows[j]["fullCustomerName"] = ProductReviews.Rows[j]["firstname"] + " " + ProductReviews.Rows[j]["lastname"];
                                }
                                
                            }
                        }
                        ProductReviews review = new ProductReviews();
                        review.reviews = ProductReviews;
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = review;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "ProductId is required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;


        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetProdutReviewsByCustomer(int id_customer,int start=0)
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                if (id_customer > 0)
                {
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start ; // Page Number
                    DataTable userDetails = ReviewsComp.GetReviewsbyCustomerID(id_customer, ItemsPerPage, PageNumber);


                    if (userDetails.Rows.Count > 0)
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = userDetails;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CustomerId is required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;

        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage MakeReviewAsUsefulNO([FromBody]ReviewsUsefulNo reviewsUsefulNo)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (reviewsUsefulNo != null)
                {
                    if (ModelState.IsValid)
                    {
                        string IpAddress = "";
                        try
                        {
                            //IpAddress = new WebClient().DownloadString("http://icanhazip.com");
                            IpAddress = Common.IPaddress;
                        }
                        catch (Exception Err) { }
                        bool IsAproved = true;
                        DateTime createdonutc = DateTime.Now;
                        DateTime updatedonutc = DateTime.Now;

                        long res = ReviewsComp.ProductReviewHelpfulNo(reviewsUsefulNo.id_customer, IpAddress, IsAproved, createdonutc, updatedonutc, reviewsUsefulNo.id_product_comment);


                        if (res == 3)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Review Submitted Sucessfully.";
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }

                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
                else
                {
                    ResponseObj.message = Common.OopsSomethingWentWrong;
                    ResponseObj.status = 0;
                    ResponseObj.result = "Please provide valid inputs";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }


        [HttpPost]
        [Authorize]
        public HttpResponseMessage MakeReviewAsUseful([FromBody]ReviewsUsefulYes reviews)
        {
            string response = "";
            ProductReviewResponseObj ResponseObj = new ProductReviewResponseObj();
            try
            {
                if (reviews != null)
                {
                    if (ModelState.IsValid)
                    {

                        string IpAddress = "";
                        try
                        {
                            //IpAddress = new WebClient().DownloadString("http://icanhazip.com");
                            IpAddress = Common.IPaddress;
                        }catch (Exception Err) { }
                        bool IsAproved = true;
                        DateTime createdonutc = DateTime.Now;
                        DateTime updatedonutc = DateTime.Now;
                        bool wasUseful = false;
                        if (reviews.is_useful==1)
                        {
                            wasUseful = true;
                        }
                        productreviews productreviewss = new productreviews();
                        long res = ReviewsComp.ProductReviewHelpfulYes(reviews.id_customer, IpAddress, IsAproved, createdonutc, updatedonutc, reviews.id_product_comment, wasUseful);
                        if (res == 3)
                        {
                            int prodRevTotYes; int prodRevTotNo;
                            DataTable rvDT = ReviewsComp.ProductReviewHelpfulYesNO(reviews.id_product_comment);
                            prodRevTotYes = (Int32)rvDT.Rows[0]["HelpfulYesTotal"];
                            prodRevTotNo = (Int32)rvDT.Rows[0]["HelpfulNoTotal"];

                            //if (wasUseful == true)
                            //{
                                decimal totreview = decimal.Add(prodRevTotYes, prodRevTotNo);
                                string reivewText = "" + prodRevTotYes + " out of " + totreview + " people found this review useful";
                                productreviewss.text = reivewText;
                            //}
                            //else
                            //{
                            //    decimal totreview = decimal.Add(prodRevTotYes, prodRevTotNo);
                            //    int usefl = prodRevTotYes - 1;
                            //    string reivewText = "" + usefl + " out of " + totreview + " people found this review useful";
                            //    productreviewss.text = reivewText;
                            //}
                            
                           
                            productreviewss.isChecked = 1;
                            ResponseObj.message= "Review Submitted Sucessfully.";
                           
                            
                            ResponseObj.status = 1;
                            ResponseObj.result = productreviewss;
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.result = productreviewss;
                            ResponseObj.status = 0;
                        }

                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }
    }
}
