﻿using CompLibrary;
using DBLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pay2CartAPI.Models;
using Pay2CartAPI.Response;
using System.Text;
using Pay2CartAPI.Classes;
using System.Net.Mail;
using Newtonsoft.Json.Linq;

namespace Pay2CartAPI.Controllers
{
    public class PasswordController : ApiController
    {
        [HttpPost]
        [Authorize]
        public HttpResponseMessage ChangePassword([FromBody]ChangePassword changePassword)
        {
            string response = "";
            ChangePasswordResponse ResponseObj = new ChangePasswordResponse();
            try
            {
                if (changePassword != null)
                {
                    if (ModelState.IsValid)
                    {
                        if(changePassword.confirmation == changePassword.passwd)
                        {
                            DataTable customerData = PasswordComp.GetCustomerInformation(changePassword.id_customer);
                            if (customerData.Rows.Count > 0)
                            {
                                if ((Int32)customerData.Rows[0]["CustomerRole_Id"] <= 3)
                                {
                                    String curPasswordSalt = customerData.Rows[0]["PasswordSalt"].ToString();
                                    string newPassword = Encryption.CreatePasswordHash(changePassword.passwd, curPasswordSalt, "SHA1");
                                    string oldPassword = Encryption.CreatePasswordHash(changePassword.currentpasswd, curPasswordSalt, "SHA1");
                                    //checking OLD password match with login  password
                                    if (oldPassword == customerData.Rows[0]["Password"].ToString())
                                    {
                                        //checking NEW password match with login  password
                                        if (newPassword != customerData.Rows[0]["Password"].ToString())
                                        {

                                            long res = PasswordComp.UpdateNewPassword(changePassword.id_customer, changePassword.passwd);
                                            DataTable newpassword = PasswordComp.GetLatestPassword(changePassword.id_customer);
                                            DataTable LoginUser = new DataTable();
                                            LoginUser = CustomerComp.changePassword(customerData.Rows[0]["Username"].ToString(), newpassword.Rows[0][0].ToString());

                                            if (res == 1)
                                            {
                                                DataTable cusotmerDet = CustomerComp.CustomerProfileDetails(changePassword.id_customer);
                                                try
                                                {
                                                    if (cusotmerDet.Rows.Count > 0)
                                                    {
                                                        for (int i = 0; i <= cusotmerDet.Rows.Count - 1; i++)
                                                        {
                                                            if (cusotmerDet.Rows[i]["Key"].ToString() == "Gender")
                                                            {
                                                                if (cusotmerDet.Rows[i]["Value"].ToString() == "M")
                                                                {
                                                                    LoginUser.Rows[0]["id_gender"] = "1";
                                                                }
                                                                else if (cusotmerDet.Rows[i]["Value"].ToString() == "F")
                                                                {
                                                                    LoginUser.Rows[0]["id_gender"] = "2";
                                                                }
                                                                else
                                                                {
                                                                    LoginUser.Rows[0]["id_gender"] = "0";
                                                                }
                                                            }
                                                            else if (cusotmerDet.Rows[i]["Key"].ToString() == "FirstName")
                                                            {
                                                                LoginUser.Rows[0]["firstname"] = cusotmerDet.Rows[i]["Value"].ToString();
                                                            }
                                                            else if (cusotmerDet.Rows[i]["Key"].ToString() == "LastName")
                                                            {
                                                                //customerdetails.lastname = cusotmerDet.Rows[i]["Value"].ToString();
                                                                LoginUser.Rows[0]["lastname"] = cusotmerDet.Rows[i]["Value"].ToString(); //customerProfile.lastname;
                                                            }

                                                            else if (cusotmerDet.Rows[i]["Key"].ToString() == "DateOfBirth")
                                                            {
                                                                LoginUser.Rows[0]["birthday"] = cusotmerDet.Rows[i]["Value"].ToString();
                                                            }

                                                        }


                                                    }
                                                }
                                                catch (Exception Err) { }


                                                ResponseObj.status = 1;
                                                ResponseObj.message = "Password Changed Successfylly.";

                                                string JSONresult;
                                                JSONresult = JsonConvert.SerializeObject(LoginUser);
                                                JSONresult = JSONresult.Replace("[", "");
                                                JSONresult = JSONresult.Replace("]", "");
                                                JObject jsonObject = JObject.Parse(JSONresult);

                                                ResponseObj.customerdetails = jsonObject;
                                            }
                                            else
                                            {
                                                ResponseObj.message = Common.OopsSomethingWentWrong;
                                                ResponseObj.status = 0;
                                            }
                                        }
                                        else
                                        {
                                            ResponseObj.message = "Old and New password should not be same";
                                            ResponseObj.status = 0;
                                        }
                                    }
                                    else
                                    {
                                        ResponseObj.message = "Please provide valid old password.";
                                        ResponseObj.status = 0;
                                    }






                                }
                                else
                                {
                                    ResponseObj.message = "Registered users can only change Password.";
                                    ResponseObj.status = 0;
                                }
                            }
                            else
                            {
                                ResponseObj.message = "Please provide valid Information";
                                ResponseObj.status = 0;
                            }
                        }
                        else
                        {
                            ResponseObj.message = "Password and Confirm Password not match";
                            ResponseObj.status = 0;
                        }
                       
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage ForgotPassword([FromBody]PasswordReset passwordReset)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (passwordReset.email != null)
                {
                    if (ModelState.IsValid)
                    {

                        DataTable customerData = PasswordComp.GetCustomerInformationByEamilid(passwordReset.email);
                        if (customerData.Rows.Count > 0)
                        {
                            if (passwordReset.email == customerData.Rows[0]["Email"].ToString())
                            {
                                DataTable emailData = PasswordComp.GetEmaillerData();
                                DataTable StoreInfo = PasswordComp.StoreInformation();
                                var token = Guid.NewGuid();
                                string passwordResetToken = token.ToString();
                                long result = PasswordComp.InsertPasswordResetToken(passwordResetToken, (Int32)customerData.Rows[0]["Id"]);

                                string str = passwordReset.email;
                                str = str.Replace("@", "%40");
                                string linktext = (StoreInfo.Rows[0]["Url"] + "customer/passwordrecoveryconfirm?token=" + token.ToString() + "&email=" + str + "").ToString();

                                string msgbody = " < H1 >Hello Muniprasad</ H1 > " +
                                    "< P > To reset your password, please click on the following link:" +
                                    " </ P > < p class = text-center m-t-4 m-b-4> " +
                                    "<a class=btn btn-danger href=" + linktext + "> Reset Password</a>" +
                                    "</ P>" +
                                    "<P>" +
                                    "If the link can not be opened, copy the following address and paste it into the address bar of your browser:" +
                                    "</ P>" +
                                    "<a href = " + linktext + " > " + linktext + " </a>" ;
                                

                                long res = SendEmail(StoreInfo, emailData, linktext, passwordReset.email);
                               
                                if (res == 1)
                                {
                                    ResponseObj.status = 1;
                                    ResponseObj.message = "Password Reset Link sent sucessfully.";
                                    ResponseObj.result = linktext;
                                }
                                else
                                {
                                    //ResponseObj.message = Common.OopsSomethingWentWrong;
                                    //ResponseObj.status = 0;
                                    ResponseObj.message = "Password Reset Link sent sucessfully.";
                                    ResponseObj.status = 1;
                                    ResponseObj.result = linktext;
                                }
                            }

                        }
                        else
                        {
                            ResponseObj.message = "User does not registed with the provided email.";
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        private static long SendEmail(DataTable store,DataTable emailer, string link,string email)
        {
            long res = 0;
             try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(emailer.Rows[0]["Host"].ToString());

                mail.From = new MailAddress(emailer.Rows[0]["Email"].ToString());
                mail.To.Add(email);
                mail.Subject = "Pay2Cart.reset Password";
                mail.Body = link;

                mail.IsBodyHtml = true;
                mail.BodyEncoding = System.Text.Encoding.UTF8;

                SmtpServer.Port = (Int32)emailer.Rows[0]["Port"];
                SmtpServer.Credentials = new System.Net.NetworkCredential(emailer.Rows[0]["Username"].ToString(), emailer.Rows[0]["Password"].ToString());
                SmtpServer.EnableSsl = (bool)emailer.Rows[0]["EnableSsl"];

                SmtpServer.Send(mail);

                res = 1;
            }
            
            catch (Exception ex)
            {
                
            }
            return res;
        }

    }
}
