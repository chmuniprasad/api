﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System;
using System.Linq;
using System.Text;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class ShoppingCartController : ApiController
    {

 
        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddToCart([FromBody][Required]AdditemstoCart additemstoCart)
        {
            string response = "";
            AddShopingCartResponseObj ResponseObj = new AddShopingCartResponseObj();
            try
            {
                if (additemstoCart != null)
                {
                    if (ModelState.IsValid)
                    {
                        int productid = additemstoCart.id_product;
                        DataTable MaxQty = ShopingCartComp.GetMaxProductQty(productid);
                        if (additemstoCart.quantity <= (Int32)MaxQty.Rows[0]["StockQuantity"])
                        {
                            int id_customer;
                            if (MaxQty.Rows.Count > 0)
                            {
                                if (!Convert.IsDBNull(MaxQty.Rows[0]["OrderMaximumQuantity"]))
                                {
                                    if (additemstoCart.quantity > (Int32)MaxQty.Rows[0]["OrderMaximumQuantity"])
                                    {
                                        ResponseObj.message = "Max Quantiy Allowed for Purchase is : " + (Int32)MaxQty.Rows[0]["OrderMaximumQuantity"] + ".";
                                        ResponseObj.status = 0;
                                        return Request.CreateResponse(HttpStatusCode.OK, ResponseObj.message);
                                    }
                                }
                                if (!Convert.IsDBNull(MaxQty.Rows[0]["AllowedQuantities"]))
                                {
                                    if (additemstoCart.quantity > (Int32)MaxQty.Rows[0]["AllowedQuantities"])
                                    {
                                        ResponseObj.message = "Allowed Quantiy for Purchase is : " + (Int32)MaxQty.Rows[0]["AllowedQuantities"] + ".";
                                        ResponseObj.status = 0;
                                        return Request.CreateResponse(HttpStatusCode.OK, ResponseObj.message);
                                    }
                                }

                                if (
                                        (additemstoCart.id_customer == "" && additemstoCart.id_guest == null) ||
                                        (additemstoCart.id_customer ==null  && additemstoCart.id_guest == "") || 
                                        (additemstoCart.id_customer == "" && additemstoCart.id_guest == "") || 
                                        (additemstoCart.id_customer == null && additemstoCart.id_guest == null)
                                    )
                                {
                                    //insert guest id
                                    bool systiemid = false;
                                    long guestid = ShopingCartComp.InsertGuestId(systiemid);
                                    id_customer = (Int32)guestid;
                                }
                                else if (additemstoCart.id_customer != "" || (additemstoCart.id_guest == ""|| additemstoCart.id_guest == null))
                                {
                                    id_customer = int.Parse(additemstoCart.id_customer);

                                }
                                else
                                {
                                    id_customer = int.Parse(additemstoCart.id_guest);
                                }

                                if (id_customer != 0)
                                {
                                    //Checking given customerID is is valid or not
                                    DataTable customerIsValid = ShopingCartComp.CheckCustomerIsvalid(id_customer);
                                    if (customerIsValid.Rows.Count == 0)
                                    {
                                        ResponseObj.message = "CustomerId is not valid.. Please provide vaild customerid";
                                        ResponseObj.status = 0;
                                    }
                                    else
                                    {
                                        // checking product had bundle items or not?
                                        DataTable ProdctBudles = ShopingCartComp.CheckProductIncluesBundle(additemstoCart.id_product);
                                        int? ParentItemId = null; int? BundleItemId = null;
                                        if (ProdctBudles.Rows.Count != 0)
                                        {
                                            int cnt = 0;
                                            //// inserting Product Varinat Combination
                                            string AttributesXml = "";
                                            //if (shopingCartItem.ProdutID_variantId != 0)
                                            //{
                                            //    DataTable GetAttibuteXml = ShopingCartComp.GetProdutAttributeXml(shopingCartItem.ProductId, shopingCartItem.ProdutID_variantId);
                                            //    AttributesXml = GetAttibuteXml.Rows[0]["AttributesXml"].ToString();
                                            //}
                                            DataTable checkprocutinCART = ShopingCartComp.AddShopingCartItem(id_customer, additemstoCart.id_product);
                                            if (checkprocutinCART.Rows.Count > 0)
                                            {
                                                long res = ShopingCartComp.UpdateCartItmes(id_customer, additemstoCart.id_product, additemstoCart.quantity);
                                            }
                                            else
                                            {
                                                long res = ShopingCartComp.AddShopingCartItem(id_customer, ParentItemId, BundleItemId, additemstoCart.id_product, AttributesXml, additemstoCart.quantity);
                                            }

                                            int parentitem = 0;
                                            DataTable bundle = ShopingCartComp.GetParentItemID(additemstoCart.id_product, id_customer);
                                            parentitem = (Int32)bundle.Rows[0][0];
                                            for (int i = 0; i <= ProdctBudles.Rows.Count - 1; i++)
                                            {
                                                //// inserting Product Varinat Combination
                                                string AttributesXm = "";
                                                //if (shopingCartItem.ProdutID_variantId != 0)
                                                //{
                                                //    DataTable GetAttibuteXml = ShopingCartComp.GetProdutAttributeXml((Int32)ProdctBudles.Rows[i]["Id"], shopingCartItem.ProdutID_variantId);
                                                //    AttributesXm = GetAttibuteXml.Rows[0]["AttributesXml"].ToString();
                                                //}


                                                long result = ShopingCartComp.AddShopingCartItem(id_customer, parentitem, (Int32)ProdctBudles.Rows[i]["Id"], (Int32)ProdctBudles.Rows[i]["ProductId"], AttributesXm, (Int32)ProdctBudles.Rows[i]["Quantity"]);
                                                cnt += 1;
                                            }
                                            if (cnt == ProdctBudles.Rows.Count)
                                            {
                                                ResponseObj.status = 1;
                                                ResponseObj.message = "Item Added to Cart Successfylly.";
                                            }
                                            else
                                            {
                                                ResponseObj.message = Common.OopsSomethingWentWrong;
                                                ResponseObj.status = 0;
                                            }
                                        }
                                        else
                                        {
                                            //// inserting Product Varinat Combination
                                            //int id_product_variant = int.Parse(additemstoCart.id_product_variant);
                                            //if (additemstoCart.id_product_variant=="")
                                            //{
                                            //   id_product_variant = 0;
                                            //}
                                            //else
                                            //{
                                            //    id_product_variant = int.Parse(additemstoCart.id_product_variant);
                                            //}
                                            string AttributesXml = "";
                                            //if (id_product_variant != 0)
                                            //{
                                            //    DataTable GetAttibuteXml = ShopingCartComp.GetProdutAttributeXml(additemstoCart.id_product, id_product_variant);
                                            //   AttributesXml = GetAttibuteXml.Rows[0]["AttributesXml"].ToString();
                                            //}
                                            DataTable checkprocutinCART = ShopingCartComp.AddShopingCartItem(id_customer, additemstoCart.id_product);
                                            long res = 0;
                                            if (checkprocutinCART.Rows.Count > 0)
                                            {
                                                res = ShopingCartComp.UpdateCartItmes(id_customer, additemstoCart.id_product, additemstoCart.quantity);
                                            }
                                            else
                                            {
                                                res = ShopingCartComp.AddShopingCartItem(id_customer, ParentItemId, BundleItemId, additemstoCart.id_product, AttributesXml, additemstoCart.quantity);
                                            }

                                            DataTable cartTotal = ShopingCartComp.cartTotal(id_customer);
                                            if (res == 1)
                                            {
                                                DataTable productprice = ProductsComp.getProductDetails(additemstoCart.id_product);
                                                ResponseObj.status = 1;
                                                ResponseObj.message = "Item Added to Cart Successfylly.";
                                                ResponseObj.guestid = id_customer;
                                                ResponseObj.cartid = 1;
                                                ResponseObj.quantity = additemstoCart.quantity;
                                                ResponseObj.productPrice = (productprice.Rows[0]["price"]);
                                                // ResponseObj.cartTotalPrice = decimal.Multiply((Int32)(productprice.Rows[0]["price"]), (additemstoCart.quantity));
                                                //decimal price = decimal.Multiply((decimal)(productprice.Rows[0]["price"]), (additemstoCart.quantity));
                                                //ResponseObj.cartTotalPrice = price.ToString();
                                                if (cartTotal.Rows.Count > 0)
                                                {
                                                    decimal sum = decimal.Zero;
                                                    for (int i = 0; i <= cartTotal.Rows.Count - 1; i++)
                                                    {
                                                        sum = sum + (decimal.Multiply((Int32)cartTotal.Rows[i]["Quantity"], (decimal)cartTotal.Rows[i]["Price"]));
                                                    }
                                                    ResponseObj.cartTotalPrice = sum;

                                                }
                                                else
                                                {
                                                    ResponseObj.cartTotalPrice = 0;
                                                }

                                            }
                                            else
                                            {
                                                ResponseObj.message = Common.OopsSomethingWentWrong;
                                                ResponseObj.status = 0;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ResponseObj.message = "CustomerId or GuestId is required";
                                    ResponseObj.status = 0;
                                }
                            }
                            else
                            {
                                ResponseObj.status = 0;
                                ResponseObj.message = "Poduct is not available.";
                            }
                        }
                        else
                        {
                            ResponseObj.status = 0;
                            ResponseObj.message = "Poduct stock not available.";
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                      
                    }
                }
               
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage changeCartQuantity([FromBody]UpdateCartItems updateCartItems)
        {
            string response = "";
            ShopingCartQuantityChange ResponseObj = new ShopingCartQuantityChange();
            try
            {
                if (updateCartItems != null)
                {
                    if (ModelState.IsValid)
                    {
                        if(updateCartItems.quantity>0)
                        {
                            DataTable getproductdetails = ShopingCartComp.getproductdetails(updateCartItems.id_customer, updateCartItems.id_product);
                            if(updateCartItems.quantity >= (Int32)getproductdetails.Rows[0]["OrderMinimumQuantity"])
                            {
                                if (updateCartItems.quantity <= (Int32)getproductdetails.Rows[0]["StockQuantity"])
                                {
                                    long res = ShopingCartComp.UpdateCartItmes(updateCartItems.id_customer, updateCartItems.id_product, updateCartItems.quantity);
                                    DataTable cartTotal = ShopingCartComp.cartTotal(updateCartItems.id_customer);

                                    if (res == 1)
                                    {
                                       
                                        if (cartTotal.Rows.Count > 0)
                                        {
                                            decimal sum = decimal.Zero;
                                            for (int i = 0; i <= cartTotal.Rows.Count - 1; i++)
                                            {
                                                sum = sum + (decimal.Multiply((Int32)cartTotal.Rows[i]["Quantity"], (decimal)cartTotal.Rows[i]["Price"]));
                                            }
                                            ResponseObj.status = 1;
                                            ResponseObj.message = "Cart updated Sucessfully";
                                            ResponseObj.cartTotalPrice = sum;
                                        }
                                        else
                                        {
                                            ResponseObj.status = 1;
                                            ResponseObj.message = "Cart updated sucessfully";
                                            ResponseObj.cartTotalPrice = 0;
                                        }

                                        ResponseObj.status = 1;
                                        ResponseObj.message = "Quantity updated To Cart Sucessfully";
                                        ResponseObj.quantity = updateCartItems.quantity;
                                        ResponseObj.stockAvailable = (Int32)getproductdetails.Rows[0]["StockQuantity"];
                                        ResponseObj.productPrice = (decimal)getproductdetails.Rows[0]["Price"];
                                        ResponseObj.minimalQuantity = (Int32)getproductdetails.Rows[0]["OrderMinimumQuantity"];
                                    }
                                    else
                                    {
                                        ResponseObj.message = Common.OopsSomethingWentWrong;
                                        ResponseObj.status = 0;
                                    }
                                }
                                else
                                {
                                    ResponseObj.message = "Product Stock Not available";
                                    ResponseObj.status = 0;
                                }
                            }
                            else
                            {
                                ResponseObj.message = "For this product minimum order Quantity is: "+ (Int32)getproductdetails.Rows[0]["OrderMinimumQuantity"];
                                ResponseObj.status = 0;
                            }
                           
                        }
                        else
                        {
                            ResponseObj.message = "Quantity should be greater than ZERO";
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

     
        [HttpPost]
        [Authorize]
        public HttpResponseMessage deleteCartItem([FromBody]DeleteCartItems deleteCartItems)
        {
            string response = "";
            ShopingCartDeleteItemResponse ResponseObj = new ShopingCartDeleteItemResponse();
            try
            {
                if (deleteCartItems != null)
                {
                    if (ModelState.IsValid)
                    {
                        long res = ShopingCartComp.DeleteCartItems(deleteCartItems.id_customer, deleteCartItems.id_product);
                        if (res == 1)
                        {
                            DataTable cartTotal = ShopingCartComp.cartTotal(deleteCartItems.id_customer);
                            if (cartTotal.Rows.Count > 0)
                            {
                                decimal sum = decimal.Zero;
                                for (int i = 0; i <= cartTotal.Rows.Count - 1; i++)
                                {
                                    sum = sum + (decimal.Multiply((Int32)cartTotal.Rows[i]["Quantity"], (decimal)cartTotal.Rows[i]["Price"]));
                                }
                                ResponseObj.cartTotalPrice = (Int32)sum;
                            }

                            ResponseObj.status = 1;
                            ResponseObj.message = "Product deleted successfully.";
                            
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage cartDetails(int id_customer, int start=0)
        {
            ShopingCartResponseObj ResponseObj = new ShopingCartResponseObj();
            string response = "";
            try
            {
                if (id_customer > 0)
                {
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start ; // Page Number
                    object objSum;
                    DataTable orrd = new DataTable();
                    DataTable CartItems = ShopingCartComp.GetCartItemsDetails(id_customer, ItemsPerPage, PageNumber);
                    if (CartItems.Rows.Count > 0)
                    {
                        try
                        {
                            orrd = CartItems.Clone();
                            orrd.Columns["image"].DataType = typeof(System.String);
                            
                            foreach (DataRow row in CartItems.Rows)
                            {
                                orrd.ImportRow(row);
                            }

                            if (orrd.Rows.Count > 0)
                            {
                                for (int i = 0; i <= orrd.Rows.Count - 1; i++)
                                {
                                    DataTable storedetail = PasswordComp.StoreInformation();
                                    DataTable getImagesLinks = ProductsComp.GetImageFullNames((Int32)CartItems.Rows[i]["image"]);
                                    string mimetype = "";

                                    var imgurl = storedetail.Rows[0]["Url"].ToString();
                                    if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/jpeg"))
                                    {
                                        mimetype = ".jpg";
                                    }
                                    else if ((getImagesLinks.Rows[0]["MimeType"].ToString()).Contains("image/png"))
                                    {
                                        mimetype = ".png";
                                    }
                                    string imgId = getImagesLinks.Rows[0]["Id"].ToString();
                                    string imgName = getImagesLinks.Rows[0]["SeoFilename"].ToString();
                                    

                                    orrd.Rows[i]["image"] = imgurl + "/media/image/" + imgId + "/" + imgName + mimetype + "";
                                    
                                }
                            }
                        
                       
                        }
                        catch (Exception Err) { }



                        for (int i=0;i<= CartItems.Rows.Count - 1; i++)
                        {
                            CartItems.Rows[i]["total"] = decimal.Multiply((Int32)CartItems.Rows[i]["cart_quantity"], (decimal)CartItems.Rows[i]["total"]);
                        }
                        objSum = CartItems.Compute("Sum(Total)", "");
                        ShopingCartresult shopingCartresult = new ShopingCartresult();
                        shopingCartresult.cartProducts = orrd;
                        shopingCartresult.totalPrice = objSum ;

                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = shopingCartresult;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                        
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CustomerID is required.";
                }
            }
            catch(Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage cartCount(int id_customer)
        {
            ShopingCartCountResponse ResponseObj = new ShopingCartCountResponse();
            string response = "";
            try
            {
                if (id_customer > 0)
                {
                    int itemscount = 0;
                    DataTable CartItems = ShopingCartComp.GetCartItemsCount(id_customer);
                    
                    if (CartItems.Rows.Count > 0)
                    {
                        string str= CartItems.Rows.Count.ToString();

                        itemscount = CartItems.Rows.Count;
                        ResponseObj.status = 1;
                        ResponseObj.message = "Shopping Cart Items Count";
                        ResponseObj.cartCount = (Int32) CartItems.Rows[0][0];
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                       
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CustomerID is required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        


    }
}
