﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DBLibrary;
using System.Web;
using System.Text;
using Pay2CartAPI.Classes;

namespace Pay2CartAPI.Controllers
{
    public class WishListController : ApiController
    {
        // Get Wishlist items of the Customer
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetWishlistItems(int id_customer, int start=0)
        {
            ResponseObj ResponseObj = new ResponseObj();
            string response = "";
            try
            {
                int shopingcarttype = 2; //WhishlistType id: 2
                if (id_customer > 0)
                {
                    int ItemsPerPage = 10; // fixed items to display in a page
                    int PageNumber = start; // Page Number
                    DataTable wishlistitems = WishListComp.GetWhishlisttemsDetails(id_customer, shopingcarttype, ItemsPerPage, PageNumber);
                    if (wishlistitems.Rows.Count > 0)
                    {
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = wishlistitems;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;
                    }
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = "CustomerID is required.";
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        //Update wishlist Quantity
        [HttpPost]
        [Authorize]
        public HttpResponseMessage UpdateWishlilstItems([FromBody]UpdateWishlistitems updateWishlistitems)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (updateWishlistitems != null)
                {
                    if (ModelState.IsValid)
                    {
                        long res = WishListComp.UpdateWishlistItmes(updateWishlistitems.id_customer, updateWishlistitems.id_product, updateWishlistitems.quantity);
                        if (res == 1)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Wishlist Updated Successfully.";
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }


        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddItemstoWishlist([FromBody][Required]AddItemstoWishList addItemstoWishList)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (addItemstoWishList != null)
                {
                    if (ModelState.IsValid)
                    {
                        int productid = addItemstoWishList.id_product;
                        DataTable MaxQty = ShopingCartComp.GetMaxProductQty(productid);

                        if (MaxQty.Rows.Count > 0)
                        {
                            if (!Convert.IsDBNull(MaxQty.Rows[0]["OrderMaximumQuantity"]))
                            {
                                if (addItemstoWishList.quantity > (Int32)MaxQty.Rows[0]["OrderMaximumQuantity"])
                                {
                                    ResponseObj.message = "Max Quantiy Allowed for Purchase is : " + (Int32)MaxQty.Rows[0]["OrderMaximumQuantity"] + ".";
                                    ResponseObj.status = 0;
                                    return Request.CreateResponse(HttpStatusCode.OK, ResponseObj.message);
                                }
                            }
                            if (!Convert.IsDBNull(MaxQty.Rows[0]["AllowedQuantities"]))
                            {
                                if (addItemstoWishList.quantity > (Int32)MaxQty.Rows[0]["AllowedQuantities"])
                                {
                                    ResponseObj.message = "Allowed Quantiy for Purchase is : " + (Int32)MaxQty.Rows[0]["AllowedQuantities"] + ".";
                                    ResponseObj.status = 0;
                                    return Request.CreateResponse(HttpStatusCode.OK, ResponseObj.message);
                                }
                            }

                            //Checking given customerID is is valid or not

                            if (addItemstoWishList.id_customer != 0)
                            {
                                DataTable customerIsValid = ShopingCartComp.CheckCustomerIsvalid(addItemstoWishList.id_customer);
                                if (customerIsValid.Rows.Count == 0)
                                {
                                    ResponseObj.message = "CustomerId is not valid.. Please provide vaild customerid";
                                    ResponseObj.status = 0;
                                }
                                else
                                {
                                    // checking product had bundle items or not?
                                    DataTable ProdctBudles = ShopingCartComp.CheckProductIncluesBundle(addItemstoWishList.id_product);
                                    int? ParentItemId = null; int? BundleItemId = null;
                                    if (ProdctBudles.Rows.Count != 0)
                                    {
                                        int cnt = 0;
                                        //// inserting Product Varinat Combination
                                        string AttributesXml = "";
                                        //if (shopingCartItem.ProdutID_variantId != 0)
                                        //{
                                        //    DataTable GetAttibuteXml = ShopingCartComp.GetProdutAttributeXml(shopingCartItem.ProductId, shopingCartItem.ProdutID_variantId);
                                        //    AttributesXml = GetAttibuteXml.Rows[0]["AttributesXml"].ToString();
                                        //}
                                        long res = WishListComp.AddItemtoWishlist(addItemstoWishList.id_customer, ParentItemId, BundleItemId, addItemstoWishList.id_product, AttributesXml, addItemstoWishList.quantity);
                                        int parentitem = 0;
                                        DataTable bundle = ShopingCartComp.GetParentItemID(addItemstoWishList.id_product, addItemstoWishList.id_customer);
                                        parentitem = (Int32)bundle.Rows[0][0];
                                        for (int i = 0; i <= ProdctBudles.Rows.Count - 1; i++)
                                        {
                                            //// inserting Product Varinat Combination
                                            string AttributesXm = "";
                                            //if (shopingCartItem.ProdutID_variantId != 0)
                                            //{
                                            //    DataTable GetAttibuteXml = ShopingCartComp.GetProdutAttributeXml((Int32)ProdctBudles.Rows[i]["Id"], shopingCartItem.ProdutID_variantId);
                                            //    AttributesXm = GetAttibuteXml.Rows[0]["AttributesXml"].ToString();
                                            //}
                                            long result = WishListComp.AddItemtoWishlist(addItemstoWishList.id_customer, parentitem, (Int32)ProdctBudles.Rows[i]["Id"], (Int32)ProdctBudles.Rows[i]["ProductId"], AttributesXm, (Int32)ProdctBudles.Rows[i]["Quantity"]);
                                            cnt += 1;
                                        }
                                        if (cnt == ProdctBudles.Rows.Count)
                                        {
                                            ResponseObj.status = 1;
                                            ResponseObj.message = "Item Added to Cart Successfylly.";
                                        }
                                        else
                                        {
                                            ResponseObj.message = Common.OopsSomethingWentWrong;
                                            ResponseObj.status = 0;
                                        }
                                    }
                                    else
                                    {
                                        //// inserting Product Varinat Combination
                                        string AttributesXml = "";
                                        if (addItemstoWishList.id_product_variant != 0)
                                        {
                                            DataTable GetAttibuteXml = ShopingCartComp.GetProdutAttributeXml(addItemstoWishList.id_product, addItemstoWishList.id_product_variant);
                                            AttributesXml = GetAttibuteXml.Rows[0]["AttributesXml"].ToString();
                                        }

                                        long res = WishListComp.AddItemtoWishlist(addItemstoWishList.id_customer, ParentItemId, BundleItemId, addItemstoWishList.id_product, AttributesXml, addItemstoWishList.quantity);

                                        if (res == 1)
                                        {
                                            ResponseObj.status = 1;
                                            ResponseObj.message = "Item Added to Cart Successfylly.";
                                        }
                                        else
                                        {
                                            ResponseObj.message = Common.OopsSomethingWentWrong;
                                            ResponseObj.status = 0;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ResponseObj.message = "CustomerId should not be ZERO";
                                ResponseObj.status = 0;
                            }
                        }
                        else
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Poduct is not available.";
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }


        [HttpPost]
        [Authorize]
        public HttpResponseMessage DeleteItemsfromWishlist([FromBody]DeleteWishlistItems deleteWishlistItems)
        {
            string response = "";
            ResponseObj ResponseObj = new ResponseObj();
            try
            {
                if (deleteWishlistItems != null)
                {
                    if (ModelState.IsValid)
                    {
                        long res = WishListComp.DeleteWishlistItems(deleteWishlistItems.id_customer, deleteWishlistItems.id_product);
                        if (res == 1)
                        {
                            ResponseObj.status = 1;
                            ResponseObj.message = "Item Deleted from Wishlist Successfully.";
                        }
                        else
                        {
                            ResponseObj.message = Common.OopsSomethingWentWrong;
                            ResponseObj.status = 0;
                        }
                    }
                    else
                    {
                        var error = ModelState.SelectMany(x => x.Value.Errors).First();
                        if (error.ErrorMessage != null && error.ErrorMessage != String.Empty)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.ErrorMessage);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }


    }
}
