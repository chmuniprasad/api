﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using Pay2CartAPI.Models;
using Newtonsoft.Json;
using CompLibrary;
using System.Data;
using Pay2CartAPI.Response;
using System;
using System.Linq;
using System.Text;
using Pay2CartAPI.Classes;

namespace testwebapi.Controllers
{
    public class NetworksController : ApiController
    {
        [HttpGet]
        [Authorize]
        public HttpResponseMessage RechargeNetworks(int type)
        {
            RechargeResponseObj ResponseObj = new RechargeResponseObj();
            string response = "";
            try
            {
                
                    int serviceid = 1;
                    if (type == 0) { type = 1; }else { type = 0; }
                    DataTable rechargeNetworks = NetworkComp.GetRechargeNetworks(serviceid, type);
                    if (rechargeNetworks.Rows.Count > 0)
                    {
                        Rechargenetworks rechargenetworks = new Rechargenetworks();
                        rechargenetworks.networks = rechargeNetworks;
                        ResponseObj.status = 1;
                        ResponseObj.message = Common.RecordsExists;
                        ResponseObj.result = rechargenetworks;
                    }
                    else
                    {
                        ResponseObj.status = 0;
                        ResponseObj.message = Common.NoRecordsExists;

                    }
               
            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage DthOperators(int type=1)
        {
            DthResponseObj ResponseObj = new DthResponseObj();
            string response = "";
            try
            {

                int serviceid = 2;
                DataTable rechargeNetworks = NetworkComp.GetRechargeNetworks(serviceid,type);
                if (rechargeNetworks.Rows.Count > 0)
                {
                    DthOperators rechargenetworks = new DthOperators();
                    rechargenetworks.operators = rechargeNetworks;
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = rechargenetworks;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;

                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage ElectricityBoards(int type=0)
        {
            ElectricityResponseObj ResponseObj = new ElectricityResponseObj();
            string response = "";
            try
            {

                int serviceid = 3;
                DataTable rechargeNetworks = NetworkComp.GetRechargeNetworks(serviceid,type);
                if (rechargeNetworks.Rows.Count > 0)
                {
                    ElectricityBoards rechargenetworks = new ElectricityBoards();
                    rechargenetworks.electricityBoards = rechargeNetworks;
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = rechargenetworks;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;

                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage gasProviders(int type = 0)
        {
            GasbillResponseObj ResponseObj = new GasbillResponseObj();
            string response = "";
            try
            {

                int serviceid = 4;
                DataTable rechargeNetworks = NetworkComp.GetRechargeNetworks(serviceid, type);
                rechargeNetworks.Columns.Add("isBillGroupVisible");
                for(int i=0;i<= rechargeNetworks.Rows.Count-1;i++)
                {
                    rechargeNetworks.Rows[i]["provider"]=(rechargeNetworks.Rows[i]["provider"].ToString()).Trim();
                    if ((rechargeNetworks.Rows[i]["provider"].ToString()).Trim()== "Mahanagar Gas Limited") { rechargeNetworks.Rows[i]["isBillGroupVisible"] = 1; }
                    else { rechargeNetworks.Rows[i]["isBillGroupVisible"] = 0; }
                }

               
                if (rechargeNetworks.Rows.Count > 0)
                {
                    gasprovider rechargenetworks = new gasprovider();
                    rechargenetworks.gasBillProviders = rechargeNetworks;
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = rechargenetworks;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;

                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage broadBandProviders(int type = 0)
        {
            BroadbandResponseObj ResponseObj = new BroadbandResponseObj();
            string response = "";
            try
            {

                int serviceid = 5;
                DataTable rechargeNetworks = NetworkComp.GetRechargeNetworks(serviceid, type);
               

                if (rechargeNetworks.Rows.Count > 0)
                {
                    broadbandprovider  rechargenetworks = new broadbandprovider();
                    rechargenetworks.broadBandProviders = rechargeNetworks;
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = rechargenetworks;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;

                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

        [HttpGet]
        [Authorize]
        public HttpResponseMessage landlineProviders(int type = 0)
        {
            landlineResponseObj ResponseObj = new landlineResponseObj();
            string response = "";
            try
            {

                int serviceid = 8;
                DataTable rechargeNetworks = NetworkComp.GetRechargeNetworks(serviceid, type);


                if (rechargeNetworks.Rows.Count > 0)
                {
                    landlinedprovider rechargenetworks = new landlinedprovider();
                    rechargenetworks.landLineProviders = rechargeNetworks;
                    ResponseObj.status = 1;
                    ResponseObj.message = Common.RecordsExists;
                    ResponseObj.result = rechargenetworks;
                }
                else
                {
                    ResponseObj.status = 0;
                    ResponseObj.message = Common.NoRecordsExists;

                }

            }
            catch (Exception Err)
            {

            }
            HttpResponseMessage HttpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            response = JsonConvert.SerializeObject(ResponseObj, Formatting.None);
            HttpResponseMessage.Content = new StringContent(response, Encoding.UTF8, "application/json");
            return HttpResponseMessage;
        }

    }
}
