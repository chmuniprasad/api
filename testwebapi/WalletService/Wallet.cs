﻿using CompLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using Pay2CartAPI.Response;
using DBLibrary;
using System.Data.SqlClient;

namespace Pay2CartAPI.WalletService
{
    public class Wallet
    {
        public static long result;
        public static float getCustomerCurrentWalletBalance(int customerID)
        {
            float CurrentBalance = 0;

            try
            {
                if(customerID > 0)
                {
                    DataTable dt = WalletComp.getCustomerCurrentWalletBalance(customerID);
                    if(dt.Rows.Count > 0)
                    {
                        int PositiveAmount = 0;
                        int nagativeAmount = 0;
                        try
                        {
                            if (dt.Rows[0]["PositiveAmount"] != null)
                            {
                                PositiveAmount = Convert.ToInt32(dt.Rows[0]["PositiveAmount"]);
                            }
                        }catch(Exception Err)
                        {
                            PositiveAmount = 0;
                        }

                        try
                        {
                            if (dt.Rows[0]["nagativeAmount"] != null)
                            {
                                nagativeAmount = Convert.ToInt32(dt.Rows[0]["nagativeAmount"]);
                            }
                        }
                        catch(Exception Err)
                        {
                            nagativeAmount = 0;
                        }
                        

                        CurrentBalance = PositiveAmount - nagativeAmount;

                    }
                }

            }catch(Exception Err)
            {
                
                //Console.Write(Err.ClassName);
            }

            return CurrentBalance;
        }

        public static bool saveWalletBalance(string userID, string orderID, string amount, string balanceAmount, int type, int serviceID, string IPAddress, string desc)
        {
            bool result = false;
            try
            {
                WalletComp.saveWalletBalance(userID,orderID, amount, balanceAmount, type, serviceID, IPAddress, desc);
                result = true;

            }catch(Exception Err)
            {

            }
            return result;
        }

        public static object createWalletOrder(int customerID, int amount)
        {
            InsufficientRechargeFundsObjWallet ISRFObj = new InsufficientRechargeFundsObjWallet();
            try
            {

                long OrderId = WalletComp.createWalletOrder(customerID, 0, amount, 6, 1, "Add Money");
                

                ISRFObj.status = 1;
                ISRFObj.message = "Connecting to payment gateway.";
                ISRFObj.pendingAmount = amount;
                ISRFObj.orderID = "wlt" + OrderId;

            }
            catch(Exception Err)
            {

            }
           
            return JsonConvert.SerializeObject(ISRFObj).ToString(); 
        }

        public static int getWalletOrderDetailsByOrderId(int OrderID)
        {
            int customerID=0;
            try
            {

                DataTable dt = WalletComp.getWalletOrderDetailsByOrderId(OrderID);
                if(dt.Rows.Count >0)
                {
                    customerID = Convert.ToInt32(dt.Rows[0]["customerID"]);
                }
            }
            catch(Exception Err)
            {

            }
            return customerID;
        }

        public static string createSendMoneyWalletOrder(int customerID, int toCustomerID, int amount, int orderID=0)
        {
            string res = "";
            InsufficientRechargeFundsObjWallet ISRFObj = new InsufficientRechargeFundsObjWallet();
            RechargeBillPaymentsObj ResponseObj = new RechargeBillPaymentsObj();
            try
            {
                float currentBalacne =  getCustomerCurrentWalletBalance(customerID);
                if (orderID==0)
                {
                    long OrderId = WalletComp.createWalletOrder(customerID, toCustomerID, amount, 7, 2, "Send Money");
                    orderID = Convert.ToInt32(OrderId);
                }
                if(currentBalacne >= amount)
                {
                    float balanceAmount = currentBalacne - amount;

                    string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                    string IpAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();

                    //Deducting money from logged in user 
                    saveWalletBalance(customerID.ToString(), orderID.ToString(), amount.ToString(), balanceAmount.ToString(), 2, 7, IpAddress, "Send Money");
                    //adding money to another user 
                    float TousercurrentBalacne = getCustomerCurrentWalletBalance(toCustomerID);
                    float TouserbalanceAmount = TousercurrentBalacne + amount;
                    saveWalletBalance(toCustomerID.ToString(), orderID.ToString(), amount.ToString(), TouserbalanceAmount.ToString(), 1, 7, IpAddress, "Recive Money through send money");

                    ResponseObj.status = 1;
                    ResponseObj.message = "Success.";

                    rbresult rbresult = new rbresult();
                    rbresult.message = ResponseObj.message;
                    rbresult.order_id = orderID.ToString();
                    ResponseObj.result = rbresult;

                    res = JsonConvert.SerializeObject(ResponseObj).ToString();
                }
                else
                {
                    float pendingAmount = Convert.ToInt32(amount) - currentBalacne;

                    ISRFObj.status = 0;
                    ISRFObj.message = "Connecting to payment gateway.";
                    ISRFObj.pendingAmount = Convert.ToInt32(pendingAmount);
                    ISRFObj.orderID ="snd" + orderID;
                    res = JsonConvert.SerializeObject(ISRFObj).ToString();
                }
                
            }
            catch (Exception Err)
            {
                res = JsonConvert.SerializeObject(ISRFObj).ToString();
            }

            return res;
        }

        public static DataTable getRechargeOrdersById(int orderID)
        {
            DataTable dt = new DataTable();
            try
            {

                dt = RechargeBillPaymentsDB.getRechargeOrdersByID(orderID);

            }
            catch (Exception Err)
            {

            }
            return dt;
        }
        public static DataTable getOrderDetailsByOrderId(int orderID)
        {
            DataTable dt = new DataTable();
            try
            {

                dt = OrdersDB.getOrderDetailsByOrderId(orderID);

            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static DataTable checkWalletOrderExists(int orderID, int serviceID)
        {
            DataTable dt = new DataTable();
            try
            {

                dt = WalletDB.checkWalletOrderExists(orderID, serviceID);

            }
            catch (Exception Err)
            {

            }
            return dt;
        }

        public static long SaveUpdate_PaymentCallbackLog(bool flgInsert, string OrderId, string Request, string Response, DateTime CreatedAt, DateTime UpdatedAt)
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@flgInsert", SqlDbType.Bit) { Value = flgInsert },
                    new SqlParameter("@OrderId", SqlDbType.VarChar) { Value = OrderId },
                    new SqlParameter("@Request", SqlDbType.VarChar) { Value = Request },
                    new SqlParameter("@Response", SqlDbType.VarChar) { Value = Response },
                    new SqlParameter("@CreatedAt", SqlDbType.DateTime) { Value = CreatedAt },
                    new SqlParameter("@UpdatedAt", SqlDbType.DateTime) { Value = UpdatedAt }

                };
                result = ClsDAL.ExecuteNonQuery(null, CommandType.StoredProcedure, "Sp_PaymentCallbackLogs", parameters);

            }
            catch (Exception Err)
            {

            }
            return result;
        }

    }
}