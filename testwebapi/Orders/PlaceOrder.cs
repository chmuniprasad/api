﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CompLibrary;
using Newtonsoft.Json;
using System.Configuration;
using System.Data;
using System.Net;
using Pay2CartAPI.Classes;
using Pay2CartAPI.Response;
using Pay2CartAPI.WalletService;
using System.Web.Mvc;
using System.Xml.Linq;
using Pay2CartAPI.Models;

namespace Pay2CartAPI
{
    

    public class PlaceOrder
    {
        public DataTable MaxQty;
        public decimal result = decimal.Zero;
        public static string MyIPAddress = "";
        public DataTable dt3;
        public DataTable dt4;
        public static decimal ProductInitialPrice = decimal.Zero;
        public static decimal ProductAdjPrice = decimal.Zero;
        public static decimal ProductFinalCost = decimal.Zero;
        public static decimal ProductFinalCostInclTax = decimal.Zero;
        public static decimal ProductFinalCostExclTax = decimal.Zero;
        public static decimal CartSubTotal = decimal.Zero;
        public static decimal CartSubTotalInclTax = decimal.Zero;
        public static decimal CartSubTotalExclTax = decimal.Zero;
        public static decimal taxrate = decimal.Zero;
        public static decimal ProductTierPrice = decimal.Zero;


        public static string CreateOrderWithPayment(string  id_customer, string id_address_delivery, string useFromWallet,string order_from,string order_id)
        {
            string res ;
            long res2;
            string msg = "";

            CreateOrderPaymentResponseObj ResponseObj = new CreateOrderPaymentResponseObj();
            createorderPaymentResult createorderPaymentResult = new createorderPaymentResult();
            InsufficientRechargeFundsObjWallet ISRFObj = new InsufficientRechargeFundsObjWallet();
            try
            {
                DataTable getaddressofcustomer = OrdersComp.getcustomeraddress(int.Parse(id_customer), int.Parse(id_address_delivery));
                if (getaddressofcustomer.Rows.Count > 0)
                {
                    ConfirmOrder confirmOrder = new ConfirmOrder();
                    InsertOrderItems insertOrderItems = new InsertOrderItems();
                    if (order_id == "") { order_id = "0"; }
                    //Checking given order payment status
                    if (int.Parse(order_id) > 0)
                    {
                        //checking payment done or not for the "order_id"
                        DataTable paymentSucess = OrdersComp.getOrderDetails(int.Parse(id_customer), int.Parse(order_id));
                        for (int c = 0; c <= paymentSucess.Rows.Count - 1; c++)
                        {
                            if (paymentSucess.Rows[c]["Id"].ToString().Contains(order_id))
                            {
                                float currentbal = Wallet.getCustomerCurrentWalletBalance(int.Parse(id_customer));
                                ////orderid created and use from wallet =1 
                                //if (useFromWallet == "1")
                                //{
                                    if (currentbal >= (Int32)CartSubTotal)
                                    {
                                        float PrsntBlnc = currentbal - Convert.ToInt32(CartSubTotal);
                                        //deducting the wallet balance
                                        Wallet.saveWalletBalance(id_customer.ToString(), order_id, CartSubTotal.ToString(), PrsntBlnc.ToString(), 2, 2, MyIPAddress, "Place Order UsingPayment Method");
                                        createorderPaymentResult.message = "Order Sucess.";
                                        createorderPaymentResult.order_id = int.Parse(order_id);
                                        ResponseObj.result = createorderPaymentResult;
                                        ResponseObj.status = 1;
                                        try
                                        {
                                            DataTable orderDet = OrdersComp.getOrderDetails(int.Parse(id_customer), int.Parse(order_id));
                                            RemoveFromCart(int.Parse(id_customer));
                                            if (orderDet.Rows.Count > 0)
                                            {
                                                for (int i = 0; i <= orderDet.Rows.Count - 1; i++)
                                                {
                                                    UpdateProductStock((Int32)orderDet.Rows[i]["ProductId"], (Int32)orderDet.Rows[i]["Quantity"]);
                                                }
                                            }
                                        }
                                        catch (Exception Err) { }

                                        res = JsonConvert.SerializeObject(ResponseObj).ToString();
                                        return res;
                                    }
                                    else
                                    {
                                        ISRFObj.status = 0;
                                        ISRFObj.message = "InSufficient Funds";
                                        float pendingAmount = Convert.ToInt32(CartSubTotal) - currentbal;
                                        ISRFObj.pendingAmount = Convert.ToInt32(pendingAmount);
                                        ISRFObj.orderID = "odr" + order_id;
                                        res = JsonConvert.SerializeObject(ISRFObj).ToString();
                                        return res;
                                    }
                                //}
                                ////orderid created and use from wallet =0
                                //else
                                //{
                                //    ISRFObj.status = 0;
                                //    //ISRFObj.message = "Payment Pending";
                                //    ISRFObj.message = Common.OopsSomethingWentWrong;
                                //    float pendingAmount = Convert.ToInt32(CartSubTotal);
                                //    ISRFObj.pendingAmount = Convert.ToInt32(pendingAmount);
                                //    ISRFObj.orderID = "odr" + order_id;
                                //    res = JsonConvert.SerializeObject(ISRFObj).ToString();
                                //    return res;
                                //}
                            }
                        }
                    }
                    else
                    {
                        CartSubTotal = decimal.Zero;
                        CartSubTotalInclTax = decimal.Zero;
                        CartSubTotalExclTax = decimal.Zero;
                        int Customerid = int.Parse(id_customer);
                        // checking bundle items are available in cart or not
                        DataTable dtr = OrdersComp.CheckBundleIteminCart(Customerid);
                        bool bundleitem = false;
                        if (dtr.Rows.Count > 0)
                        {
                            for (int i = 0; i <= dtr.Rows.Count - 1; i++)
                            {
                                bundleitem = (bool)dtr.Rows[i][0];
                                //Checking Product Stock availability before purchase
                                DataTable MaxQty = OrdersComp.GetCartItemsBeforeOrder(Customerid, bundleitem);

                                if (MaxQty.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in MaxQty.Rows)
                                    {
                                        if ((Int32)(dr["StockQuantity"]) < (Int32)(dr["Quantity"]))
                                        {
                                            createorderPaymentResult.message = "Product Qty is not Available in Stock.";
                                            ResponseObj.result = createorderPaymentResult;
                                            ResponseObj.status = 2;
                                        }
                                        else
                                        {
                                            PlaceOrder placeOrder = new PlaceOrder();
                                            placeOrder.GetCartSubTotal(dr);
                                        }
                                        CartSubTotal += ProductFinalCost;
                                        CartSubTotalInclTax += ProductFinalCostInclTax;
                                        CartSubTotalExclTax += ProductFinalCostExclTax;
                                    }
                                    try
                                    {
                                        // MyIPAddress = new WebClient().DownloadString("http://icanhazip.com");
                                        MyIPAddress = Common.IPaddress;
                                    }
                                    catch (Exception Err) { }

                                    float currentbal = Wallet.getCustomerCurrentWalletBalance(Customerid);
                                    //order with payment option and checked "usefromwallet=1" option
                                    if (useFromWallet == "1")
                                    {
                                        if (order_id == "" || order_id == "0" || order_id == null)
                                        {

                                            if (currentbal >= (Int32)CartSubTotal)
                                            {

                                                #region ORDER Data
                                                confirmOrder.StoreId = 1;
                                                confirmOrder.CustomerId = Customerid;
                                                confirmOrder.BillingAddressId = int.Parse(id_address_delivery);
                                                confirmOrder.ShippingAddressId = int.Parse(id_address_delivery);
                                                confirmOrder.OrderStatusId = 20;
                                                confirmOrder.ShippingStatusId = 20;
                                                confirmOrder.PaymentStatusId = 30;
                                                confirmOrder.PaymentMethodSystemName = "Online Payment";
                                                confirmOrder.CustomerCurrencyCode = "INR";
                                                confirmOrder.CurrencyRate = 1;
                                                confirmOrder.CustomerTaxDisplayTypeId = 10;
                                                confirmOrder.VatNumber = "";
                                                confirmOrder.OrderSubtotalInclTax = CartSubTotalInclTax;
                                                confirmOrder.OrderSubtotalExclTax = CartSubTotalExclTax;
                                                confirmOrder.OrderSubTotalDiscountInclTax = 0;
                                                confirmOrder.OrderSubTotalDiscountExclTax = 0;
                                                confirmOrder.OrderShippingInclTax = 0;
                                                confirmOrder.OrderShippingExclTax = 0;
                                                confirmOrder.PaymentMethodAdditionalFeeInclTax = 0;
                                                confirmOrder.PaymentMethodAdditionalFeeExclTax = 0;
                                                confirmOrder.TaxRates = "0:0;";
                                                confirmOrder.OrderTax = 0;
                                                confirmOrder.OrderDiscount = 0;
                                                confirmOrder.OrderTotal = CartSubTotal;
                                                confirmOrder.RefundedAmount = 0;
                                                confirmOrder.RewardPointsWereAdded = false;
                                                confirmOrder.CheckoutAttributeDescription = "";
                                                confirmOrder.CheckoutAttributesXml = "";
                                                confirmOrder.CustomerLanguageId = 1;
                                                confirmOrder.AffiliateId = 1;
                                                confirmOrder.CustomerIp = MyIPAddress;
                                                confirmOrder.AllowStoringCreditCardNumber = false;
                                                confirmOrder.CardType = "";
                                                confirmOrder.CardName = "";
                                                confirmOrder.CardNumber = "";
                                                confirmOrder.MaskedCreditCardNumber = "";
                                                confirmOrder.CardCvv2 = "";
                                                confirmOrder.CardExpirationMonth = "";
                                                confirmOrder.CardExpirationYear = "";
                                                confirmOrder.AllowStoringDirectDebit = false;
                                                confirmOrder.DirectDebitAccountHolder = "";
                                                confirmOrder.DirectDebitAccountNumber = "";
                                                confirmOrder.DirectDebitBankCode = "";
                                                confirmOrder.DirectDebitBankName = "";
                                                confirmOrder.DirectDebitBIC = "";
                                                confirmOrder.DirectDebitCountry = "";
                                                confirmOrder.DirectDebitIban = "";
                                                confirmOrder.AuthorizationTransactionId = "";
                                                confirmOrder.AuthorizationTransactionCode = "";
                                                confirmOrder.AuthorizationTransactionResult = "";
                                                confirmOrder.CaptureTransactionId = "";
                                                confirmOrder.CaptureTransactionResult = "";
                                                confirmOrder.SubscriptionTransactionId = "";
                                                confirmOrder.PurchaseOrderNumber = "";
                                                confirmOrder.PaidDateUtc = DateTime.Now;
                                                confirmOrder.ShippingMethod = "By Ground";
                                                confirmOrder.ShippingRateComputationMethodSystemName = "Shipping.FixedRate";
                                                confirmOrder.Deleted = false;
                                                confirmOrder.CreatedOnUtc = DateTime.Now;
                                                confirmOrder.UpdatedOnUtc = DateTime.Now;
                                                confirmOrder.RewardPointsRemaining = 0;
                                                confirmOrder.CustomerOrderComment = "";
                                                confirmOrder.OrderShippingTaxRate = 0;
                                                confirmOrder.PaymentMethodAdditionalFeeTaxRate = 0;
                                                confirmOrder.HasNewPaymentNotification = false;
                                                confirmOrder.AcceptThirdPartyEmailHandOver = false;
                                                confirmOrder.OrderTotalRounding = 0;
                                                confirmOrder.order_from = order_from;
                                                #endregion ORDER Data 

                                                res2 = OrdersComp.ConfirmOrder(confirmOrder.StoreId, confirmOrder.CustomerId, confirmOrder.BillingAddressId, confirmOrder.ShippingAddressId, confirmOrder.OrderStatusId, confirmOrder.ShippingStatusId, confirmOrder.PaymentStatusId, confirmOrder.PaymentMethodSystemName, confirmOrder.CustomerCurrencyCode, confirmOrder.CurrencyRate, confirmOrder.
                                                                                    CustomerTaxDisplayTypeId, confirmOrder.VatNumber, confirmOrder.OrderSubtotalInclTax, confirmOrder.OrderSubtotalExclTax, confirmOrder.OrderSubTotalDiscountInclTax, confirmOrder.OrderSubTotalDiscountExclTax, confirmOrder.OrderShippingInclTax, confirmOrder.OrderShippingExclTax, confirmOrder.
                                                                                    PaymentMethodAdditionalFeeInclTax, confirmOrder.PaymentMethodAdditionalFeeExclTax, confirmOrder.TaxRates, confirmOrder.OrderTax, confirmOrder.OrderDiscount, confirmOrder.OrderTotal, confirmOrder.RefundedAmount, confirmOrder.RewardPointsWereAdded, confirmOrder.CheckoutAttributeDescription, confirmOrder.CheckoutAttributesXml, confirmOrder.
                                                                                    CustomerLanguageId, confirmOrder.AffiliateId, confirmOrder.CustomerIp, confirmOrder.AllowStoringCreditCardNumber, confirmOrder.CardType, confirmOrder.CardName, confirmOrder.CardNumber, confirmOrder.MaskedCreditCardNumber, confirmOrder.CardCvv2, confirmOrder.CardExpirationMonth, confirmOrder.CardExpirationYear, confirmOrder.AllowStoringDirectDebit, confirmOrder.
                                                                                    DirectDebitAccountHolder, confirmOrder.DirectDebitAccountNumber, confirmOrder.DirectDebitBankCode, confirmOrder.DirectDebitBankName, confirmOrder.DirectDebitBIC, confirmOrder.DirectDebitCountry, confirmOrder.DirectDebitIban, confirmOrder.AuthorizationTransactionId, confirmOrder.AuthorizationTransactionCode, confirmOrder.
                                                                                    AuthorizationTransactionResult, confirmOrder.CaptureTransactionId, confirmOrder.CaptureTransactionResult, confirmOrder.SubscriptionTransactionId, confirmOrder.PurchaseOrderNumber, confirmOrder.PaidDateUtc, confirmOrder.ShippingMethod, confirmOrder.ShippingRateComputationMethodSystemName, confirmOrder.Deleted, confirmOrder.
                                                                                    CreatedOnUtc, confirmOrder.UpdatedOnUtc, confirmOrder.RewardPointsRemaining, confirmOrder.CustomerOrderComment, confirmOrder.OrderShippingTaxRate, confirmOrder.PaymentMethodAdditionalFeeTaxRate, confirmOrder.HasNewPaymentNotification, confirmOrder.AcceptThirdPartyEmailHandOver, confirmOrder.OrderTotalRounding, confirmOrder.order_from);

                                                //Insert data in OrderItem table

                                                foreach (DataRow dr2 in MaxQty.Rows)
                                                {
                                                    //calculating cart subtotal value
                                                    PlaceOrder placeOrder = new PlaceOrder();
                                                    placeOrder.GetCartSubTotal(dr2);

                                                    #region OrderItem Data
                                                    insertOrderItems.ProductId = (Int32)dr2["ProductId"];
                                                    insertOrderItems.Quantity = (Int32)dr2["Quantity"];
                                                    insertOrderItems.UnitPriceInclTax = (decimal)dr2["Price"];
                                                    insertOrderItems.UnitPriceExclTax = (decimal)dr2["Price"];
                                                    insertOrderItems.PriceInclTax = ProductFinalCostInclTax;
                                                    insertOrderItems.PriceExclTax = ProductFinalCostExclTax;
                                                    insertOrderItems.DiscountAmountInclTax = 0;
                                                    insertOrderItems.DiscountAmountExclTax = 0;
                                                    insertOrderItems.AttributeDescription = "";
                                                    insertOrderItems.AttributesXml = dr2["AttributesXml"].ToString();
                                                    insertOrderItems.DownloadCount = 0;
                                                    insertOrderItems.IsDownloadActivated = false;
                                                    insertOrderItems.LicenseDownloadId = 0;
                                                    insertOrderItems.ItemWeight = 0;
                                                    insertOrderItems.BundleData = "";
                                                    insertOrderItems.ProductCost = 0;
                                                    insertOrderItems.TaxRate = 0;

                                                    #endregion OrderItem Data

                                                    int CustomerId = confirmOrder.CustomerId;
                                                    long res1 = OrdersComp.InsertOrderItemsData(CustomerId, insertOrderItems.ProductId, insertOrderItems.Quantity, insertOrderItems.UnitPriceInclTax, insertOrderItems.UnitPriceExclTax, insertOrderItems.PriceInclTax, insertOrderItems.PriceExclTax, insertOrderItems.DiscountAmountInclTax, insertOrderItems.DiscountAmountExclTax, insertOrderItems.AttributeDescription,
                                                                                               insertOrderItems.AttributesXml, insertOrderItems.DownloadCount, insertOrderItems.IsDownloadActivated, insertOrderItems.LicenseDownloadId, insertOrderItems.ItemWeight, insertOrderItems.BundleData, insertOrderItems.ProductCost, insertOrderItems.TaxRate);
                                                    RemoveFromCart(Customerid);
                                                    //UpdateProductStock updateProductStock = new UpdateProductStock();
                                                    UpdateProductStock((Int32)dr2["ProductId"], (Int32)dr2["Quantity"]);
                                                }

                                                //if wallet balance is more than cart value
                                                float PrsntBlnc = currentbal - Convert.ToInt32(CartSubTotal);
                                                //deducting the wallet balance
                                                Wallet.saveWalletBalance(Customerid.ToString(), res2.ToString(), CartSubTotal.ToString(), PrsntBlnc.ToString(), 2, 2, MyIPAddress, "Place Order UsingPayment Method");
                                                msg = "Order success";

                                            }
                                            else
                                            {
                                                //order with payment option and checked "usefromwallet=1" option, but wallet balance is less than cart value

                                                #region ORDER Data
                                                confirmOrder.StoreId = 1;
                                                confirmOrder.CustomerId = Customerid;
                                                confirmOrder.BillingAddressId = int.Parse(id_address_delivery);
                                                confirmOrder.ShippingAddressId = int.Parse(id_address_delivery);
                                                confirmOrder.OrderStatusId = 10;
                                                confirmOrder.ShippingStatusId = 20;
                                                confirmOrder.PaymentStatusId = 10;
                                                confirmOrder.PaymentMethodSystemName = "Online Payment";
                                                confirmOrder.CustomerCurrencyCode = "INR";
                                                confirmOrder.CurrencyRate = 1;
                                                confirmOrder.CustomerTaxDisplayTypeId = 10;
                                                confirmOrder.VatNumber = "";
                                                confirmOrder.OrderSubtotalInclTax = CartSubTotalInclTax;
                                                confirmOrder.OrderSubtotalExclTax = CartSubTotalExclTax;
                                                confirmOrder.OrderSubTotalDiscountInclTax = 0;
                                                confirmOrder.OrderSubTotalDiscountExclTax = 0;
                                                confirmOrder.OrderShippingInclTax = 0;
                                                confirmOrder.OrderShippingExclTax = 0;
                                                confirmOrder.PaymentMethodAdditionalFeeInclTax = 0;
                                                confirmOrder.PaymentMethodAdditionalFeeExclTax = 0;
                                                confirmOrder.TaxRates = "0:0;";
                                                confirmOrder.OrderTax = 0;
                                                confirmOrder.OrderDiscount = 0;
                                                confirmOrder.OrderTotal = CartSubTotal;
                                                confirmOrder.RefundedAmount = 0;
                                                confirmOrder.RewardPointsWereAdded = false;
                                                confirmOrder.CheckoutAttributeDescription = "";
                                                confirmOrder.CheckoutAttributesXml = "";
                                                confirmOrder.CustomerLanguageId = 1;
                                                confirmOrder.AffiliateId = 1;
                                                confirmOrder.CustomerIp = MyIPAddress;
                                                confirmOrder.AllowStoringCreditCardNumber = false;
                                                confirmOrder.CardType = "";
                                                confirmOrder.CardName = "";
                                                confirmOrder.CardNumber = "";
                                                confirmOrder.MaskedCreditCardNumber = "";
                                                confirmOrder.CardCvv2 = "";
                                                confirmOrder.CardExpirationMonth = "";
                                                confirmOrder.CardExpirationYear = "";
                                                confirmOrder.AllowStoringDirectDebit = false;
                                                confirmOrder.DirectDebitAccountHolder = "";
                                                confirmOrder.DirectDebitAccountNumber = "";
                                                confirmOrder.DirectDebitBankCode = "";
                                                confirmOrder.DirectDebitBankName = "";
                                                confirmOrder.DirectDebitBIC = "";
                                                confirmOrder.DirectDebitCountry = "";
                                                confirmOrder.DirectDebitIban = "";
                                                confirmOrder.AuthorizationTransactionId = "";
                                                confirmOrder.AuthorizationTransactionCode = "";
                                                confirmOrder.AuthorizationTransactionResult = "";
                                                confirmOrder.CaptureTransactionId = "";
                                                confirmOrder.CaptureTransactionResult = "";
                                                confirmOrder.SubscriptionTransactionId = "";
                                                confirmOrder.PurchaseOrderNumber = "";
                                                confirmOrder.PaidDateUtc = DateTime.Now;
                                                confirmOrder.ShippingMethod = "By Ground";
                                                confirmOrder.ShippingRateComputationMethodSystemName = "Shipping.FixedRate";
                                                confirmOrder.Deleted = false;
                                                confirmOrder.CreatedOnUtc = DateTime.Now;
                                                confirmOrder.UpdatedOnUtc = DateTime.Now;
                                                confirmOrder.RewardPointsRemaining = 0;
                                                confirmOrder.CustomerOrderComment = "";
                                                confirmOrder.OrderShippingTaxRate = 0;
                                                confirmOrder.PaymentMethodAdditionalFeeTaxRate = 0;
                                                confirmOrder.HasNewPaymentNotification = false;
                                                confirmOrder.AcceptThirdPartyEmailHandOver = false;
                                                confirmOrder.OrderTotalRounding = 0;

                                                #endregion ORDER Data 

                                                res2 = OrdersComp.ConfirmOrder(confirmOrder.StoreId, confirmOrder.CustomerId, confirmOrder.BillingAddressId, confirmOrder.ShippingAddressId, confirmOrder.OrderStatusId, confirmOrder.ShippingStatusId, confirmOrder.PaymentStatusId, confirmOrder.PaymentMethodSystemName, confirmOrder.CustomerCurrencyCode, confirmOrder.CurrencyRate, confirmOrder.
                                                                                    CustomerTaxDisplayTypeId, confirmOrder.VatNumber, confirmOrder.OrderSubtotalInclTax, confirmOrder.OrderSubtotalExclTax, confirmOrder.OrderSubTotalDiscountInclTax, confirmOrder.OrderSubTotalDiscountExclTax, confirmOrder.OrderShippingInclTax, confirmOrder.OrderShippingExclTax, confirmOrder.
                                                                                    PaymentMethodAdditionalFeeInclTax, confirmOrder.PaymentMethodAdditionalFeeExclTax, confirmOrder.TaxRates, confirmOrder.OrderTax, confirmOrder.OrderDiscount, confirmOrder.OrderTotal, confirmOrder.RefundedAmount, confirmOrder.RewardPointsWereAdded, confirmOrder.CheckoutAttributeDescription, confirmOrder.CheckoutAttributesXml, confirmOrder.
                                                                                    CustomerLanguageId, confirmOrder.AffiliateId, confirmOrder.CustomerIp, confirmOrder.AllowStoringCreditCardNumber, confirmOrder.CardType, confirmOrder.CardName, confirmOrder.CardNumber, confirmOrder.MaskedCreditCardNumber, confirmOrder.CardCvv2, confirmOrder.CardExpirationMonth, confirmOrder.CardExpirationYear, confirmOrder.AllowStoringDirectDebit, confirmOrder.
                                                                                    DirectDebitAccountHolder, confirmOrder.DirectDebitAccountNumber, confirmOrder.DirectDebitBankCode, confirmOrder.DirectDebitBankName, confirmOrder.DirectDebitBIC, confirmOrder.DirectDebitCountry, confirmOrder.DirectDebitIban, confirmOrder.AuthorizationTransactionId, confirmOrder.AuthorizationTransactionCode, confirmOrder.
                                                                                    AuthorizationTransactionResult, confirmOrder.CaptureTransactionId, confirmOrder.CaptureTransactionResult, confirmOrder.SubscriptionTransactionId, confirmOrder.PurchaseOrderNumber, confirmOrder.PaidDateUtc, confirmOrder.ShippingMethod, confirmOrder.ShippingRateComputationMethodSystemName, confirmOrder.Deleted, confirmOrder.
                                                                                    CreatedOnUtc, confirmOrder.UpdatedOnUtc, confirmOrder.RewardPointsRemaining, confirmOrder.CustomerOrderComment, confirmOrder.OrderShippingTaxRate, confirmOrder.PaymentMethodAdditionalFeeTaxRate, confirmOrder.HasNewPaymentNotification, confirmOrder.AcceptThirdPartyEmailHandOver, confirmOrder.OrderTotalRounding, confirmOrder.order_from);

                                                //Insert data in OrderItem table

                                                foreach (DataRow dr2 in MaxQty.Rows)
                                                {
                                                    //calculating cart subtotal value
                                                    PlaceOrder placeOrder = new PlaceOrder();
                                                    placeOrder.GetCartSubTotal(dr2);

                                                    #region OrderItem Data
                                                    insertOrderItems.ProductId = (Int32)dr2["ProductId"];
                                                    insertOrderItems.Quantity = (Int32)dr2["Quantity"];
                                                    insertOrderItems.UnitPriceInclTax = (decimal)dr2["Price"];
                                                    insertOrderItems.UnitPriceExclTax = (decimal)dr2["Price"];
                                                    insertOrderItems.PriceInclTax = ProductFinalCostInclTax;
                                                    insertOrderItems.PriceExclTax = ProductFinalCostExclTax;
                                                    insertOrderItems.DiscountAmountInclTax = 0;
                                                    insertOrderItems.DiscountAmountExclTax = 0;
                                                    insertOrderItems.AttributeDescription = "";
                                                    insertOrderItems.AttributesXml = dr2["AttributesXml"].ToString();
                                                    insertOrderItems.DownloadCount = 0;
                                                    insertOrderItems.IsDownloadActivated = false;
                                                    insertOrderItems.LicenseDownloadId = 0;
                                                    insertOrderItems.ItemWeight = 0;
                                                    insertOrderItems.BundleData = "";
                                                    insertOrderItems.ProductCost = 0;
                                                    insertOrderItems.TaxRate = 0;

                                                    #endregion OrderItem Data

                                                    int CustomerId = confirmOrder.CustomerId;
                                                    long res1 = OrdersComp.InsertOrderItemsData(CustomerId, insertOrderItems.ProductId, insertOrderItems.Quantity, insertOrderItems.UnitPriceInclTax, insertOrderItems.UnitPriceExclTax, insertOrderItems.PriceInclTax, insertOrderItems.PriceExclTax, insertOrderItems.DiscountAmountInclTax, insertOrderItems.DiscountAmountExclTax, insertOrderItems.AttributeDescription,
                                                                                               insertOrderItems.AttributesXml, insertOrderItems.DownloadCount, insertOrderItems.IsDownloadActivated, insertOrderItems.LicenseDownloadId, insertOrderItems.ItemWeight, insertOrderItems.BundleData, insertOrderItems.ProductCost, insertOrderItems.TaxRate);
                                                    // RemoveFromCart(Customerid);
                                                    //UpdateProductStock updateProductStock = new UpdateProductStock();
                                                    //UpdateProductStock((Int32)dr2["ProductId"], (Int32)dr2["Quantity"]);
                                                }

                                                ISRFObj.status = 0;
                                                ISRFObj.message = "InSufficient Funds";
                                                float pendingAmount = Convert.ToInt32(CartSubTotal) - currentbal;

                                                ISRFObj.pendingAmount = Convert.ToInt32(pendingAmount);
                                                ISRFObj.orderID = "odr" + res2;
                                                res = JsonConvert.SerializeObject(ISRFObj).ToString();
                                                return res;
                                            }
                                            if (res2 > 0)
                                            {
                                                createorderPaymentResult.message = "Order Placed Successfully.";
                                                createorderPaymentResult.order_id = res2;
                                                ResponseObj.status = 1;
                                                ResponseObj.result = createorderPaymentResult;


                                            }
                                            else
                                            {
                                                createorderPaymentResult.message = Common.OopsSomethingWentWrong;
                                                ResponseObj.result = createorderPaymentResult;
                                                ResponseObj.status = 0;
                                            }
                                        }
                                        else
                                        {
                                            //checking payment done or not for the "order_id"
                                            DataTable paymentSucess = OrdersComp.getOrderDetails(int.Parse(id_customer), int.Parse(order_id));
                                            for (int c = 0; c <= paymentSucess.Rows.Count - 1; c++)
                                            {
                                                if (paymentSucess.Rows[c]["Id"].ToString().Contains(order_id))
                                                {
                                                    float PrsntBlnc = currentbal - Convert.ToInt32(CartSubTotal);
                                                    //deducting the wallet balance
                                                    Wallet.saveWalletBalance(Customerid.ToString(), order_id, CartSubTotal.ToString(), PrsntBlnc.ToString(), 2, 2, MyIPAddress, "Place Order UsingPayment Method");
                                                    createorderPaymentResult.message = "Order Sucess.";
                                                    ResponseObj.result = createorderPaymentResult;
                                                    CartSubTotal = decimal.Zero;
                                                    ResponseObj.status = 1;
                                                    res = JsonConvert.SerializeObject(ResponseObj).ToString();
                                                    return res;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //order with payment option and checked "usefromwallet=0" option
                                        if (order_id == "" || order_id == null || order_id == "0")
                                        {
                                            #region ORDER Data
                                            confirmOrder.StoreId = 1;
                                            confirmOrder.CustomerId = Customerid;
                                            confirmOrder.BillingAddressId = int.Parse(id_address_delivery);
                                            confirmOrder.ShippingAddressId = int.Parse(id_address_delivery);
                                            confirmOrder.OrderStatusId = 10;
                                            confirmOrder.ShippingStatusId = 20;
                                            confirmOrder.PaymentStatusId = 10;
                                            confirmOrder.PaymentMethodSystemName = "Online Payment";
                                            confirmOrder.CustomerCurrencyCode = "INR";
                                            confirmOrder.CurrencyRate = 1;
                                            confirmOrder.CustomerTaxDisplayTypeId = 10;
                                            confirmOrder.VatNumber = "";
                                            confirmOrder.OrderSubtotalInclTax = CartSubTotalInclTax;
                                            confirmOrder.OrderSubtotalExclTax = CartSubTotalExclTax;
                                            confirmOrder.OrderSubTotalDiscountInclTax = 0;
                                            confirmOrder.OrderSubTotalDiscountExclTax = 0;
                                            confirmOrder.OrderShippingInclTax = 0;
                                            confirmOrder.OrderShippingExclTax = 0;
                                            confirmOrder.PaymentMethodAdditionalFeeInclTax = 0;
                                            confirmOrder.PaymentMethodAdditionalFeeExclTax = 0;
                                            confirmOrder.TaxRates = "0:0;";
                                            confirmOrder.OrderTax = 0;
                                            confirmOrder.OrderDiscount = 0;
                                            confirmOrder.OrderTotal = CartSubTotal;
                                            confirmOrder.RefundedAmount = 0;
                                            confirmOrder.RewardPointsWereAdded = false;
                                            confirmOrder.CheckoutAttributeDescription = "";
                                            confirmOrder.CheckoutAttributesXml = "";
                                            confirmOrder.CustomerLanguageId = 1;
                                            confirmOrder.AffiliateId = 1;
                                            confirmOrder.CustomerIp = MyIPAddress;
                                            confirmOrder.AllowStoringCreditCardNumber = false;
                                            confirmOrder.CardType = "";
                                            confirmOrder.CardName = "";
                                            confirmOrder.CardNumber = "";
                                            confirmOrder.MaskedCreditCardNumber = "";
                                            confirmOrder.CardCvv2 = "";
                                            confirmOrder.CardExpirationMonth = "";
                                            confirmOrder.CardExpirationYear = "";
                                            confirmOrder.AllowStoringDirectDebit = false;
                                            confirmOrder.DirectDebitAccountHolder = "";
                                            confirmOrder.DirectDebitAccountNumber = "";
                                            confirmOrder.DirectDebitBankCode = "";
                                            confirmOrder.DirectDebitBankName = "";
                                            confirmOrder.DirectDebitBIC = "";
                                            confirmOrder.DirectDebitCountry = "";
                                            confirmOrder.DirectDebitIban = "";
                                            confirmOrder.AuthorizationTransactionId = "";
                                            confirmOrder.AuthorizationTransactionCode = "";
                                            confirmOrder.AuthorizationTransactionResult = "";
                                            confirmOrder.CaptureTransactionId = "";
                                            confirmOrder.CaptureTransactionResult = "";
                                            confirmOrder.SubscriptionTransactionId = "";
                                            confirmOrder.PurchaseOrderNumber = "";
                                            confirmOrder.PaidDateUtc = DateTime.Now;
                                            confirmOrder.ShippingMethod = "By Ground";
                                            confirmOrder.ShippingRateComputationMethodSystemName = "Shipping.FixedRate";
                                            confirmOrder.Deleted = false;
                                            confirmOrder.CreatedOnUtc = DateTime.Now;
                                            confirmOrder.UpdatedOnUtc = DateTime.Now;
                                            confirmOrder.RewardPointsRemaining = 0;
                                            confirmOrder.CustomerOrderComment = "";
                                            confirmOrder.OrderShippingTaxRate = 0;
                                            confirmOrder.PaymentMethodAdditionalFeeTaxRate = 0;
                                            confirmOrder.HasNewPaymentNotification = false;
                                            confirmOrder.AcceptThirdPartyEmailHandOver = false;
                                            confirmOrder.OrderTotalRounding = 0;

                                            #endregion ORDER Data 


                                            res2 = OrdersComp.ConfirmOrder(confirmOrder.StoreId, confirmOrder.CustomerId, confirmOrder.BillingAddressId, confirmOrder.ShippingAddressId, confirmOrder.OrderStatusId, confirmOrder.ShippingStatusId, confirmOrder.PaymentStatusId, confirmOrder.PaymentMethodSystemName, confirmOrder.CustomerCurrencyCode, confirmOrder.CurrencyRate, confirmOrder.
                                                                                CustomerTaxDisplayTypeId, confirmOrder.VatNumber, confirmOrder.OrderSubtotalInclTax, confirmOrder.OrderSubtotalExclTax, confirmOrder.OrderSubTotalDiscountInclTax, confirmOrder.OrderSubTotalDiscountExclTax, confirmOrder.OrderShippingInclTax, confirmOrder.OrderShippingExclTax, confirmOrder.
                                                                                PaymentMethodAdditionalFeeInclTax, confirmOrder.PaymentMethodAdditionalFeeExclTax, confirmOrder.TaxRates, confirmOrder.OrderTax, confirmOrder.OrderDiscount, confirmOrder.OrderTotal, confirmOrder.RefundedAmount, confirmOrder.RewardPointsWereAdded, confirmOrder.CheckoutAttributeDescription, confirmOrder.CheckoutAttributesXml, confirmOrder.
                                                                                CustomerLanguageId, confirmOrder.AffiliateId, confirmOrder.CustomerIp, confirmOrder.AllowStoringCreditCardNumber, confirmOrder.CardType, confirmOrder.CardName, confirmOrder.CardNumber, confirmOrder.MaskedCreditCardNumber, confirmOrder.CardCvv2, confirmOrder.CardExpirationMonth, confirmOrder.CardExpirationYear, confirmOrder.AllowStoringDirectDebit, confirmOrder.
                                                                                DirectDebitAccountHolder, confirmOrder.DirectDebitAccountNumber, confirmOrder.DirectDebitBankCode, confirmOrder.DirectDebitBankName, confirmOrder.DirectDebitBIC, confirmOrder.DirectDebitCountry, confirmOrder.DirectDebitIban, confirmOrder.AuthorizationTransactionId, confirmOrder.AuthorizationTransactionCode, confirmOrder.
                                                                                AuthorizationTransactionResult, confirmOrder.CaptureTransactionId, confirmOrder.CaptureTransactionResult, confirmOrder.SubscriptionTransactionId, confirmOrder.PurchaseOrderNumber, confirmOrder.PaidDateUtc, confirmOrder.ShippingMethod, confirmOrder.ShippingRateComputationMethodSystemName, confirmOrder.Deleted, confirmOrder.
                                                                                CreatedOnUtc, confirmOrder.UpdatedOnUtc, confirmOrder.RewardPointsRemaining, confirmOrder.CustomerOrderComment, confirmOrder.OrderShippingTaxRate, confirmOrder.PaymentMethodAdditionalFeeTaxRate, confirmOrder.HasNewPaymentNotification, confirmOrder.AcceptThirdPartyEmailHandOver, confirmOrder.OrderTotalRounding, confirmOrder.order_from);

                                            //Insert data in OrderItem table

                                            foreach (DataRow dr2 in MaxQty.Rows)
                                            {
                                                //calculating cart subtotal value
                                                PlaceOrder placeOrder = new PlaceOrder();
                                                placeOrder.GetCartSubTotal(dr2);

                                                #region OrderItem Data
                                                insertOrderItems.ProductId = (Int32)dr2["ProductId"];
                                                insertOrderItems.Quantity = (Int32)dr2["Quantity"];
                                                insertOrderItems.UnitPriceInclTax = (decimal)dr2["Price"];
                                                insertOrderItems.UnitPriceExclTax = (decimal)dr2["Price"];
                                                insertOrderItems.PriceInclTax = ProductFinalCostInclTax;
                                                insertOrderItems.PriceExclTax = ProductFinalCostExclTax;
                                                insertOrderItems.DiscountAmountInclTax = 0;
                                                insertOrderItems.DiscountAmountExclTax = 0;
                                                insertOrderItems.AttributeDescription = "";
                                                insertOrderItems.AttributesXml = dr2["AttributesXml"].ToString();
                                                insertOrderItems.DownloadCount = 0;
                                                insertOrderItems.IsDownloadActivated = false;
                                                insertOrderItems.LicenseDownloadId = 0;
                                                insertOrderItems.ItemWeight = 0;
                                                insertOrderItems.BundleData = "";
                                                insertOrderItems.ProductCost = 0;
                                                insertOrderItems.TaxRate = 0;

                                                #endregion OrderItem Data

                                                int CustomerId = confirmOrder.CustomerId;
                                                long res1 = OrdersComp.InsertOrderItemsData(CustomerId, insertOrderItems.ProductId, insertOrderItems.Quantity, insertOrderItems.UnitPriceInclTax, insertOrderItems.UnitPriceExclTax, insertOrderItems.PriceInclTax, insertOrderItems.PriceExclTax, insertOrderItems.DiscountAmountInclTax, insertOrderItems.DiscountAmountExclTax, insertOrderItems.AttributeDescription,
                                                                                           insertOrderItems.AttributesXml, insertOrderItems.DownloadCount, insertOrderItems.IsDownloadActivated, insertOrderItems.LicenseDownloadId, insertOrderItems.ItemWeight, insertOrderItems.BundleData, insertOrderItems.ProductCost, insertOrderItems.TaxRate);
                                                //RemoveFromCart(Customerid);
                                                ////UpdateProductStock updateProductStock = new UpdateProductStock();
                                                //UpdateProductStock((Int32)dr2["ProductId"], (Int32)dr2["Quantity"]);
                                            }

                                            //InsufficientRechargeFundsObj ISRFObj = new InsufficientRechargeFundsObj();
                                            ISRFObj.status = 0;
                                            ISRFObj.message = "Payment Pending";
                                            float pendingAmount = Convert.ToInt32(CartSubTotal);
                                            ISRFObj.pendingAmount = Convert.ToInt32(pendingAmount);
                                           // CartSubTotal = decimal.Zero;
                                            ISRFObj.orderID = "odr"+res2;
                                            res = JsonConvert.SerializeObject(ISRFObj).ToString();
                                            return res;

                                        }
                                        else
                                        {
                                            //checking payment done or not for the "order_id"
                                            DataTable paymentSucess = OrdersComp.getOrderDetails(int.Parse(id_customer), int.Parse(order_id));
                                            for (int c = 0; c <= paymentSucess.Rows.Count - 1; c++)
                                            {
                                                if (paymentSucess.Rows[c]["Id"].ToString().Contains(order_id))
                                                {
                                                    float PrsntBlnc = currentbal - Convert.ToInt32(CartSubTotal);
                                                    //deducting the wallet balance
                                                    Wallet.saveWalletBalance(Customerid.ToString(), order_id, CartSubTotal.ToString(), PrsntBlnc.ToString(), 2, 2, MyIPAddress, "Place Order UsingPayment Method");
                                                    createorderPaymentResult.message = "Order Sucess.";
                                                    createorderPaymentResult.order_id = order_id;
                                                    ResponseObj.result = createorderPaymentResult;
                                                    //CartSubTotal = decimal.Zero;
                                                    ResponseObj.status = 1;
                                                    res = JsonConvert.SerializeObject(ResponseObj).ToString();
                                                    return res;
                                                }
                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    createorderPaymentResult.message = "Cart is Empty..Please add items to cart before place order";
                                    ResponseObj.result = createorderPaymentResult;
                                    ResponseObj.status = 0;

                                }
                            }
                        }
                        else
                        {
                            createorderPaymentResult.message = "Cart is Empty..Please add items to cart before place order";
                            ResponseObj.result = createorderPaymentResult;
                            ResponseObj.status = 0;

                        }
                    }
                }
                else
                {
                    createorderPaymentResult.order_id= "id_address_delivery does not belogs to the customer";

                    ResponseObj.result = createorderPaymentResult;
                    ResponseObj.status = 0;
                }
               
            }
            catch (Exception Err)
            {

            }
            res = JsonConvert.SerializeObject(ResponseObj).ToString();
            return res;
        }


       
        private   decimal GetCartSubTotal(DataRow dataRow)
        {
            result = 0;
            try
            {
                if (dataRow["SpecialPrice"].ToString() == "")
                {
                    string str = dataRow["AttributesXml"].ToString();
                    Product_GetPriceAdjustment2(str);
                    Gettaxrate((Int32)dataRow["id"]);
                    //Checking Tier Price for the product
                    if ((bool)dataRow["HasTierPrices"] == true)
                    {
                        GetTierProdutTierPrice2((Int32)dataRow["id"], (Int32)dataRow["Quantity"]);
                    }
                    if (ProductTierPrice != decimal.Zero)
                    {
                        ProductInitialPrice = ProductTierPrice;
                    }
                    else
                    {
                        ProductInitialPrice = (decimal)dataRow["Price"];
                    }
                    ProductFinalCost = decimal.Zero;
                    ProductFinalCost = decimal.Multiply((ProductInitialPrice + ProductAdjPrice), (Int32)dataRow["Quantity"]);
                    //ProductFinalCost = decimal.Multiply(((decimal)dataRow["Price"] + ProductAdjPrice - (decimal)dataRow["Discount"]), (Int32)dataRow["Quantity"]);
                    ProductFinalCostExclTax = ProductFinalCost;
                    ProductFinalCostInclTax = ProductFinalCost * (1 + taxrate / 100);
                }
                else
                {
                    string str = dataRow["AttributesXml"].ToString();
                    Product_GetPriceAdjustment2(str);
                    Gettaxrate((Int32)dataRow["id"]);
                    ProductFinalCost = decimal.Zero;
                    ProductFinalCost = decimal.Multiply(((decimal)dataRow["SpecialPrice"] + ProductAdjPrice - (decimal)dataRow["Discount"]), (Int32)dataRow["Quantity"]);
                    ProductFinalCostExclTax = ProductFinalCost;
                    ProductFinalCostInclTax = ProductFinalCost * (1 + taxrate / 100);
                }
            }
            catch (Exception Err)
            {

            }

            return CartSubTotal;
        }

    
        private decimal Product_GetPriceAdjustment2(string attribute)
        {
            ProductAdjPrice = decimal.Zero;
            try
            {
                if (attribute != "")
                {
                    var attributesXml = attribute.ToString();
                    var doc = XDocument.Parse(attributesXml);

                    // Attributes/ProductVariantAttribute
                    foreach (var node1 in doc.Descendants("ProductVariantAttribute"))
                    {
                        string ID = node1.Attribute("ID").Value;
                        int ir = int.Parse(ID);
                        // ProductVariantAttributeValue/Value
                        foreach (int node2 in node1.Descendants("Value"))
                        {
                            int Val = node2;
                            dt4 = GetPriceAjustments(ir, Val);
                            if (dt4.Rows.Count > 0)
                            {
                                decimal AdditionalPrice = decimal.Zero;
                                AdditionalPrice = (decimal)dt4.Rows[0][0];
                                ProductAdjPrice += AdditionalPrice;
                            }
                        }
                    }
                }
            }
            catch (Exception Err)
            {

            }
            return result;

        }
      
        private DataTable GetPriceAjustments(int ID, int Value)
        {

            dt4 = OrdersComp.GetPriceAdjnustment(ID, Value);

            return dt4;
        }
    
        private  decimal Gettaxrate(int productID)
        {
            taxrate = decimal.Zero;
            try
            {
                DataTable dt1 = OrdersComp.GetProductTaxCategory(productID);

                if (dt1.Rows.Count > 0 && (bool)dt1.Rows[0]["IsTaxExempt"] == false)
                {
                    taxrate = (decimal)dt1.Rows[0]["Percentage"];
                }
                else if (dt1.Rows.Count > 0 && (bool)dt1.Rows[0]["IsTaxExempt"] == true)
                {
                    taxrate = 0;
                }
            }
            catch (Exception Err)
            {

            }
            return result = taxrate;
        }

        public static void RemoveFromCart(int CustomerID)
        {
            try
            {
                long res = OrdersComp.RemoveFromCart(CustomerID);
            }
            catch (Exception Err)
            {

            }

        }

        public static  void UpdateProductStock(int ProductId, int Qty)
        {

            try
            {
                long res = OrdersComp.UpdateProductStock(ProductId, Qty);
            }
            catch (Exception Err)
            {

            }

        }

        public  void GetTierProdutTierPrice2(int ProductId, int Qty)
        {
            try
            {
                DataTable dt1 = OrdersComp.GetTierPrice(ProductId);
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i <= dt1.Rows.Count - 1; i++)
                    {
                        if (Qty >= (Int32)dt1.Rows[i]["Quantity"])
                        {
                            ProductTierPrice = (decimal)dt1.Rows[i]["TierPrice"];
                        }
                    }
                }
                else
                {
                    ProductTierPrice = decimal.Zero;
                }
            }
            catch (Exception Err)
            {

            }
        }

    }
}